
<!-- README.md is generated from README.Rmd. Please edit that file -->

<img src=CeCylogo.png align="right" width="12%" hspace="50">

# Public repository for the Cerrado plant cytogenetics shiny app<br></br><br></br>CerradoCyto<br></br><br></br>

<!-- badges: start -->

[![](https://img.shields.io/badge/doi-10.3897/CompCytogen.v11i2.11395-green.svg)](https://doi.org/10.3897/CompCytogen.v11i2.11395)
<!-- badges: end -->

This repository shows only the shiny basic files of the shiny app hosted
in:

Web-page: <https://cyto.shinyapps.io/cerrado/>

The related publication is:
<https://compcytogen.pensoft.net/article/11395/>

The left side menu of the page consists of:

  - Scientometrics
    
      - techniques chronology: Plot, table
      - Authors by region: Plot, table
      - Study effort: Network, table, video
      - Coauthors network <br></br> <br></br>  

  - 2n and 2C histograms - dotplots - violinplots
    
      - 2n in major clades: Clade, Family, Genus, Table
      - Genome size 2C-value: Clade, Family, Genus, Table <br></br>
        <br></br>  

  - Total Chromosome Length -TCL- and TC Area - TCA - vs 2C: Histogram,
    Correlation, Table

  - Database search: Query, Histograms

  - Stats

  - Contribute

  - About

## Authors

[Fernando
Roa](https://ferroao.gitlab.io/curriculumpu/)<a href="https://liberapay.com/ferroao/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a>
<img src="http://img.shields.io/liberapay/receives/ferroao.svg?logo=liberapay">

[Mariana Telles](http://lattes.cnpq.br/4648436798023532)

## References

<div id="refs" class="references">

<div id="ref-R-robustHD">

Alfons A. 2016. *RobustHD: Robust methods for high-dimensional data*.
<https://CRAN.R-project.org/package=robustHD> 

</div>

<div id="ref-R-rmarkdown">

Allaire J, Xie Y, McPherson J, Luraschi J, Ushey K, Atkins A, Wickham H,
Cheng J, Chang W, Iannone R. 2019. *Rmarkdown: Dynamic documents for r*.
<https://CRAN.R-project.org/package=rmarkdown> 

</div>

<div id="ref-R-shinyjs">

Attali D. 2018. *Shinyjs: Easily improve the user experience of your
shiny apps in seconds*. <https://CRAN.R-project.org/package=shinyjs> 

</div>

<div id="ref-R-gridExtra">

Auguie B. 2017. *GridExtra: Miscellaneous functions for "grid"
graphics*. <https://CRAN.R-project.org/package=gridExtra> 

</div>

<div id="ref-R-sna">

Butts CT. 2016. *Sna: Tools for social network analysis*.
<https://CRAN.R-project.org/package=sna> 

</div>

<div id="ref-R-network">

Butts CT. 2019. *Network: Classes for relational data*.
<https://CRAN.R-project.org/package=network> 

</div>

<div id="ref-R-shinydashboard">

Chang W, Borges Ribeiro B. 2018. *Shinydashboard: Create dashboards with
’shiny’*. <https://CRAN.R-project.org/package=shinydashboard> 

</div>

<div id="ref-R-shiny">

Chang W, Cheng J, Allaire J, Xie Y, McPherson J. 2019. *Shiny: Web
application framework for r*. <https://CRAN.R-project.org/package=shiny>

</div>

<div id="ref-R-data.table">

Dowle M, Srinivasan A. 2019. *Data.table: Extension of ‘data.frame‘*.
<https://CRAN.R-project.org/package=data.table> 

</div>

<div id="ref-R-RefManageR">

McLean MW. 2019. *RefManageR: Straightforward ’bibtex’ and ’biblatex’
bibliography management*.
<https://CRAN.R-project.org/package=RefManageR> 

</div>

<div id="ref-R-mongolite">

Ooms J. 2019. *Mongolite: Fast and simple ’mongodb’ client for r*.
<https://CRAN.R-project.org/package=mongolite> 

</div>

<div id="ref-R-bib2df">

Ottolinger P. 2019. *Bib2df: Parse a bibtex file to a data frame*.
<https://CRAN.R-project.org/package=bib2df> 

</div>

<div id="ref-R-rhandsontable">

Owen J. 2018. *Rhandsontable: Interface to the ’handsontable.js’
library*. <https://CRAN.R-project.org/package=rhandsontable> 

</div>

<div id="ref-R-GGally">

Schloerke B, Crowley J, Cook D, Briatte F, Marbach M, Thoen E, Elberg A,
Larmarange J. 2018. *GGally: Extension to ’ggplot2’*.
<https://CRAN.R-project.org/package=GGally> 

</div>

<div id="ref-R-gtools">

Warnes GR, Bolker B, Lumley T. 2018. *Gtools: Various r programming
tools*. <https://CRAN.R-project.org/package=gtools> 

</div>

<div id="ref-R-plyr">

Wickham H. 2016. *Plyr: Tools for splitting, applying and combining
data*. <https://CRAN.R-project.org/package=plyr> 

</div>

<div id="ref-R-scales">

Wickham H. 2018. *Scales: Scale functions for visualization*.
<https://CRAN.R-project.org/package=scales> 

</div>

<div id="ref-R-stringr">

Wickham H. 2019a. *Stringr: Simple, consistent wrappers for common
string operations*. <https://CRAN.R-project.org/package=stringr> 

</div>

<div id="ref-R-lazyeval">

Wickham H. 2019b. *Lazyeval: Lazy (non-standard) evaluation*.
<https://CRAN.R-project.org/package=lazyeval> 

</div>

<div id="ref-R-readxl">

Wickham H, Bryan J. 2019. *Readxl: Read excel files*.
<https://CRAN.R-project.org/package=readxl> 

</div>

<div id="ref-R-ggplot2">

Wickham H, Chang W, Henry L, Pedersen TL, Takahashi K, Wilke C, Woo K,
Yutani H. 2019a. *Ggplot2: Create elegant data visualisations using the
grammar of graphics*. <https://CRAN.R-project.org/package=ggplot2> 

</div>

<div id="ref-R-dplyr">

Wickham H, François R, Henry L, Müller K. 2019b. *Dplyr: A grammar of
data manipulation*. <https://CRAN.R-project.org/package=dplyr> 

</div>

<div id="ref-R-tidyr">

Wickham H, Henry L. 2019. *Tidyr: Easily tidy data with ’spread()’ and
’gather()’ functions*. <https://CRAN.R-project.org/package=tidyr> 

</div>

<div id="ref-R-knitr">

Xie Y. 2019. *Knitr: A general-purpose package for dynamic report
generation in r*. <https://CRAN.R-project.org/package=knitr> 

</div>

<div id="ref-R-DT">

Xie Y, Cheng J, Tan X. 2019. *DT: A wrapper of the javascript library
’datatables’*. <https://CRAN.R-project.org/package=DT> 

</div>

</div>
