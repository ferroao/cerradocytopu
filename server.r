options(shiny.maxRequestSize=30*1024^2) 
function(input, output, session) {
  formData <- reactive({
    assign(fields[1],input$query)
    assign(fields[2],Sys.time() )
    assign(fields[3],input$fingerprint)
    assign(fields[4],input$ipid )
    data<-as.data.frame(mget(fields))
    data
  })
  observeEvent(input$tablequerybutton, {
    savequery<-tryCatch(saveData(formData(),"queriesappsio","cyto"),error = function(e) {print(paste("no internet")) ; "no internet" } )
    if (typeof(savequery)=="character")
    {
      values[["texterrorquery"]] <- "Connection to mongo database was not possible"      
    }
    else {
      values[["texterrorquery"]] <- ""
    }
      })
  countryaffiduplicated_r<-reactive({
    if(!invalid(input$affiforprovhisto)){ 
    data = countryaffiduplicated[which(countryaffiduplicated$affispecific %in% c(input$affiforprovhisto) )    ,]
      }
    else { 
    data = countryaffiduplicated
    }
    groupby <- group_by_at(data, .vars=c("species", "parsed2n", "parsed2c", "year", "provspecific") )
    datagrouped<- summarise(groupby)#, count = n())
    datagrouped = as.data.frame(datagrouped)
    datagrouped
  })
  summaryperyear_r<-reactive({ 
  groupy <- group_by_at(countryaffiduplicated_r(), vars(year,provspecific) )
  summaryperyear<- summarise(groupy, count = n())
  summaryperyear = as.data.frame(summaryperyear)[, 1:3]
  summaryperyear
  }) 
  output$histoaccperyear <- renderPlot({ 
    if( max(as.numeric(summaryperyear_r()$year)) -  min (as.numeric(summaryperyear_r()$year)) < 5 ) 
      { br <- 5} else { br <- 50}
    ggplot(summaryperyear_r(),aes(x=as.numeric(year), y=count, 
                                  fill=as.factor(summaryperyear_r()$provspecific)) ) + 
      geom_bar(stat='identity') + 
        labs(x = "year", y = " Accesions ") + 
      theme(axis.title.y = element_text(angle = 0, size=rel(1), hjust=0, vjust=0.5)) +
        scale_fill_manual(name="Provenance of plant", values= cbPalette) + 
     theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
        scale_x_continuous(breaks=pretty_breaks(br)) + 
      scale_y_continuous(breaks=pretty_breaks(15)) 
    }) 
  a=reactive({
    a=sort(input$selectcountry)
  })
  b=sort(levels(countryduplicated$provspecific))
  comp_list4 <- function(x) sum(duplicated.default(x)) == length(x) - 1L
  countrydataselected <-reactive({
    test_true=list(a(),b)
    if (all(!is.null(input$selectcountry),!input$selectcountry == "", !comp_list4(test_true)  )  )  {
      data = countryduplicated[which(countryduplicated$provspecific %in% c(input$selectcountry) )    ,]
      data[data==0] <- NA
      data$range=as.factor(data$range)
      data
    } else {
      data <- tech.m
      data<-setDT(data)
      data[data==0] <- NA
      data$range=as.factor(data$range)
     data
      }
  })
  observeEvent(input$tablequerybutton, {
    shinyjs::addClass(selector= "body", class = "sidebar-collapse" )
      } )
  observe({
    if( input$tabs=="networktab")
    {
    shinyjs::addClass(selector= "body", class = "sidebar-collapse" )
  }
    })
  observe({
    if(!invalid(input$allowmodes)){
    if( input$allowmodes==TRUE)
    {
      shinyjs::addClass(selector= "body", class = "sidebar-collapse" )
    }
    }
  })
  summarydata= reactive({
    data<-countrydataselected()
    sumdata<- data %>% 
      group_by_at(.vars=c("range","variable") ) %>%
      summarise(
        N    = sum(!is.na(value)) 
      ) %>% 
      bind_rows(
        data %>% 
          group_by_at("variable")  %>%
          summarise(
            N    = sum(!is.na(value)) 
          ) %>% 
          mutate(range = "All years")
      ) #bind_rows
    sumdata<-as.data.frame(sumdata)
    sumdata[sumdata==0] <- NA
    sumdata$range=as.factor(sumdata$range)
    sumdata
  }) #end reactive
  observeEvent(input$showcountry,{
    if (input$showcountry == "") return(NULL)
    toggle(id = "countryoptionsbox", anim = TRUE)
  }) #END OBSERVEEVENT
  columnstoshow = reactive ({
    x=  ((vars$counter+1)/2) # %% 2 == 0)
    if (!is.null (x))
    {
      if (x %% 2 == 0) {
        c=c(5, 24, 3, 4, 21, 20, 12, 10, 14, 16,17,18,19,23)
      }
      else {
        c=c(5, 24, 3, 4, 21, 20, 12, 10, 14, 23)
      }
    } #end 1st if
    else {
      c=c(5, 24, 3, 4, 21, 20, 12, 10, 14, 16,17,18,19,23)
    }
  })
  columnstoshowtblhistogen = reactive ({
    x=  ((varshistogen$counter+1)/2) # %% 2 == 0)
    if (!is.null (x))
    {
      if (x %% 2 == 0) {
        c=c(5, 24, 3, 4, 21, 20, 12, 10, 14, 16,17,18,19,23)
      }
      else {
        c=c(5, 24, 3, 4, 21, 20, 12, 10, 14, 23)
      }
    }
    else {
      c=c(5, 24, 3, 4, 21, 20, 12, 10, 14, 16,17,18,19,23)
    }
  })
  columnstoshowtblhistogen2c = reactive ({
    c=c(5, 24, 3, 4,15, 21, 20, 12, 10, 14, 23)
  })
  columnstoshowtblhistotcl = reactive ({
    c=c(5, 24, 3, 4,24,15, 21, 20, 12, 10, 14, 23) #24 es tcl?
  })
  vars = reactiveValues(counter = 0)
  varshistogen = reactiveValues(counter = 0)    
  observeEvent(input$plot_click2,{
    if(typeof(tblaffi())=="list"){
      if (!is.null(tblaffi() )  & nrow(tblaffi() ) > 0  )
      {
    if (input$plot_click2$x == "") 
      return(NULL)
    show(id = "hiddenbutton", anim = TRUE)
      }}
  }) #END OBSERVEEVENT
  observeEvent(input$tablehistogenbutton,{
    if (input$tablehistogenbutton == ""| nrow(tblhistogen() ) == 0 )  
      hide(id = "hiddenbuttonhistogen", anim = TRUE)# return(NULL)
    show(id = "hiddenbuttonhistogen", anim = TRUE)
  }) #END OBSERVEEVENT
  output$tablehistotclstatbuttonoutput <- renderUI({
    actionButton('tablehistotclstatbutton',strong('Generate table of 2C and',tcaortcl(),'data'),  icon("hand-pointer-o"),
                 style="color: #000; background-color: #0099ff; border-color: #2e6da4"
    )
  })
  output$contributetab <- renderUI({  
  fluidRow(
    column(width=4, 
           wellPanel(
             h3("Table size"),
             helpText("Create a table of any size to paste/write your data", p("by pressing \"Generate table\" button."), 
                      p("Right-click on the table to delete/insert rows.")
             ),
             sliderInput("numrows", "number of desired rows", 1, 20, 10, step = 1, ticks=F ),
             sliderInput("numcols", "number of desired columns", 1, 20, 10, step = 1, ticks=F ),
             actionButton('generatetablebutton',strong('Generate table'),  icon("hand-pointer-o") , style="padding:10px 5px 10px 5px"
                 ),
             uiOutput("errorgeneratetable")    
                  ), # end wellpanel
           br(), 
           wellPanel(
             h3("Upload"), 
             actionButton("save", "Submit table"),
             uiOutput("rownumui"),
             uiOutput("errorsave"),  
                          helpText("Your contributions are verified before publishing" #, p("by pressing \"Generate table\" button."), 
             )
           )
    ) #end column
    , # end column
    column(width=8,
           rHandsontableOutput("out")
    ) # column
  ) #fluidrow
  })
  
  output$bubbleprovenancebox <- renderUI({
    box(width=12, 
        span("Choose provenance of plants"),
        solidHeader =F, 
        status="primary",
        checkboxGroupInput("provenancecheckbox", " ",
                           choices  = levels(countryaffiduplicated$provspecific),
                           selected = levels(countryaffiduplicated$provspecific)
                           ),
        HTML("<br></br>"),
        radioButtons("referencesoracc", "Choose bubble size variable",
                           choices  = c("number of references"="ref", "number of accessions"="acc"),#levels(countryaffiduplicated$provspecific),
                           selected = "acc"
        ),
        HTML("<br></br>"),
        span("If nothing is selected all the sample is shown")
    )
  })
  observe({
    validate(
      need(try(!invalid(input$selectallaffis)), "Wait 801")
    )     
    if (input$selectallaffis > 0) {
      if (input$selectallaffis %% 2 == 0){
        updateCheckboxGroupInput(session=session, 
                                 'affiforprovhisto',
                                 '', 
                                 choices= standardnamescountryaffi,# levels(countryaffiduplicated$affispecific),
                                 selected = levels(countryaffiduplicated$affispecific)   
        )
      }
      else {
        updateCheckboxGroupInput(session=session, 
                                 'affiforprovhisto',
                                 '', 
                                 choices= standardnamescountryaffi, # levels(countryaffiduplicated$affispecific),
                                 selected = c()#levels(countryaffiduplicated$affispecific)   
        )
      }
    }
  } ) #end observe
  output$studyhistopropertiesbox <- renderUI({
    box(width=12, 
        span("Select country or region of affiliation of authors to be included in histogram"),
        solidHeader =F, 
        status="primary",
        checkboxGroupInput("affiforprovhisto","", 
                           choices= standardnamescountryaffi, #levels(countryaffiduplicated$affispecific),
                           selected = levels(countryaffiduplicated$affispecific)        
                           # selected = unique(countryaffiduplicated$affispecific)        
    ),
    div( style="width: 100%;padding: 10px 5px 0px 5px" ,actionButton("selectallaffis", label="Select/Deselect all")),
    HTML("<br></br>"),
    span("If nothing is selected all the sample is shown")
    )# end box
  })
  output$networkpropertiesbox <- renderUI({
      box(width=12, 
        span("Choose year interval and Country and click button"),
        solidHeader =F, 
        status="primary",
  div( style="width: 100%;padding: 10px 5px 10px 5px" , actionButton('generatenetworkbutton',strong('Generate network'),   icon("hand-pointer-o") )
  ),      
  sliderInput(
    "labelnetwork",
    "font size:",
    min = 3,
    max = 5,
    value = 3, ticks=F, step=1
  ),
        sliderInput("rangeyearnetwork",
                    "Years:",
                    min = minyear(),
                    max = maxyear(),
                    value = c(minyear(),maxyear()), ticks=F, round=F, step=1
                    ),
        selectizeInput('countryselector','Select Country',  width="100%", levels(as.factor(authorduplicated$countryalone)), 
                       levels(as.factor(authorduplicated$countryalone)), multiple = T
        ),
  div( style="width: 100%;padding: 0px 5px 0px 5px" , actionButton("selectamerica", label="Select/Deselect America")),
  div( style="width: 100%;padding: 10px 5px 0px 5px" ,  actionButton("selecteurope", label="Select/Deselect Europe")),
       div( style="width: 100%;padding: 10px 5px 0px 5px" ,actionButton("selectrestworld", label="Select/Deselect Rest of the World")),
            div( style="width: 100%;padding: 10px 5px 0px 5px" ,actionButton("selectallcountries", label="Select/Deselect all")),
  htmlOutput("outauthor")
)
  })
  minyear<- reactive({
    min(tech2$year)  #bibdf
    })
  maxyear<- reactive({
    max(tech2$year) # bibdf  
    })
countryselec<-eventReactive(input$generatenetworkbutton, {
countrydup<-authorduplicated[which(authorduplicated$countryalone  %in% c(input$countryselector)),]
})

refs<- reactive ({
   validate(
     need(try(countryselec() != ""), "Please click on \"Generate network\" button, also any time you change parameters")
   )
  newselected<-countryselec() %>%
    group_by_at("uniquecite") %>%
    summarise(countryallmerged = paste(countryalone, collapse = "; "),
              author = paste(authoralone, collapse = " and ")) # important create author
  # newselected<-as.data.frame(newselected)
  # refs<-bibdfallaffi
  refs<-merge(bibdfallaffi, newselected, by="uniquecite", all.x = TRUE)
  refs<-refs[which(refs$uniquecite %in% countryselec()$uniquecite # 
  )  ,  ]
  refs$uniquecite <- gsub("\\s+", "_", refs$uniquecite) # was key
  refs$uniquecite <- gsub("[^a-z0-9_A-Z]", "", refs$uniquecite) # was key
  rownames(refs) <- refs[,"uniquecite"] # was key
  refs<-refs[,c(2:ncol(refs))]
  refs$author<-as.character(refs$author)
  refs<-refs[which(refs$year >= input$rangeyearnetwork[1]
                                            & refs$year <= input$rangeyearnetwork[2]
  )  ,  ]
 refs
  })
  observeEvent(input$generatenetworkbutton, {
    validate(
      need(try(countryselec() != ""), "Wait 204")
    )
    values[["refs2"]] <- refs()
    values[["yearauthormin"]] <- input$rangeyearnetwork[1]
    values[["yearauthormax"]] <- input$rangeyearnetwork[2]
  })
  coauthorrelated <- reactiveValues()
  observeEvent( input$generatenetworkbutton, {
    validate(
      need(try(refs() != ""), "No data to show")
    )
    if(!is.null(input$rangeyearnetwork))
    {
      citations <- as.BibEntry(values[["refs2"]])
  authors <- lapply(citations, function(x) as.character(x$author))
  unique.authors<-unique(unlist(authors))
  coauth.table <- matrix(nrow=length(unique.authors),
                         ncol = length(unique.authors),
                         dimnames = list(unique.authors, unique.authors), 0)
  for(i in 1:length(citations)){
    print(i)
    paper.auth <- unlist(authors[[i]])#[names(unlist(authors[[i]])) == 'family']
    coauth.table[paper.auth,paper.auth] <- coauth.table[paper.auth,paper.auth] + 1
  }
  coauth.table <- coauth.table[rowSums(coauth.table)>0, colSums(coauth.table)>0]
  tryCatch(diag(coauth.table) <- 0, error = function(e) {print(paste("not enough data")) ; "not enough data 312" } )
  coadf<-as.data.frame(coauth.table)
  coauthorrelated$coadf<-coadf
  matrixnetwork = network(coadf,
                matrix.type = "adjacency",
                ignore.eval = FALSE,
                names.eval = "weights")
  matched<-   authorcountryalone$countryalone[match(unique.authors, authorcountryalone$authoralone)]
  matrixnetwork %v% "Country" = matched
  smat <- sort(table(matched),decreasing=T)
  lem<-length(smat)
  names(copalette) <- names(smat)
  coauthorrelated$matrix<- matrixnetwork
  coauthorrelated$palette<-copalette[1:lem]
  coauthorrelated$lenpalette<-lem
  coauthorrelated$uniqueauthorslen<-length(unique.authors)
      } 
  })
  output$networkboxes <- renderUI({  
    div(id = "networkboxesbox",
  HTML("<div class='col-sm-2' style='min-width: 290px !important; padding:5px 5px 0px 5px;'>"),
  uiOutput("networkpropertiesbox"),
  HTML("</div>") ,
  HTML("<div class='col-sm-9' style='padding:5px 5px 0px 5px;'>"),
  tryCatch(uiOutput("networkplotbox"),
           error = function(e) {print(paste("not enough data")) ; "not enough data 359" } ),
  HTML("</div>") 
    ) # end div
  })
  output$studyhistoboxes <- renderUI({  
    div(id = "studyhistoboxesbox",
        HTML("<div class='col-sm-2' style='min-width: 200px !important; padding:5px 5px 0px 5px;'>"),
        uiOutput("studyhistopropertiesbox"),
        HTML("</div>") ,
        HTML("<div class='col-sm-10' style='padding:5px 5px 0px 5px;'>"),
        tryCatch(uiOutput("studyhistoplotbox"),error = function(e) {print(paste("not enough data")) ; "not enough data 359" } ),
        HTML("</div>") 
    ) # end div
  })
  output$networkplot <- tryCatch(renderPlot({
    validate(
      need(any(coauthorrelated$coadf>0) == TRUE, "Insufficient sample, please select more countries/years")
    )
      if(!invalid(coauthorrelated$matrix)  && any(coauthorrelated$coadf>0) == TRUE)
        {
        if(coauthorrelated$lenpalette<2){ 
      coauthorrelated$plot<-tryCatch(ggnet2(coauthorrelated$matrix, color = "Country", legend.position = "right", 
             label = TRUE,  alpha = 0.5, 
             label.size = input$labelnetwork, 
             edge.size="weights", size="degree", size.legend="Degree Centrality"), error = function(e) {print(paste("not enough data 358")) ; "not enough data 358" } ) 
      tryCatch(coauthorrelated$plot + themepmu01010101cm + themeLegBerLegSpa1+#theme(plot.margin=unit(c(0.1,0.1,0.1,0.1),"cm")) + 
        theme(legend.box = "horizontal"), error = function(e) {print(paste("not enough data 362")) ; "not enough data 362" } )
        } # one color
        else{
          coauthorrelated$plot<-tryCatch(ggnet2(coauthorrelated$matrix, color = "Country", palette = coauthorrelated$palette, legend.position = "right", 
                                                label = TRUE,  alpha = 0.5, 
                                                label.size = input$labelnetwork, 
                                                edge.size="weights", size="degree", size.legend="Degree Centrality"), error = function(e) {print(paste("not enough data 369")) ; "not enough data 369" } ) 
          tryCatch(coauthorrelated$plot + themepmu01010101cm +# theme(plot.margin=unit(c(0.1,0.1,0.1,0.1),"cm")) +
                     themeLegBerLegSpa1+
                     theme(legend.box = "horizontal"), error = function(e) {print(paste("not enough data 2")) ; "not enough data 2" } ) 
        } # end else
    } 
  }) , error = function(e) {print(paste("not enough data")) ; "not enough data" } )
  
  
  
  output$studyhistoplotbox = renderUI({
    box(width=12,  
        title = "Study effort per year, number of accessions", #textOutput("text1"),
        status="primary", 
        solidHeader=T,
              plotOutput("histoaccperyear") #, height = 800, width = 1100, hover=hoverOpts(id="hoverinputauthor"), click =clickOpts(id="plot_clickauthor") 
    )
  })
  output$networkplotbox = renderUI({
    validate(
      need(nrow(refs()) > 0, "Please make a search, click button \"Generate network\" everytime you change parameters" )
    )  
    box(width=12,  
        height="950px",
        title = "Coauthor network", #textOutput("text1"),
        status="primary", 
        solidHeader=T,
        div(style = 'overflow-x: scroll',  
            tryCatch(plotOutput("networkplot", height = 800, width = 1100, hover=hoverOpts(id="hoverinputauthor"), click =clickOpts(id="plot_clickauthor") 
        ),error = function(e) {print(paste("not enough data")) ; "not enough data 393" } )
        )
       ,
       HTML('Pass mouse over author names and click to generate table')
       , 
       htmlOutput("hover_dadosauthor")
      )# box
  })
  output$optionsclades2nbox2gen <- renderUI({
    sliderInput("bins2ngen",
                "Bin width:",
                min = 1,
                max = 5,
                value = 2, step=1, ticks=F)
  })
  output$optionsclades2nbox3gen <- renderUI({
    radioButtons("x2nlegendtypegen","Type of legend",c("median and counts per taxa","3 modes and counts"))
  })
  output$optionsclades2nbox4gen <- renderUI({
    validate(
      need(min2ngen() != "", "Wait 186")
    )
    sliderInput("range2ngen",
                "2n:",
                min = 50,#min2ngen(),
                                max = max2ngen(),
                value = c(max2ngen()), ticks=F, round=F, step=1)
  })
  output$optionsclades2nbox2fam <- renderUI({
    sliderInput("bins2nfam",
                "Bin width:",
                min = 1,
                max = 5,
                value = 2, step=1, ticks=F)
  })
  output$optionsclades2nbox3fam <- renderUI({
    radioButtons("x2nlegendtypefam","Type of legend",c("median and counts per taxa","3 modes and counts"))
  })
  output$optionsclades2nbox4fam <- renderUI({
    validate(
      need(min2nfam() != "", "Wait 213")
    )
    sliderInput("range2nfam",
                "2n:",
                min = 50,#min2nfam(),
                max = max2nfam(),
                value = c(max2nfam()), ticks=F, round=F, step=1)
  })
  output$majorcladepanel <- renderUI({  
    fluidRow(
      column(3,
             box(width=12, title="Options", solidHeader =T, status="primary",
                 radioButtons("par2ntype","",c("dotplot","histogram"),"dotplot"),
                 uiOutput("optionsclades2nbox")
             )#box
            ),#col
      column(9,
             uiOutput("threeboxes"),
             uiOutput("x2nhistobox")#,
      ) #col
) # end fluidrow
})
  output$majorcladepanel2c <- renderUI({  
    fluidRow(
      column(3,
             box(width=12, title="Options", solidHeader =T, status="primary",
                 radioButtons("par2ctype","",c("violinplot","histogram or density plot"="histogram"),"violinplot"),
                 uiOutput("x2ctabbox")
             )#box
      ),#col
      column(9,
             uiOutput("threeboxes2c"),
             uiOutput("x2chistobox")#,
      ) #col
    ) # end fluidrow
  } )
  output$familypanel <- renderUI({ 
    fluidRow(
             HTML("<div class='col-sm-3' style='padding:0px 0px 0px 0px;'>"),
             box(width=12, title="Options", solidHeader =T, status="primary",
                 selectInput('apgcladeinputfam','Select clade',  width="100%", levels(x2ndata$apgorder), 
                             selected=levels(x2ndata$apgorder)[2])
                 ,radioButtons("par2ntypefam","",c("dotplot","histogram"),"dotplot"),
                 uiOutput("optionsclades2nboxfam"),
                 uiOutput("selectfamilybox")
             ) # end box
  ,HTML("</div>") ,
    HTML("<div class='col-sm-9' style='padding:0px 0px 0px 0px;'>"),
             uiOutput("threeboxesfam"),
             uiOutput("x2nhistofambox")#,
    ,HTML("</div>")
    ) #fluidrow
 } ) 
  output$familypanel2c <- renderUI({ 
    fluidRow(
  HTML("<div class='col-sm-3' style='padding:5px 5px 0px 5px;'>"),
  box(width=12, title="Options", solidHeader =T, status="primary",
      selectInput('apgcladeinputfam2c','Clade',  
                  width="100%", levels(x2cdata$apgorder), selected=levels(x2cdata$apgorder)[2]
      )
      ,radioButtons("par2ctypefam","",c("violinplot","histogram or density plot"="histogram"),"violinplot"),
      uiOutput("x2ctabboxfam")
  ) # end box
  ,HTML("</div>") ,
  HTML("<div class='col-sm-9' style='padding:0px 0px 0px 0px;'>"),
         uiOutput("threeboxes2cfam"),
         uiOutput("x2chistofambox")#,
  ,HTML("</div>") 
    ) # end fluidrow
  } )
  output$genuspanel2c <- renderUI({ 
    fluidRow(
      HTML("<div class='col-sm-3' style='padding:5px 5px 0px 5px;'>"),
      box(width=12, title="Options", solidHeader =T, status="primary",
          selectInput('apgcladeinputgenclade2c','Clade',  width="100%", 
                      levels(x2cdata$apgorder), selected=levels(x2cdata$apgorder)[4]
          ) #end input
          ,uiOutput("selectinputapgcladeinputgenfam2c")
          ,uiOutput("selectinputapgcladeinputgensubfam2c")
          ,radioButtons("par2ctypegen","",c("violinplot","histogram or density plot"="histogram"),"violinplot")
          ,uiOutput("x2ctabboxgen"), 
          actionButton('tablehistogenbutton2c',strong('Generate table of histogram data'),  icon("hand-pointer-o"),
                       style="color: #000; background-color: #0099ff; border-color: #2e6da4"
          )
      ) # end box
      ,HTML("</div>") ,
      HTML("<div class='col-sm-9' style='padding:0px 0px 0px 0px;'>"),
      uiOutput("threeboxes2cgen"),
      uiOutput("x2chistogenbox")#,
      ,HTML("</div>") 
    ) # end fluidrow
  } )
  output$x2ctabboxgen = renderUI({
    if(input$par2ctypegen=="histogram")
    {
      if (length(levels(x2cdata2gencladefam()$`family_apg_aste`)) <=1 )
      {
        wellPanel(width=12, 
                  radioButtons("histoordensitygen","Choose type of graph",c("Histogram","Density")),
                  checkboxInput("par2cgridgen","Show as grid",value=TRUE),
             selectizeInput(
               inputId = "selectgenus2c", 
               label = "",
               multiple  = T,
               choices  = levels(x2cdata2gencladefam()$genus),
               selected = levels(x2cdata2gencladefam()$genus)
             ), #select
             actionButton("selectallgen2c", label="Select/Deselect all"),
             HTML("<br></br>"),
             span("Maintain any genus(era), or clic in the field to delete (with keyboard) or reselect (mouse) any"),
             HTML("<br></br>"),
             span("If nothing is selected all the sample is shown")
        ) #box
      } # if no subfamilies
      else{
        wellPanel(width=12, 
                  radioButtons("histoordensitygen","Choose type of graph",c("Histogram","Density")),
                  checkboxInput("par2cgridgen","Show as grid",value=TRUE),
             selectizeInput(
               inputId = "selectgenus2c", 
               label = "",
               multiple  = T,
               choices  = levels(x2cdata2gencladesubfam()$genus),
               selected = levels(x2cdata2gencladesubfam()$genus)
             ), #select
             actionButton("selectallgen2c", label="Select/Deselect all"),
             HTML("<br></br>"),
             span("Mantain any genus(era), or clic in the field to delete (with keyboard) or reselect (mouse) any"),
             HTML("<br></br>"),
             span("If nothing is selected all the sample is shown")
        ) #box
      }
    } # end histogram
      else # violinplot violinplot violinplot violinplot
      wellPanel(width = 12,
                sliderInput("x2cadjustgen",
                            "bandwidth adjust:",
                            0.1,
                            1,
                            0.5, round=F, ticks=F, step=0.1)
      ) #well
  })
  output$threeboxes2cgen<-renderUI({
    validate(
      need(try(!invalid(input$par2ctypegen)), "Wait 656")
    )
    if(viohistogen()=="histo")
      fluidRow(
        column(4,uiOutput("x2ctabboxgen2") ),
        column(4,uiOutput("x2ctabboxgen3") ),
        column(4,uiOutput("x2ctabboxgen4") )
      )
  })
  output$threeboxes2cfam<-renderUI({
    validate(
      need(try(!invalid(input$par2ctypefam)), "Wait 656")
    )
    if(viohistofam()=="histo")
      fluidRow(
        column(4,uiOutput("x2ctabboxfam2") ),
        column(4,uiOutput("x2ctabboxfam3") ),
        column(4,uiOutput("x2ctabboxfam4") )
      )
  })
  output$optionsclades2nboxfam <- renderUI({
    validate(
      need(try(!invalid(input$par2ntypefam)), "Wait 658")
    )
    if(input$par2ntypefam=="dotplot")
      wellPanel(width = 12,
                sliderInput("size2ndotplotfam","Modify width of graph",20,40,28,step=2, ticks = FALSE)     
      )
  })
  output$x2ctabboxfam = renderUI({
    if(input$par2ctypefam=="histogram")
    {
      wellPanel(width=12, 
                radioButtons("histoordensityfam","Choose type of graph",c("Histogram","Density")),
                checkboxInput("par2cgridfam","Show as grid",value=TRUE),
                selectizeInput(
                  inputId = "selectfamily2c", 
                  label = "",
                  multiple  = T,
                  choices  = levels(x2cdata2famclade()$family_apg),
                  selected = levels(x2cdata2famclade()$family_apg)
                ), #select
                actionButton("selectallfam2c", label="Select/Deselect all"),
                HTML("<br></br>"),
                span("Mantain any family(ies), or clic in the field to delete (with keyboard) or reselect (mouse) any"),
                HTML("<br></br>"),
                span("If nothing is selected all the sample is shown")
      ) #end well 
    }
    else # violinplot violinplot violinplot violinplot
      wellPanel(width = 12,
                sliderInput("x2cadjustfam",
                            "bandwidth adjust:",
                            0.1,
                            1,
                            0.5, round=F, ticks=F, step=0.1)
      ) #well
  })
  output$familyselector <- renderUI({
    available <- x2ndata[x2ndata$apgorder == input$apgcladeinputgenclade, "family_apg"]
    selectInput(
      inputId = "apgcladeinputgenfam", 
      label = "family:",
      choices = unique(available),
      selected = unique(available)[1])
  })
  output$selectinputapgcladeinputgenfam2c <- renderUI({
    available <- x2cdata[x2cdata$apgorder == input$apgcladeinputgenclade2c, "family_apg"]
    selectInput(
      inputId = "apgcladeinputgenfam2c", 
      label = "family:",
      choices = unique(available),
      selected = unique(available)[1])
  })
  output$genuspanel <- renderUI({
    fluidRow(
      HTML("<div class='col-sm-3' style='padding:0px 0px 0px 0px;'>"),
      box(width=12, title="Options", solidHeader =T, status="primary",
          selectInput('apgcladeinputgenclade','Clade',  width="100%", levels(x2ndata$apgorder), 
                      selected=levels(x2ndata$apgorder)[2]
          ) #end input 
          ,uiOutput("familyselector")
         ,uiOutput("selectinputapgcladeinputgensubfam")
                    ,radioButtons("par2ntypegen","",c("dotplot","histogram"),"dotplot")
          ,uiOutput("optionsclades2nboxgen"), 
          uiOutput("selectgenbox"),
          actionButton('tablehistogenbutton',strong('Generate table of histogram data'),  icon("hand-pointer-o"),
                       style="color: #000; background-color: #0099ff; border-color: #2e6da4"
          )
      ) # end box
 ,HTML("</div>") ,
 HTML("<div class='col-sm-9' style='padding:0px 0px 0px 0px;'>"),
 uiOutput("threeboxesgen"),
 uiOutput("x2nhistogenbox")#,
 ,HTML("</div>")
    ) #fluidrow
  })
  output$threeboxesgen<-renderUI({
    validate(
      need(try(!invalid(input$par2ntypegen)), "Wait 656")
    )
    if(dothistogen()=="histo")
      fluidRow(
        column(4,uiOutput("optionsclades2nbox2gen") ),
        column(4,uiOutput("optionsclades2nbox3gen") ),
        column(4,uiOutput("optionsclades2nbox4gen") )
      )
  })
  output$optionsclades2nboxgen <- renderUI({
      validate(
        need(try(!invalid(input$par2ntypegen)), "Wait 681")
      )
      if(input$par2ntypegen=="dotplot")
        wellPanel(width = 12,
                  sliderInput("size2ndotplotgen","Modify width of graph",20,40,28,step=2, ticks = FALSE)     
        )
  })
  output$optionsclades2nbox <- renderUI({
      if(input$par2ntype=="histogram")
        {
        wellPanel(width=12,
        checkboxInput("par2ngrid","Show as grid",value=TRUE),
        checkboxGroupInput('apgcladeinput','Select clades', levels(x2ndata$apgorder), selected=levels(x2ndata$apgorder) ) ,
        actionButton("selectallclades", label="Select/Deselect all"),
        HTML("<br></br>"),
        actionButton("selectmonocots", label="Select/Deselect monocots"),
        actionButton("selecteudicots", label="Select/Deselect eudicots"),
        HTML("<br></br>"),
        span("If nothing is selected all the sample is shown")
    ) # end 
  }
else
  wellPanel(width = 12,
         sliderInput("size2ndotplot","Modify width of graph",20,40,28,step=2, ticks = FALSE)     
  )
})
  dothisto<-reactive({
    validate(
      need(try(!invalid(input$par2ntype)), "Wait 658")
    )
    if(input$par2ntype=="dotplot")
      {dothisto<-"dot"}
    else{
      {dothisto<-"histo"}
    }
  })
  viohisto<-reactive({
    validate(
      need(try(!invalid(input$par2ctype)), "Wait 792")
    )
    if(input$par2ctype=="violinplot")
    {o<-"vio"}
    else{
    {o<-"histo"}
    }
  })
  viohistofam<-reactive({
    validate(
      need(try(!invalid(input$par2ctypefam)), "Wait 792")
    )
    if(input$par2ctypefam=="violinplot")
    {o<-"vio"}
    else{
      {o<-"histo"}
    }
  })
  viohistogen<-reactive({
    validate(
      need(try(!invalid(input$par2ctypegen)), "Wait 792")
    )
    if(input$par2ctypegen=="violinplot")
    {o<-"vio"}
    else{
      {o<-"histo"}
    }
  })
  dothistofam<-reactive({
    validate(
      need(try(!invalid(input$par2ntypefam)), "Wait 658")
    )
    if(input$par2ntypefam=="dotplot")
    {dothistofam<-"dot"}
    else{
      {dothistofam<-"histo"}
    }
  })
  dothistogen<-reactive({
    validate(
      need(try(!invalid(input$par2ntypegen)), "Wait 658")
    )
    if(input$par2ntypegen=="dotplot")
    {dothistogen<-"dot"}
    else{
      {dothistogen<-"histo"}
    }
  })
  output$optionsclades2nbox2 <- renderUI({
    sliderInput("bins2n",
                "Bin width:",
                min = 1,
                max = 5,
                value = 1, step=1, ticks=F)
    })
  output$optionsclades2nbox3 <- renderUI({
    radioButtons("x2nlegendtype","Type of legend",c("median and counts per taxa","3 modes and counts"))
  })
  output$threeboxes<-renderUI({
    validate(
      need(try(!invalid(input$par2ntype)), "Wait 656")
    )
    if(dothisto()=="histo")
  fluidRow(
    column(4,uiOutput("optionsclades2nbox2") ),
    column(4,uiOutput("optionsclades2nbox3") ),
    column(4,uiOutput("optionsclades2nbox4") )
  )
  })
  output$threeboxes2c<-renderUI({
    validate(
      need(try(!invalid(input$par2ctype)), "Wait 656")
    )
    if(viohisto()=="histo")
      fluidRow(
        column(4,uiOutput("x2ctabbox2") ),
        column(4,uiOutput("x2ctabbox3") ),
        column(4,uiOutput("x2ctabbox4") )
      )
  })
  output$threeboxesfam<-renderUI({
    validate(
      need(try(!invalid(input$par2ntypefam)), "Wait 656")
    )
    if(dothistofam()=="histo")
      fluidRow(
        column(4,uiOutput("optionsclades2nbox2fam") ),
        column(4,uiOutput("optionsclades2nbox3fam") ),
        column(4,uiOutput("optionsclades2nbox4fam") )
      )
  })
  output$optionsclades2nbox4 <- renderUI({ 
    validate(
      need(min2n() != "", "Wait 213")
    )
    sliderInput("range2n",
                "2n:",
                min = 50,#min2n(),
                max = max2n(),
                value = c(max2n()), ticks=F, round=F, step=1)
  })
  output$selectcountryui <- renderUI({
    shinyjs::hidden(
      div(id = "countryoptionsbox",
          box( id ="boxcountry",
               width  = 12, 
               height = "100%",
               solidHeader = F, 
               selectizeInput(
                 inputId = "selectcountry", 
                 label = "Select Region",
                 multiple  = T,
                 choices = levels(countryduplicated$provspecific),
                 selected = levels(countryduplicated$provspecific)
               ), #select
               HTML("<br></br>"),
               span("Mantain any region(s), or clic in the field to delete (with keyboard) or reselect (mouse) any"),
               HTML("<br></br>"),
               span("If nothing is selected all the sample is shown")
          ) #box
      ) #div
    ) #end shinyjs
  })
  output$showallcolumnsbutton <- renderUI({
    actionButton("showallcolumns", label = label(),
                 icon("hand-pointer-o"),
                 style="color: #000; background-color: #0099ff; border-color: #2e6da4" 
    )
  })
  output$showallcolumnshistogenbutton <- renderUI({
    actionButton("showallcolumnshistogen", label = labelhistogen(),
                 icon("hand-pointer-o"),
                 style="color: #000; background-color: #0099ff; border-color: #2e6da4" 
    )
  })
  observe({
    if(!is.null(input$showallcolumns)){
      input$showallcolumns
      isolate({
        vars$counter <- vars$counter + 1
      })
    }
  })
  label <- reactive({
    if(!is.null(input$showallcolumns)){
      if( ( (vars$counter+1)/2) %% 2 == 0) label <- "Hide some columns"
      else label <- "Show hidden columns"
    }
  })
  observe({
    if(!is.null(input$showallcolumnshistogen)){
      input$showallcolumnshistogen
      isolate({
        varshistogen$counter <- varshistogen$counter + 1
      })
    }
  })
  labelhistogen <- reactive({
    if(!is.null(input$showallcolumnshistogen)){
      if( ( (varshistogen$counter+1)/2) %% 2 == 0) label <- "Hide some columns"
      else label <- "Show hidden columns"
    }
  })
  output$bubbletechniqueplot <- renderPlot({
    x= summarydata()$variable
    x <- factor(x, levels = standardnamestechniques )
    lx=length(levels(x))
    serie1=(c(1:lx))
    y=as.factor(summarydata()$range)
    ly=length(levels(y))
    serie2=(c(1:ly))
    g <- ggplot(summarydata(), aes(y, x)  ) + geom_point(aes(size = N), colour = "green") + 
      theme_bw() + xlab(expression(atop("Year intervals", paste(""["bubble size indicates number of plants analysed"] ) ) ) ) + ylab(expression(atop("Cytogenetic", paste("data"))))
    g2= g+ scale_size_continuous(range=c(0,15))  + geom_text(aes(label = N)) + theme(legend.position = 'none') #+ theme(panel.background = element_rect(fill = "#cDcdcd"))
    g3 = g2 +  themePGMwPBe4tityA0S15titxS15M20ptPtitS2AxtexS10 + scalexLwrap + themeLtexS13 +themeLtitS13
    g3
  }) #end plot
  summaryaffi<-reactive({ 
    if (!invalid(input$provenancecheckbox)){ 
    affiselectcolumn<-affiselectcolumn[which(affiselectcolumn$provspecific  %in% input$provenancecheckbox              ),]
    }
    else {
      affiselectcolumn<-affiselectcolumn#[which(affiselectcolumn$provspecific  %in% input$provenancecheckbox              ),]
        }
  data<-affiselectcolumn
  summaryaffi<-data %>% 
    group_by_at(.vars=c("affispecific", "range2")) %>%
    summarise(
      countacc = n(), count=n_distinct(uniquecite)#N    = sum(!is.na(value)) 
    ) %>% 
    bind_rows(
      data %>% 
        group_by_at("affispecific")  %>%
        summarise(
          countacc = n(), count=n_distinct(uniquecite)#N    = sum(!is.na(value)) 
        ) %>% 
        mutate(range2 = "All years")
    )
  summaryaffi[summaryaffi==0] <- NA
  summaryaffi$range2=as.factor(summaryaffi$range2)
  summaryaffi <- as.data.frame(summaryaffi)[, 1:4]
  })
  summarydata= reactive({
    data<-countrydataselected()
    sumdata<- data %>% 
      group_by_at(.vars=c("range","variable") ) %>%
      summarise(
        N    = sum(!is.na(value)) 
      ) %>% 
      bind_rows(
        data %>% 
          group_by_at("variable")  %>%
          summarise(
            N    = sum(!is.na(value)) 
          ) %>% 
          mutate(range = "All years")
      )
    sumdata[sumdata==0] <- NA
    sumdata$range=as.factor(sumdata$range)
    sumdata
  }) #end reactive
  bubblevalues<-reactiveValues()
    observe({
      validate(
        need(try(!invalid(input$referencesoracc)), "Wait 959")
      )
    if(input$referencesoracc=="ref"){
      bubblevalues$countbubble<-"count"
      bubblevalues$legendbubble<-  "bubble size indicates number of references"
      }
    else if(input$referencesoracc=="acc")
      bubblevalues$countbubble<-"countacc"
    bubblevalues$legendbubble<-  "bubble size indicates number of accessions"
  })
  output$bubbleaffiplot <- renderPlot({
    y=summaryaffi()$range2
    x=factor(summaryaffi()$affispecific)
    droplevels(x)
    standardnamescountry<-intersect(standardnamescountryaffi,levels(x) )
    x <- factor(x, levels = standardnamescountry)
    droplevels(x)
    g <- ggplot(summaryaffi(), aes(y, x)  ) + geom_point(aes_string(size = bubblevalues$countbubble), colour = "green") + theme_bw() + 
     xlab("Year intervals" )  + ylab("Author's\n\naffiliation\n\ncountry")   
        g2= g+ scale_size_continuous(range=c(0,15))  + geom_text(aes_string(label = bubblevalues$countbubble)) + theme(legend.position = 'none') #+ theme(panel.background = element_rect(fill = "#cDcdcd"))
    g3 = g2 +  themePGMwPBe4tityA0S15titxS15M20ptPtitS2AxtexS10+ scalexLwrap+themeLtexS13+themeLtitS13 #theme(panel.grid.major = element_line(colour = "white"))+ theme(
    g3
  }) #end plot 2
  lenplots<-reactiveValues()
  observe({
    validate(
      need(try(!invalid(input$par2ntype)), "Wait 93")
    )
    if(input$par2ntype=="dotplot")
    {
      lenplots$h<-  paste("750px")
      lenplots$heightofplot1<-600
    }
    else{
      validate(
        need(try(!invalid(input$par2ngrid)), "Wait 105")
      ) 
      if ( !invalid(x2ndata2()) ){  
        lenplots$interval<-intervals[length(levels(x2ndata2()$apgorder))]
        if(input$par2ngrid)
        {
          lenplots$h<-paste0(lenplots$interval*300+200,"px")
          lenplots$heightofplot1<-lenplots$interval*300+80
        }
        else
        {
          lenplots$h<-  paste("750px")
          lenplots$heightofplot1<-650   
        }
      }
    }
  }) # end observe 2n
  observe({
    validate(
      need(try(!invalid(input$par2ctype)), "Wait 93")
    )
    if(input$par2ctype=="violinplot")
    {
      lenplots$h2c<-  paste("750px")
      lenplots$heightofplot12c<-600
    }
    else{
      validate(
        need(try(!invalid(input$par2cgrid)), "Wait 105")
      ) 
      if ( !invalid(x2cdata2()) ){  
        lenplots$interval2c<-intervals[length(levels(x2cdata2()$apgorder))]
        if(input$par2cgrid)
        {
          lenplots$h2c<-paste0(lenplots$interval2c*300+200,"px")
          lenplots$heightofplot12c<-lenplots$interval2c*300+80
        }
        else
        {
          lenplots$h2c<-  paste("750px")
          lenplots$heightofplot12c<-650   
        }
      }
    }
  }) # end observe 2c
  observe({
    validate(
      need(try(!invalid(input$par2ctypefam)), "Wait 93")
    )
    if(input$par2ctypefam=="violinplot")
    {
      lenplots$interval2cfam<-intervals[length(levels(x2cdata2fam()$family_apg))]
      lenplots$h2cfam<-  paste0(lenplots$interval2cfam*120+200,"px") #paste("750px")
      lenplots$heightofplot12cfam<-lenplots$interval2cfam*120+80
    }
    else{
      validate(
        need(try(!invalid(input$par2cgridfam)), "Wait 105")
      ) 
      if ( !invalid(x2cdata2fam()) ){  
        lenplots$interval2cfam<-intervals[length(levels(x2cdata2fam()$family_apg))]
        if(input$par2cgridfam)
        {
          lenplots$h2cfam<-paste0(lenplots$interval2cfam*300+200,"px")
          lenplots$heightofplot12cfam<-lenplots$interval2cfam*300+80
        }
        else
        {
          lenplots$h2cfam<-  paste("750px")
          lenplots$heightofplot12cfam<-650   
        }
      }
    }
  }) # end observe 2cfam
  observe({
    validate(
      need(try(!invalid(input$par2ntypefam)), "Wait 93")
    )
    if(input$par2ntypefam=="dotplot")
    {
      lenplots$intervalfam<-intervals[length(levels(x2ndata2fam()$family_apg))]
      lenplots$hfam<-  paste0(lenplots$intervalfam*120+200,"px") #paste("750px")
      lenplots$heightofplot1fam<-lenplots$intervalfam*120+80
    }
    else{
      validate(
        need(try(!invalid(input$par2ngridfam)), "Wait 105")
      ) 
      if ( !invalid(x2ndata2fam()) ){  
        lenplots$intervalfam<-intervals[length(levels(x2ndata2fam()$family_apg))]
        if(input$par2ngridfam)
        {
          lenplots$hfam<-paste0(lenplots$intervalfam*300+200,"px")
          lenplots$heightofplot1fam<-lenplots$intervalfam*300+80
        }
        else
        {
          lenplots$hfam<-  paste("750px")
          lenplots$heightofplot1fam<-650   
        }
      }
    }
  }) # end observe 2n fam
  observe({
    validate(
      need(try(!invalid(input$par2ntypegen)), "Wait 93")
    )
    if(input$par2ntypegen=="dotplot")
    {
      lenplots$intervalgen<-intervals[length(levels(x2ndata2gen()$genlabelcount))]
      lenplots$hgen<-  paste0(lenplots$intervalgen*120+200,"px") #paste("750px")
      lenplots$heightofplot1gen<-lenplots$intervalgen*120+80
    }
    else{
      validate(
        need(try(!invalid(input$par2ngridgen)), "Wait 105")
      ) 
      if ( !invalid(x2ndata2gen()) ){  
        lenplots$intervalgen<-intervals[length(levels(x2ndata2gen()$genlabelcount))]
        if(input$par2ngridgen)
        {
          lenplots$hgen<-paste0(lenplots$intervalgen*300+200,"px")
          lenplots$heightofplot1gen<-lenplots$intervalgen*300+80
        }
        else
        {
          lenplots$hgen<-  paste("750px")
          lenplots$heightofplot1gen<-650   
        }
      }
    }
  }) # end observe 2n fam
  observe({
    validate(
      need(try(!invalid(input$par2ctypegen)), "Wait 93")
    )
    validate(
      need(try(!invalid(x2cdata2gen())), "Wait 93")
    )
    if(input$par2ctypegen=="violinplot")
    {
      lenplots$interval2cgen<-intervals[length(levels(x2cdatacustomlabelgen()$gen2clabelcount))]
      lenplots$h2cgen<-  paste0(lenplots$interval2cgen*120+200,"px") #paste("750px")
      lenplots$heightofplot12cgen<-lenplots$interval2cgen*120+80
    }
    else{ # histogram or density
      validate(
        need(try(!invalid(input$par2cgridgen)), "Wait 105")
      ) 
      if ( !invalid(x2cdata2gen()) ){  
        lenplots$interval2cgen<-intervals[length(levels(x2cdata2gen()$gen2clabelcount))]
        if(input$par2cgridgen)
        {
          lenplots$h2cgen<-paste0(lenplots$interval2cgen*300+200,"px")
          lenplots$heightofplot12cgen<-lenplots$interval2cgen*300+80
        }
        else
        {
          lenplots$h2cgen<-  paste("750px")
          lenplots$heightofplot12cgen<-650   
        }
      }
    }
  }) # end observe 2c fam
  observe({
    validate(
      need(try(!invalid(input$radiohisto)), "Wait 93")
    )
    if(input$radiohisto!="2C - Genome size") # 2n
    {
      validate(
        need(try(!invalid(input$par2ntypequery)), "Wait 93"),
        need(try(!invalid(querygenera$tbl)), "Wait 1460")
      )
    if(input$par2ntypequery=="dotplot")
    {
      lenplots$intervalquery<-intervals[length(levels(querygenera$tbl$genlabelcountquery))]
      lenplots$hquery<-  paste0(lenplots$intervalquery*120+200,"px") #paste("750px")
      lenplots$heightofplot1query<-lenplots$intervalquery*120+80
    }
    else{ # histogram
      validate(
        need(try(!invalid(input$par2ngridquery)), "Wait 105")
      ) 
      if ( !invalid(querygenera$tbl) ){  
        lenplots$intervalquery<-intervals[length(levels(querygenera$tbl$genlabelcountquery))]
        if(input$par2ngridquery)
        {
          lenplots$hquery<-paste0(lenplots$intervalquery*300+200,"px")
          lenplots$heightofplot1query<-lenplots$intervalquery*300+80
        }
        else
        {
          lenplots$hquery<-  paste("750px")
          lenplots$heightofplot1query<-650   
        }
      }
    } # end histogram
    } # end 2n
    else if(input$radiohisto=="2C - Genome size")
    {
    validate(
      need(try(!invalid(input$par2ctypequery)), "Wait 93")
    )
    validate(
      need(try(!invalid(querygenera$tbl)), "Wait 93")
    )
    if(input$par2ctypequery=="violinplot")
    {
      lenplots$intervalquery<-intervals[length(levels(x2cdatacustomlabelquery()$query2clabelcountcustom))]
      lenplots$hquery<-  paste0(lenplots$intervalquery*120+200,"px") #paste("750px")
      lenplots$heightofplot1query<-lenplots$intervalquery*120+80
    }
    else{ # histogram or density
      validate(
        need(try(!invalid(input$par2cgridquery)), "Wait 105")
      ) 
      if ( !invalid(querygenera$tbl) ){  
        lenplots$intervalquery<-intervals[length(levels(querygenera$tbl$gen2clabelcountquery))]
        if(input$par2cgridquery)
        {
          lenplots$hquery<-paste0(lenplots$intervalquery*300+200,"px")
          lenplots$heightofplot1query<-lenplots$intervalquery*300+80
        }
        else
        {
          lenplots$hquery<-  paste("750px")
          lenplots$heightofplot1query<-650   
        }
      }
    } # end histogram
    } # end 2c
  }) # end observe 2c 
  output$x2nhistoplot <- renderPlot({
    validate(
      need(try(!invalid(input$par2ntype)), "Wait 787")
    )
    if(input$par2ntype=="dotplot"){
      datasetplotx2n<-
        gridplot(x2ndatatotal,
                 "labelcount",
                 "parsed2n",10,
                 "Chromosome number 2n",1,105,input$size2ndotplot) # 2.5
      print(datasetplotx2n)
    }
    else{ # histogram
      validate(
        need(try(!invalid(input$par2ngrid)), "Wait 787")
      )  
      if(input$par2ngrid==FALSE)
      {
      i<- scale_y_continuous(limits=c(0,maxy2n()), expand=c(0,0),breaks = pby2n())
      j<-  scale_x_continuous(breaks = pbx2n()) 
      }
      else{
      i <-scaleyconE0P10#scale_y_continuous(expand=c(0,0),breaks = pretty_breaks(10))
      j <-  scalexB10E0#scale_x_continuous(breaks = pretty_breaks(10), expand = c(0,0))
      }
    d<- theme(legend.title=element_text(family="CourierNew",size=rel(1.1) ),
              legend.text=element_text(family="CourierNew", size=rel(1.1)) )
    validate(
      need(try(!invalid(input$x2nlegendtype)), "Wait 458")
    ) 
    if(input$x2nlegendtype== "median and counts per taxa")
    { 
      x= x2ndata2()$ordmedcoufamcougencou
      title<-"APG Clade                                    2n-median fam gen counts"
    }
      else #modes
      {
        x= x2ndata2()$labelcount
        title<-"APG Clade                                          modes (3)     counts"
      }
      plota=ggplot(x2ndata2(), aes(parsed2n)) + geom_histogram(binwidth=input$bins2n, aes(fill=x) ) + i + j + expandl050010+
      scale_fill_manual(name=title, values=cbPalette) # , breaks=levels(as.factor(x2ndata2()$apgorderN) ) )    
      plotb = plota + labs(x = "parsed 2n", y = " Frequency ")#, fill ="APG Clade                       median")
      g3 = plotb +themeslxS05CblyS05Cb +themePGMyblank +themelegJtPbDvBh +d + themePGMwPBe4tityA0S15titxS15M20ptPtitS2AxtexS10 #+ coord_fixed(ratio = 0.8)
    if(input$par2ngrid)
    {
      g3+ facet_wrap(~ labelcount, ncol = 3, scales = "free")
    }
    else
    {
      g3
    }
    }
  }) #end plot 3
  x2cviolinplot<-reactive({
    violinplot(x2cdatacustomlabel(),"parsed2c", "labelcountcustom",input$x2cadjust,15)#x2cadjustreac(),15)#x2cadjustreac(),15)#input$papimax2c)
  })
  x2cviolinplotfam<-reactive({
    violinplot(x2cdatacustomlabelfam(),"parsed2c", "fam2clabelcountcustom",input$x2cadjustfam,15)#x2cadjustreacfam(),15)#x2cadjustreac(),15)#input$papimax2c)
  })
  x2cviolinplotgen<-reactive({
    tryCatch(violinplot(x2cdatacustomlabelgen(),"parsed2c", "gen2clabelcountcustom",input$x2cadjustgen,15)#x2cadjustreacgen(),15)#x2cadjustreac(),15)#input$papimax2c)
,    error = function(e) {print(paste("not enough data for plot")) ; "not enough data for plot" }   )
})
  x2cviolinplotquery<-reactive({
    tryCatch(violinplot(x2cdatacustomlabelquery(),"parsed2c", "query2clabelcountcustom",input$x2cadjustquery,15)#x2cadjustreacquery(),15)#x2cadjustreac(),15)#input$papimax2c)
    ,    error = function(e) {print(paste("not enough data for plot")) ; "not enough data for plot" }   )
  })
  x2cadjustreac<-reactive({
    f<-input$x2cadjust
  })
  observeEvent(input$x2cadjust,{
    updateSliderInput(session, "x2cadjust", "bandwidth adjust:", x2cadjustreac(), 0.1,1,0.1
    )
  }) 
  x2cadjustreacdensity<-reactive({
    f<-input$x2cadjustdensity
  })
  observeEvent(input$x2cadjustdensity,{
    updateSliderInput(session, "x2cadjustdensity", "bandwidth adjust:", x2cadjustreacdensity(), 0.1,1,0.1
    )
  }) 
  x2cadjustreacdensityquery<-reactive({
    f<-input$x2cadjustdensityquery
  })
  observeEvent(input$x2cadjustdensityquery,{
    updateSliderInput(session, "x2cadjustdensityquery", "bandwidth adjust:", x2cadjustreacdensityquery(), 0.1,1,0.1
    )
  }) 
  x2cadjustreacfam<-reactive({
    f<-input$x2cadjustfam
  })
  observeEvent(input$x2cadjustfam,{
    updateSliderInput(session, "x2cadjustfam", "bandwidth adjust:", x2cadjustreacfam(), 0.1,1,0.1
    )
  }) 
  x2cadjustreacgen<-reactive({
    f<-input$x2cadjustgen
  })
  observeEvent(input$x2cadjustgen,{
    updateSliderInput(session, "x2cadjustgen", "bandwidth adjust:", x2cadjustreacgen(), 0.1,1,0.1
    )
  }) 
  x2cadjustreacquery<-reactive({
    f<-input$x2cadjustquery
  })
  observeEvent(input$x2cadjustquery,{
    updateSliderInput(session, "x2cadjustquery", "bandwidth adjust:", x2cadjustreacquery(), 0.1,1,0.1
    )
  }) 
  x2cadjustreacdensityfam<-reactive({
    f<-input$x2cadjustdensityfam
  })
  observeEvent(input$x2cadjustdensityfam,{
    updateSliderInput(session, "x2cadjustdensityfam", "bandwidth adjust:", x2cadjustreacdensityfam(), 0.1,1,0.1
    )
  }) 
  x2cadjustreacdensitygen<-reactive({
    f<-input$x2cadjustdensitygen
  })
  observeEvent(input$x2cadjustdensitygen,{
    updateSliderInput(session, "x2cadjustdensitygen", "bandwidth adjust:", x2cadjustreacdensitygen(), 0.1,1,0.1
    )
  }) 
  output$x2chistoplot <- renderPlot({
    validate(
      need(try(!invalid(input$par2ctype)), "Wait 787")
    )
    if(input$par2ctype=="violinplot"){
    print(x2cviolinplot())
    }
    else{ # histogram or density
        validate(
          need(try(!invalid(input$histoordensity)), "Wait 304")
        )   
    a<- labs(x = "parsed 2C (pg)", y = " Frequency ") #, fill ="APG Clade                       median")
    e<-theme(legend.title=element_text(family="CourierNew",
                                       size=rel(1.1)), legend.text=element_text(family="CourierNew") ) 
    f<-  theme(legend.text=element_text(size=rel(1.1))) 
    if(input$histoordensity== "Histogram")
    { 
      if(input$par2cgrid==FALSE)
      {
        a1<- scale_y_continuous(limits=c(0, maxy2c()),expand=c(0,0),breaks = pby2c() )  
        b1<-  scale_x_continuous(breaks = pbx2c() )#scales::pretty_breaks(n=15))  
        }
      else{ # grid TRUE
        a1<-scaley2rE0B10#scale_y_sqrt(expand=c(0,0),breaks = pretty_breaks(10)) 
        b1<-  scalexB10E0#scale_x_continuous(breaks = pretty_breaks(10), expand = c(0,0)) 
      }
      validate(
        need(try(!invalid(input$x2clegendtype)), "Wait 1266")
      )
      if(input$x2clegendtype== "median and counts per taxa")
      { 
        x= x2cdata2()$ordmedcoufamcougencou
        x= as.factor(x)
        title<-"APG Clade                                     2C-median fam gen counts"
      }
      else{
        x= x2cdata2()$labelcount
        x= as.factor(x)
        title<-"APG Clade                                               modes (3)    counts"
      }
      plota=ggplot(x2cdata2(), aes(parsed2c)) + geom_histogram(binwidth=input$bins2c, aes(fill=x)) + a1+ b1+ expandl0505+
        scale_fill_manual(name=title, values=cbPalette) # , breaks=levels(as.factor(x2ndata2()$apgorderN) ) )    
      g3= plota+a+themeslxS05CblyS05Cb+themePGMyblank+themelegJtPbDvBh+e+themePGMwPBe4tityA0S15titxS15M20ptPtitS2AxtexS10+f
    }
    else # density , use x2cdatacustomlabeldensity
    {
      a<- labs(x = "parsed 2C (pg)", y = " Density ") #, fill ="APG Clade                       median")
      if(input$x2clegendtype== "median and counts per taxa")
      { 
        x= x2cdatacustomlabeldensity()$ordmedcoufamcougencou
        x= as.factor(x)
        title<-"APG Clade                                     2C-median fam gen counts"
      }
      else{ #modes
        x= x2cdatacustomlabeldensity()$labelcountcustom
        x= as.factor(x)
        title<-"APG Clade                                               modes (3)    counts"
      }
      plota2<-ggplot(x2cdatacustomlabeldensity(), aes(parsed2c, fill = x) ) + geom_density(adjust=input$x2cadjustdensity,#x2cadjustreacdensity(), #0.5
        alpha = 0.4 ) + scalexB10E0 +
        scale_y_continuous(expand=c(0,0),breaks = pby2c() ) + 
        scale_fill_manual(name=title, values=cbPalette) # , breaks=levels(as.factor(x2ndata2()$apgorderN) ) )    
      g3= plota2+a+themeslxS05CblyS05Cb+themePGMyblank+themelegJtPbDvBh+e+themePGMwPBe4tityA0S15titxS15M20ptPtitS2AxtexS10+f+expandl050+themePGMwPBe4tityA0S15titxS15M20ptPtitS2AxtexS10
    } # end density
    if(input$par2cgrid)
    {
      g3+ facet_wrap(~ labelcount, ncol = 3, scales = "free")
    }
    else # GRID FALSE
    {
      g3
    }
   } # end histogram (no violinplot)
  } ) #end plot 2c-1
  output$x2nhistofamplot <- renderPlot({
    validate(
      need(try(!invalid(input$par2ntypefam)), "Wait 787")
    )
    validate(
      need(try(!invalid(x2ndata2fam())), "Wait 787")
    )
    if(input$par2ntypefam=="dotplot"){
      validate(
        need(try(!invalid(input$apgcladeinputfam)), "Wait 787")
      )
      datasetplotx2nfam<-
        gridplot(x2ndata2fam(),
                 "famlabelcount",
                 "parsed2n",9,
                 "Chromosome number 2n",1,105,input$size2ndotplotfam) # 2.5
      print(datasetplotx2nfam)
    }
    else{ # histogram
      validate(
        need(try(!invalid(input$par2ngridfam)), "Wait 787")
      )  
      if(input$par2ngridfam==FALSE)
      {
        i<- scale_y_continuous(limits=c(0,maxy2nfam()), expand=c(0,0),breaks = pby2nfam())
        j<-  scale_x_continuous(breaks = pbx2nfam()) 
      }
      else{
        i <-scaleyconE0P10#scale_y_continuous(expand=c(0,0),breaks = pretty_breaks(10))
        j <-  scalexB10E0#scale_x_continuous(breaks = pretty_breaks(10), expand = c(0,0))
      }
    a<- labs(x = "parsed 2n", y = " Frequency ") #, fill ="APG Clade                       median")
    validate(
      need(try(!invalid(input$x2nlegendtypefam)), "Wait 458")
    ) 
    if(input$x2nlegendtypefam== "median and counts per taxa")
    { 
      x= x2ndata2fam()$fammedgencou
      title<-"APG Family    2n-median gen counts"
    }
    else #modes
    {
      x= x2ndata2fam()$famlabelcount
      title<-"APG Family                       modes (3)  counts"
    }
    plota=ggplot(x2ndata2fam(), aes(parsed2n)) + geom_histogram(binwidth=input$bins2nfam, aes(fill=x ) ) + i + j + expandl050010+
       scale_fill_manual(name=title, values=cbPalette) # , breaks=levels(as.factor(x2ndata2fam()$family_apg) ) )    
      g3 = plota +a+themeslxS05CblyS05Cb+themePGMyblank+themelegJtPbDvBh+themelegtitCou13texCou+themePGMwPBe4tityA0S15titxS15LtexS13PtitS2AxtexS10
      if(input$par2ngridfam)
      {
        validate(
          need(try(!invalid(x2ndata2fam())), "Wait 458")
        ) 
        g3+ facet_wrap(~ famlabelcount, ncol = 3, scales = "free")
      }
      else
      {
        g3
      }
    }
  }) #end plot 4
  output$x2chistofamplot <- renderPlot({
    validate(
      need(try(!invalid(input$par2ctypefam)), "Wait 787")
    )
    if(input$par2ctypefam=="violinplot"){
      print(x2cviolinplotfam())
    }
    else{ # histogram or density
      validate(
        need(try(!invalid(input$histoordensityfam)), "Wait 304")
    )   
    a<- labs(x = "parsed 2C (pg)", y = " Frequency ") #, fill ="APG Clade                       median")
     if(input$histoordensityfam== "Histogram")
    { 
      if(input$par2cgridfam==FALSE)
      {
        a1<- scale_y_continuous(limits=c(0, maxy2cfam()),expand=c(0,0),breaks = pby2cfam() )  
        b1<-  scale_x_continuous(breaks = pbx2cfam() )#scales::pretty_breaks(n=15))  
      }
      else{ # grid TRUE
        a1<-scaley2rE0B10#scale_y_sqrt(expand=c(0,0),breaks = pretty_breaks(10)) 
        b1<-  scalexB10E0#scale_x_continuous(breaks = pretty_breaks(10), expand = c(0,0)) 
      }
      validate(
        need(try(!invalid(input$x2clegendtypefam)), "Wait 1266")
      )
      if(input$x2clegendtypefam== "median and counts per taxa")
      { 
        x= x2cdata2fam()$fammedgencou
        x= as.factor(x)
        title<-"APG Family    2C-median gen counts"
      }
      else{ #modes 
        x= x2cdata2fam()$fam2clabelcount
        x= as.factor(x)
        title<-"APG Family          modes (3)     counts"
      }
      plota=ggplot(x2cdata2fam(), aes(parsed2c)) + geom_histogram(binwidth=input$bins2cfam, aes(fill=x) ) + a1+ b1+ expandl0505+#d1+
      scale_fill_manual(name=title, values=cbPalette) # , breaks=levels(as.factor(x2cdata2fam()$family_apg) ) )    
      g3= plota+a+themeslxS05CblyS05Cb+themePGMyblank+themelegJtPbDvBh+themelegtitCou13texCou+themePGMwPBe4tityA0S15titxS15M20ptPtitS2AxtexS10+themeLtexS13
      g3 #+ coord_fixed(ratio = 0.8)
    }
    else # density
    {
      a<- labs(x = "parsed 2C (pg)", y = " Density ") 
      if(input$x2clegendtypefam== "median and counts per taxa")
      { 
        x= x2cdatacustomlabeldensityfam()$fammedgencou
        x= as.factor(x)
        title<-"APG Family    2C-median gen counts"
      }
      else{ 
        x= x2cdatacustomlabeldensityfam()$fam2clabelcountcustom
        x= as.factor(x)
        title<-"APG Family          modes (3)     counts"
      }
      plota2<-ggplot(x2cdatacustomlabeldensityfam(), aes(parsed2c, fill = x) ) + geom_density(adjust=input$x2cadjustdensityfam#x2cadjustreacdensityfam()
      ,alpha = 0.4 )
      plota = plota2 +   scale_y_continuous(expand=c(0,0),breaks = pby2cfam() ) + 
        scale_x_continuous(breaks = pbx2cfam()) +     
        scale_fill_manual(name=title, values=cbPalette) # , breaks=levels(as.factor(x2ndata2()$apgorderN) ) )    
      g3= plota+a+themeslxS05CblyS05Cb+themePGMyblank+themelegJtPbDvBh+themelegtitCou13texCou+themePGMwPBe4tityA0S15titxS15M20ptPtitS2AxtexS10+expandl050+themeLtexS13
    } # end density
    if(input$par2cgridfam)
    {
      g3+ facet_wrap(~ fam2clabelcount, ncol = 3, scales = "free")
    }
    else # GRID FALSE
    {
      g3
    }
    } # end histogram (no violinplot)
  })
  output$x2nhistogenplot <- renderPlot({
    validate(
      need(try(!invalid(input$par2ntypegen)), "Wait 787")
    )
    validate(
      need(try(x2ndata2gen() != ""), "Wait 1300")
    )
    if(input$par2ntypegen=="dotplot"){
        validate(
        need(try(!invalid(input$apgcladeinputgenfam)), "Wait 787")
        )
       datasetplotx2ngen<-
                gridplot(x2ndata2gen(),
                "genlabelcount",
                 "parsed2n",9,
                 "Chromosome number 2n",1,105,input$size2ndotplotgen) # 2.5
      print(datasetplotx2ngen)
    }
    else{ # histogram
      validate(
        need(try(!invalid(input$par2ngridgen)), "Wait 1313")
      )  
      if(input$par2ngridgen==FALSE)
      {
        i<- scale_y_continuous(limits=c(0,maxy2ngen()), expand=c(0,0),breaks = pby2ngen())
        j<-  scale_x_continuous(breaks = pbx2ngen()) 
      }
      else{
        i <-scaleyconE0P10#scale_y_continuous(expand=c(0,0),breaks = pretty_breaks(10))
        j <-  scalexB10E0#scale_x_continuous(breaks = pretty_breaks(10), expand = c(0,0))
      }
    f<- labs(x = "parsed 2n", y = " Frequency ")
    validate(
      need(try(!invalid(input$x2nlegendtypegen)), "Wait 458")
    ) 
    if(input$x2nlegendtypegen== "median and counts per taxa")
    { 
      x= x2ndata2gen()$genmedgencou
      title="Genera          2n-median  counts"
    }
    else #modes
    {
      x= x2ndata2gen()$genlabelcount
      title="Genera                           modes(3)           counts"
    }
    plota= ggplot(x2ndata2gen(), aes(parsed2n)) + geom_histogram(binwidth=input$bins2ngen, aes(fill=x  ) ) + i+j+expandl050010+
    scale_fill_manual(name=title, values=cbPalette)
    g3<-plota+f+themePGMyblank+themeslxS05CblyS05Cb+themelegJtPbDvBh+themelegtitCou13texCou+themePGMwPBe4tityA0S15titxS15LtexS13PtitS2AxtexS10
    if(input$par2ngridgen)
    {
      validate(
        need(try(!invalid(x2ndata2gen())), "Wait 1364")
      ) 
      g3+ facet_wrap(~ genlabelcount, ncol = 3, scales = "free")
    }
    else
    {
      g3
    }
    }
}) #end plot 5
  output$x2chistogenplot <- renderPlot({
    validate(
      need(try(!invalid(input$par2ctypegen)), "Wait 787")
    )
    if(input$par2ctypegen=="violinplot"){
      print(x2cviolinplotgen())
    }
    else{ # histogram or density
      validate(
        need(try(!invalid(input$histoordensitygen)), "Wait 304")
      )   
    a<- labs(x = "parsed 2C (pg)", y = " Frequency ") #, fill ="APG Clade                       median")
    if(input$histoordensitygen== "Histogram")
    { 
      if(input$par2cgridgen==FALSE)
      {
        a1<- scale_y_continuous(limits=c(0, maxy2cgen()),expand=c(0,0),breaks = pby2cgen() )  
        b1<-  scale_x_continuous(breaks = pbx2cgen() )#scales::pretty_breaks(n=15))  
      }
      else{ # grid TRUE
        a1<-scaley2rE0B10#scale_y_sqrt(expand=c(0,0),breaks = pretty_breaks(10)) 
        b1<-  scalexB10E0#scale_x_continuous(breaks = pretty_breaks(10), expand = c(0,0)) 
      }
      validate(
        need(try(!invalid(input$x2clegendtypegen)), "Wait 1266")
      )
      if(input$x2clegendtypegen== "median and counts per taxa")
      { 
        x= x2cdata2gen()$genusmediancount
        x= as.factor(x)
        title="Genera        2C-median counts"
      }
      else{ 
        x= x2cdata2gen()$gen2clabelcount
        x= as.factor(x)
        title="Genera                modes(3)    counts"
      }
      plota= ggplot(x2cdata2gen(), aes(parsed2c)) + geom_histogram(binwidth=input$bins2cgen, aes(fill=x) ) +  a1+ b1+ expandl0505+#d1+
        scale_fill_manual(name=title,    values=cbPalette) # , breaks=levels(as.factor(x2cdata2fam()$family_apg) ) )    
      g3= plota +a+themeslxS05CblyS05Cb+themePGMyblank+themelegJtPbDvBh+themelegtitCou13texCou+themeLtexS13+themePGMwPBe4tityA0S15titxS15M20ptPtitS2AxtexS10 #+ coord_fixed(ratio = 0.8)
    }
    else # density
    {
      a<- labs(x = "parsed 2C (pg)", y = " Density ") 
      if(input$x2clegendtypegen== "median and counts per taxa")
      { 
        x= x2cdatacustomlabeldensitygen()$genusmediancount
        x= as.factor(x)
        title="Genera        2C-median counts"
      }
      else{ 
        x= x2cdatacustomlabeldensitygen()$gen2clabelcountcustom
        x= as.factor(x)
        title="Genera                modes(3)    counts"
      }
      plota2<-ggplot(x2cdatacustomlabeldensitygen(), aes(parsed2c, fill = x) ) + geom_density(adjust=input$x2cadjustdensitygen#x2cadjustreacdensitygen()
        ,alpha = 0.4 )
      plota = plota2 +   scale_y_continuous(expand=c(0,0),breaks = pby2cgen() ) + scale_x_continuous(breaks = pbx2cgen()) + 
        scale_fill_manual(name=title, values=cbPalette) # , breaks=levels(as.factor(x2ndata2()$apgorderN) ) )    
      g3= plota+a+themeslxS05CblyS05Cb+themePGMyblank+themelegJtPbDvBh+themelegtitCou13texCou+themePGMwPBe4tityA0S15titxS15M20ptPtitS2AxtexS10+themeLtexS13+expandl050
    } # end density
    if(input$par2cgridgen)
    {
      g3+ facet_wrap(~ gen2clabelcount, ncol = 3, scales = "free")
    }
    else # GRID FALSE
    {
      g3
    }
    } # end histogram (no violinplot)
    }) #end plot 2c gen
  observe({
    validate(
      need(try(!invalid(input$selectallclades)), "Wait 726")
    ) 
    if (input$selectallclades > 0) {
      if (input$selectallclades %% 2 == 0){
        updateCheckboxGroupInput(session=session, 
                                 'apgcladeinput','Select clades', levels(x2ndata$apgorder), selected=levels(x2ndata$apgorder)
        )
      }
      else {
        updateCheckboxGroupInput(session=session, 'apgcladeinput','Select clades', levels(x2ndata$apgorder),
                                 selected = c()
        )
      }
    }
  }) # end observe
  observe({
    validate(
      need(try(!invalid(input$selectallclades2c)), "Wait 689")
    )     
    if (input$selectallclades2c > 0) {
      if (input$selectallclades2c %% 2 == 0){
        updateCheckboxGroupInput(session=session, 
                                 'apgcladeinput2c','Clades', levels(x2cdata$apgorder), selected=levels(x2cdata$apgorder)
        )
      }
      else {
        updateCheckboxGroupInput(session=session, 'apgcladeinput2c','Clades', levels(x2cdata$apgorder),
                                 selected = c()
        )
      }
    }
  }) # end observe
  observe({
    validate(
      need(try(!invalid(input$selectmonocots)), "Wait 773")
    )   
    if (input$selectmonocots > 0) {
      if (input$selectmonocots %% 2 == 0){
        updateCheckboxGroupInput(session=session, 
                                 'apgcladeinput',
                                 'Select clades', 
                                 levels(x2ndata$apgorder), #  
                                 selected = c() #levels(x2ndata$apgorder)
        )
      }
      else {
        updateCheckboxGroupInput(session=session, 
                                 'apgcladeinput',
                                 'Select clades', 
                                 levels(x2ndata$apgorder),
                                 selected= grep(glob2rx("*onocot*"), levels(x2ndata$apgorder), value = TRUE)  
        )
      }
    }
  } ) # end observe
  observe({
    validate(
      need(try(!invalid(input$selectmonocots2c)), "Wait 743")
    )     
    if (input$selectmonocots2c > 0) {
      if (input$selectmonocots2c %% 2 == 0){
        updateCheckboxGroupInput(session=session, 
                                 'apgcladeinput2c',
                                 'Clades', 
                                 levels(x2cdata$apgorder), #  
                                 selected = c()
        )
      }
      else {
        updateCheckboxGroupInput(session=session, 
                                 'apgcladeinput2c',
                                 'Clades', 
                                 levels(x2cdata$apgorder),
                                 selected= grep(glob2rx("*onocot*"), levels(x2cdata$apgorder), value = TRUE)  
        )
      }
    }
  } ) # end observe
  observe({
    validate(
      need(try(!invalid(input$selecteudicots)), "Wait 834")
    )   
    if (input$selecteudicots > 0) {
      if (input$selecteudicots %% 2 == 0){
        updateCheckboxGroupInput(session=session, 
                                 'apgcladeinput',
                                 'Select clades', 
                                 levels(x2ndata$apgorder), #  
                                 selected = c() #levels(x2ndata$apgorder)
        )
      }
      else {
        updateCheckboxGroupInput(session=session, 
                                 'apgcladeinput',
                                 'Select clades', 
                                 levels(x2ndata$apgorder),
                                 selected= grep(glob2rx("*udicot*"), levels(x2ndata$apgorder), value = TRUE)  
        )
      }
    }
  } ) #end observe
  observe({
    validate(
      need(try(!invalid(input$selectamerica)), "Wait 920")
    )   
    if (input$selectamerica > 0) {
      if (input$selectamerica %% 2 == 0){
        updateSelectizeInput(session=session, 
                                 'countryselector',
                                 'Select Country', 
                             levels(as.factor(authorduplicated$countryalone)), #  
                                 selected = grep(glob2rx("Trinidad-and-Tobago|Brazil|Argentina|Colombia|U.S.A|Canada|Uruguay|Chile|Mexico|Paraguay"), levels(as.factor(authorduplicated$countryalone)), 
                                                 value = TRUE, invert=TRUE)
        )
      }
  
      else {
        updateSelectizeInput(session=session, 
                             'countryselector',
                             'Select Country', 
                             levels(as.factor(authorduplicated$countryalone)), #  
                             selected = grep(glob2rx("Trinidad-and-Tobago|Brazil|Argentina|Colombia|U.S.A|Canada|Uruguay|Chile|Mexico|Paraguay"), levels(as.factor(authorduplicated$countryalone)), 
                                             value = TRUE, invert=FALSE) 
        )
      }
    }
  } ) #end observe
  observe({
    validate(
      need(try(!invalid(input$selecteurope)), "Wait 920")
    )   
    if (input$selecteurope > 0) {
      if (input$selecteurope %% 2 == 0){
        updateSelectizeInput(session=session, 
                             'countryselector',
                             'Select Country', 
                             levels(as.factor(authorduplicated$countryalone)), #  

                             selected = grep(glob2rx("Austria|Belgium|Denmark|France|Germany|Italy|Netherlands|Spain|Sweden|Switzerland|United-Kingdom"), levels(as.factor(authorduplicated$countryalone)), 
                                             value = TRUE, invert=TRUE)
        )
      }
      else {
        updateSelectizeInput(session=session, 
                             'countryselector',
                             'Select Country', 
                             levels(as.factor(authorduplicated$countryalone)), #  
                             selected = grep(glob2rx("Austria|Belgium|Denmark|France|Germany|Italy|Netherlands|Spain|Sweden|Switzerland|United-Kingdom"), levels(as.factor(authorduplicated$countryalone)), 
                                             value = TRUE, invert=FALSE) 
        )
      }
    }
  } ) #end observe
  observe({
    validate(
      need(try(!invalid(input$selectrestworld)), "Wait 920")
    )   
    if (input$selectrestworld > 0) {
      if (input$selectrestworld %% 2 == 0){
        updateSelectizeInput(session=session, 
                             'countryselector',
                             'Select Country', 
                             levels(as.factor(authorduplicated$countryalone)), #  
                             # [1] "Algeria"        "Arabia"         "Argentina"      "Australia"      "Austria"        "Bangladesh"     "Belgium"        "Brazil"        
                             # [9] "Canada"         "Chile"          "China"          "Colombia"       "Denmark"        "Egypt"          "France"         "Germany"       
                             # [17] "Ghana"          "India"          "Israel"         "Italy"          "Japan"          "Korea"          "Mexico"         "NA"            
                             # [25] "Nepal"          "Netherlands"    "Nigeria"        "Pakistan"       "Paraguay"       "Philippines"    "Spain"          "Sweden"        
                             # [33] "Switzerland"    "Taiwan"         "Thailand"       "Tobago"         "United-Kingdom" "Uruguay"        "U.S.A"          "Vanuatu"       
                             # [41] "Vietnam"  
                             selected = grep(glob2rx("Algeria|Arabia|Australia|Bangladesh|China|Egypt|Ghana|Israel|India|Japan|Korea|NA|Nepal|Nigeria|Pakistan|Philippines|Taiwan|Thailand|Vanuatu|Vietnam"), levels(as.factor(authorduplicated$countryalone)), 
                                             value = TRUE, invert=TRUE)
        )
      }
      else {
        updateSelectizeInput(session=session, 
                             'countryselector',
                             'Select Country', 
                             levels(as.factor(authorduplicated$countryalone)), #  
                             selected = grep(glob2rx("Algeria|Arabia|Australia|Bangladesh|China|Egypt|Ghana|Israel|India|Japan|Korea|NA|Nepal|Nigeria|Pakistan|Philippines|Taiwan|Thailand|Vanuatu|Vietnam"), levels(as.factor(authorduplicated$countryalone)), 
                                             value = TRUE, invert=FALSE) 
        )
      }
    }
  } ) #end observe
  observe({
    validate(
      need(try(!invalid(input$selectallcountries)), "Wait 920")
    )   
    if (input$selectallcountries > 0) {
      if (input$selectallcountries %% 2 == 0){
        updateSelectizeInput(session=session, 
                             'countryselector',
                             'Select Country', 
                             levels(as.factor(authorduplicated$countryalone)), #  
                             selected = levels(as.factor(authorduplicated$countryalone))
        )
      }
      else {
        updateSelectizeInput(session=session, 
                             'countryselector',
                             'Select Country', 
                             levels(as.factor(authorduplicated$countryalone)), #  
                             selected = c() 
        )
      }
    }
  } ) #end observe
  observe({
    validate(
      need(try(!invalid(input$selecteudicots2c)), "Wait 801")
    )     
    if (input$selecteudicots2c > 0) {
      if (input$selecteudicots2c %% 2 == 0){
        updateCheckboxGroupInput(session=session, 
                                 'apgcladeinput2c',
                                 'Clades', 
                                 levels(x2cdata$apgorder), #  
                                 selected = c() #levels(x2ndata$apgorder)
        )
      }
      else {
        updateCheckboxGroupInput(session=session, 
                                 'apgcladeinput2c',
                                 'Clades', 
                                 levels(x2cdata$apgorder),
                                 selected= grep(glob2rx("*udicot*"), levels(x2cdata$apgorder), value = TRUE)  
        )
      }
    }
  } ) #end observe
  tblhistogen = eventReactive(input$tablehistogenbutton, {
    if(!is.null(input$selectgenus))
    { 
      tb=techg[which(techg$genus  %in% c(input$selectgenus)
                     &
                       !is.na(techg$parsed2n ) 
      )
      ,  ]
    }
    else if (length(levels(x2ndata2gencladefam()$`family_apg_aste`)) >1 )
    {
      tb <- techg[which(techg$`family_apg_aste` %in% input$apgcladeinputgensubfam
                        & !is.na(techg$parsed2n)
      )#family_apg_aste
      ,]
      tb <- droplevels(tb)
      tb
    }
    else
    {
      tb <- techg[which(tech$family_apg %in% input$apgcladeinputgenfam
                        & !is.na(techg$parsed2n)
      )#family_apg_aste
      ,]
      tb <- droplevels(tb)
      tb
    } 
  } ) # end tblhistogen
  tblhistogen2c = eventReactive(input$tablehistogenbutton2c, {
    if(!is.null(input$selectgenus2c))
    { 
      tb=techg[which(techg$genus  %in% c(input$selectgenus2c)
                     &
                       !is.na(techg$parsed2c ) 
      )
      ,  ]
    }
    else if (length(levels(x2cdata2gencladefam()$`family_apg_aste`)) >1 )
    {
      tb <- techg[which(techg$`family_apg_aste` %in% input$apgcladeinputgensubfam2c
                        & !is.na(techg$parsed2c)
      )#family_apg_aste
      ,]
      tb <- droplevels(tb)
      tb
    }
    else
    {
      tb <- techg[which(techg$family_apg %in% input$apgcladeinputgenfam2c
                        & !is.na(techg$parsed2c)
      )#family_apg_aste
      ,]
      tb <- droplevels(tb)
      tb
    } 
  } ) # end tblhistogen 2c
  tblhistotcl <- reactiveValues(tbtcl = NULL,centered=NULL,linked=NULL,current=NULL,predicted=NULL)
  observeEvent( input$tablehistotclbutton, {
    if (tcaortcl()=="TCL") 
    { tblhistotcl$current<- "tclwithout2c"
    tblhistotcl$renamed<-c("Clade"="apgorder", "parsed 2n"="parsed2n", "TCL" ="tc" ) 
    }
    else #if (tcaortcl()=="TCA")
    { tblhistotcl$current<- "tcawithout2c"
    tblhistotcl$renamed<-c("Clade"="apgorder", "parsed 2n"="parsed2n", "TCA" ="tc" )
    }
    tblhistotcl$centered<- c(5,6)
    tblhistotcl$linked<-c(4)
    if (input$predictorclade=="Show Clades")
    {
      if (!is.null(input$tclapgcladeinput) && input$tclapgcladeinput != "") 
      { 
        tblhistotcl$tbtcl <- tcnot2c()[which(tcnot2c()$apgorder %in% input$tclapgcladeinput
                                             & !is.na(tcnot2c()$tc)     )#
                                       ,]
        tblhistotcl$tbtcl<- tblhistotcl$tbtcl[, c(1:7)] 
      } #if
      else {
        tblhistotcl$tbtcl <- tcnot2c()[which(!is.na(tcnot2c()$tc )
        )
        ,]
        tblhistotcl$tbtcl<- tblhistotcl$tbtcl[, c(1:7)] 
      } #else
    } #if
    else if (input$predictorclade=="Show predicted 2C")
    {tblhistotcl$tbtcl <- tcnot2c()[which( !is.na(tcnot2c()$tc)
    )#family_apg_aste
    ,]
    tblhistotcl$tbtcl<- tblhistotcl$tbtcl[, c(1:7)] 
    }
  } )
  observeEvent(input$tablehistotclstatbutton , {
    tblhistotcl$tbtcl <- tcand2c()
    tblhistotcl$centered<- c(5:7)
    tblhistotcl$linked<-c(4,9)
    if (tcaortcl()=="TCL") 
    { tblhistotcl$current<- "tcland2c"
    tblhistotcl$renamed<-c("Clade"="apgorder", "parsed 2n"="parsed2n","parsed 2C"="parsed2c", "TCL" ="tc" ) }
    else if (tcaortcl()=="TCA")
    { tblhistotcl$current<- "tcaand2c"
    tblhistotcl$renamed<-c("Clade"="apgorder", "parsed 2n"="parsed2n","parsed 2C"="parsed2c", "TCA" ="tc" ) }
    tblhistotcl$tbtcl<- tblhistotcl$tbtcl[, c(1:10)]#, with=FALSE] 
  })  
  observeEvent(input$tablehistogenbutton, {
    updateTabsetPanel(session, "tabsetpanel3", "Table of selected genera")
  }
  )
  observe({
    if (!is.null(input$plot_clickauthor) && !is.na(input$plot_clickauthor) && !invalid(tblres$tblauthors) && nrow(tblres$tblauthors) > 0 ) {
      updateTabsetPanel(session, "tabsetnetwork", selected = "Table")
    }
    else {
      updateTabsetPanel(session, "tabsetnetwork", selected = "Co-author network")
    }
  })
  observeEvent(input$tablehistogenbutton2c, {
    updateTabsetPanel(session, "tabsetpanel4", "Table of selected genera")
  }
  )
  observeEvent(input$tablehistotclstatbutton, {
    updateTabsetPanel(session, "tabsetpaneltcl", "Table of selected sample")
  }
  )
  observeEvent(input$tablehistotclbutton, {
    updateTabsetPanel(session, "tabsetpaneltcl", "Table of selected sample")
  }
  )
  tblgendl = reactive ({  
    tb=tblhistogen()[, columnstoshowtblhistogen(), ] #with = FALSE]
    tb
  })
  tblgendl2c = reactive ({  
    tb=tblhistogen2c()[, columnstoshowtblhistogen2c(), ]# with = FALSE]
    tb
  })
  output$downloadCsv <- downloadHandler(
    filename = function() {
      paste(tblgendl()$family_apg[1],".csv", sep="")
    },
    content = function(file) {
      write.csv(tblgendl(), file, na="",
                row.names = FALSE, quote = TRUE)
    }
  )
  output$downloadCsv2 <- downloadHandler(
    filename = function() {
      paste(tblgendl2c()$family_apg[1],".csv", sep="")
    },
    content = function(file) {
      write.csv(tblgendl2c(), file, na="",
                row.names = FALSE, quote = TRUE)
    }
  )
  output$downloadCsv3 <- downloadHandler(
    filename = function() {
      paste(tblhistotcl$current,".csv", sep="")
    }
    ,
    content = function(file) {
      write.csv(tblhistotcl$tbtcl, file, na="",
                row.names = FALSE, quote = TRUE)
    }
  ) # END output csv3
  output$downloadCsv4 <- downloadHandler(
    filename = function() {
      paste("yourquery.csv", sep="")
    }
    ,
    content = function(file) {
      write.csv(tblres$tblquery, file, na="",
                row.names = FALSE, quote = TRUE)
    }
  ) # END output csv4
  output$downloadCsvauthor <- downloadHandler(
    filename = function() {
      paste("yourauthors.csv", sep="")
    }
    ,
    content = function(file) {
      write.csv(tblres$tblauthors, file, na="",
                row.names = FALSE, quote = TRUE)
    }
  ) # END output csv
  output$downloadCsv7 <- downloadHandler(
    filename = function() {
      paste("stats.csv", sep="")
    }
    ,
    content = function(file) {
      write.csv(stattable(), file, na="",
                row.names = FALSE, quote = TRUE)
    }
  ) # END output csv4
  observe({
    if (!invalid(input$selectallfam) ){  
      if (input$selectallfam > 0) {
        if (input$selectallfam %% 2 == 0){
          updateSelectizeInput(session=session, 
                               inputId = "selectfamily", 
                               label = "",
                               choices  = levels(x2ndata2famclade()$family_apg),
                               selected = levels(x2ndata2famclade()$family_apg)
          )
        }
        else {
          updateSelectizeInput(session=session, 
                               inputId = "selectfamily", 
                               label = "",
                               choices  = levels(x2ndata2famclade()$family_apg),
                               selected = c()
          )
        }
      }
    }
  }) #end
  observe({
    if (!invalid(input$selectallfam2c) ){  
      if (input$selectallfam2c > 0) {
        if (input$selectallfam2c %% 2 == 0){
          updateSelectizeInput(session=session, 
                               inputId = "selectfamily2c", 
                               label = "",
                               choices  = levels(x2cdata2famclade()$family_apg),
                               selected = levels(x2cdata2famclade()$family_apg)
          )
        }
        else {
          updateSelectizeInput(session=session, 
                               inputId = "selectfamily", 
                               label = "",
                               choices  = levels(x2cdata2famclade()$family_apg),
                               selected = c()
          )
        }
      }
    }
  }) #end
  observe({
    validate(
      need(try(x2ndata2gencladefam() != ""), "Wait x2ndata2gencladefam observe button")
    )
    if (length(levels(x2ndata2gencladefam()$`family_apg_aste`)) ==1 )
    {        
      if (!invalid(input$selectallgen) ){  
        if (input$selectallgen > 0) {
          if (input$selectallgen %% 2 == 0){
            updateSelectizeInput(session=session, 
                                 inputId = "selectgenus", 
                                 label = "",
                                 choices  = levels(x2ndata2gencladefam()$genus),
                                 selected = levels(x2ndata2gencladefam()$genus)
            )
          }
          else {
            updateSelectizeInput(session=session, 
                                 inputId = "selectgenus", 
                                 label = "",
                                 choices  = levels(x2ndata2gencladefam()$genus),
                                 selected = c()
            )
          }
        }
      } #end major if invalid
    }
    else
    {
      if (!invalid(input$selectallgen) ){  
        if (input$selectallgen > 0) {
          if (input$selectallgen %% 2 == 0){
            updateSelectizeInput(session=session, 
                                 inputId = "selectgenus", 
                                 label = "",
                                 choices  = levels(x2ndata2gencladesubfam()$genus),
                                 selected = levels(x2ndata2gencladesubfam()$genus)
            )
          }
          else {
            updateSelectizeInput(session=session, 
                                 inputId = "selectgenus", 
                                 label = "",
                                 choices  = levels(x2ndata2gencladesubfam()$genus),
                                 selected = c()
            )
          }
        }
      } #end major if invalid
    }
  })
  x2ndatab<-reactive({
    data2b<-x2ndata 
    if (!is.null(input$apgcladeinput) && input$apgcladeinput != "") {
      data2b = data2b[data2b$apgorder %in% c(input$apgcladeinput),]
    } else {
      data2b <- x2ndata   
    }
  }) # end reactive
  x2cdatab<-reactive({
    data2b<-x2cdata 
    if (!is.null(input$apgcladeinput2c) && input$apgcladeinput2c != "") {
      data2b = data2b[data2b$apgorder %in% c(input$apgcladeinput2c),]
    } else {
      data2b <- x2cdata   
    }
  }) # end reactive
  x2ndata2<-reactive({
    x2ndata$labelcount<-as.character(x2ndata$labelcount)
    x2ndata <- x2ndata[with(x2ndata,order(labelcount)), ] #added
    x2ndata$labelcount = as.factor(x2ndata$labelcount)
    datab<-x2ndata 
    if (!is.null(input$apgcladeinput) && input$apgcladeinput != "") {
      datab = datab[which(datab$apgorder %in% c(input$apgcladeinput) 
                          & datab$parsed2n <= input$range2n[1]
      )
      ,]
      datab <- droplevels(datab)
          } else {
      datab <- x2ndata   
    }
  } ) #end reactive
x2cdatacustomlabel<-reactive({
   x2cdata<-x2cdata
    data<-get.3modes.andcounts(x2cdata,"apgorder","parsed2c",x2cadjustreac())
    namecol<-"labelcountcustom"
    x2cdatamode2cmodescount<-make.legend.withstats(data,namecol)
    x2cdatab<-merge(x2cdata, x2cdatamode2cmodescount, all.x = TRUE)
    x2cdatab$labelcountcustom = as.factor(x2cdatab$labelcountcustom)
    x2cdatab
})
x2cdatacustomlabeldensity<-reactive({
  x2cdata<-x2cdata2()
  data<-get.3modes.andcounts(x2cdata,"apgorder","parsed2c",x2cadjustreacdensity())
  namecol<-"labelcountcustom"
  x2cdatamode2cmodescount<-make.legend.withstats(data,namecol)
  x2cdatab<-merge(x2cdata, x2cdatamode2cmodescount, all.x = TRUE)
  x2cdatab$labelcountcustom = as.factor(x2cdatab$labelcountcustom)
  x2cdatab
})
x2cdatacustomlabelfam<-reactive({
  x2cdata<-x2cdata2fam()
  data<-get.3modes.andcounts(x2cdata,"family_apg","parsed2c",x2cadjustreacfam())
  namecol<-"fam2clabelcountcustom"
  x2cdatamode2cmodescount<-make.legend.withstats(data,namecol)
  x2cdatab<-merge(x2cdata, x2cdatamode2cmodescount, all.x = TRUE)
  x2cdatab$fam2clabelcountcustom = as.factor(x2cdatab$fam2clabelcountcustom)
  x2cdatab
})
x2cdatacustomlabelgen<-reactive({
  x2cdata<-x2cdata2gen()
  data<-get.3modes.andcounts(x2cdata,"genus","parsed2c",x2cadjustreacgen())
  namecol<-"gen2clabelcountcustom"
  x2cdatamode2cmodescount<-make.legend.withstats(data,namecol)
  x2cdatab<-merge(x2cdata, x2cdatamode2cmodescount, all.x = TRUE)
  x2cdatab$gen2clabelcountcustom = as.factor(x2cdatab$gen2clabelcountcustom)
  dt <- as.data.table(x2cdatab)
  dt1 <- dt[, n := .N, by = namecol]
  dt2<-as.data.frame(dt1[n >= 3,])
  dt2<- droplevels(dt2)
  dt2
})
x2cdatacustomlabelquery<-reactive({
  x2cdata<-querygenera$tbl
  data<-get.3modes.andcounts(x2cdata,"genus","parsed2c",x2cadjustreacquery())
  namecol<-"query2clabelcountcustom"
  x2cdatamode2cmodescount<-make.legend.withstats(data,namecol)
  x2cdatab<-merge(x2cdata, x2cdatamode2cmodescount, all.x = TRUE)
  x2cdatab$query2clabelcountcustom = as.factor(x2cdatab$query2clabelcountcustom)
  dt <- as.data.table(x2cdatab)
  dt1 <- dt[, n := .N, by = namecol]
  dt2<-as.data.frame(dt1[n >= 3,])
  dt2<- droplevels(dt2)
  dt2
})
x2cdatacustomlabeldensityfam<-reactive({
  x2cdatac<-x2cdata2fam()
  data<-get.3modes.andcounts(x2cdatac,"family_apg","parsed2c",x2cadjustreacdensityfam())
  namecol<-"fam2clabelcountcustom"
  x2cdatamode2cmodescountc<-make.legend.withstats(data,namecol)
  x2cdatac<-merge(x2cdatac, x2cdatamode2cmodescountc, all.x = TRUE)
  x2cdatac$fam2clabelcountcustom = as.factor(x2cdatac$fam2clabelcountcustom)
  x2cdatac
})
x2cdatacustomlabeldensitygen<-reactive({
  x2cdatac<-x2cdata2gen()
  data<-get.3modes.andcounts(x2cdatac,"genus","parsed2c",x2cadjustreacdensitygen())
  namecol<-"gen2clabelcountcustom"
  x2cdatamode2cmodescountc<-make.legend.withstats(data,namecol)
  x2cdatac<-merge(x2cdatac, x2cdatamode2cmodescountc, all.x = TRUE)
  x2cdatac$gen2clabelcountcustom = as.factor(x2cdatac$gen2clabelcountcustom)
  x2cdatac
})
x2cdatacustomlabeldensityquery<-reactive({
  x2cdatac<-querygenera$tbl
  data<-get.3modes.andcounts(x2cdatac,"genus","parsed2c",x2cadjustreacdensityquery())
  namecol<-"query2clabelcountcustom"
  x2cdatamode2cmodescountc<-make.legend.withstats(data,namecol)
  x2cdatac<-merge(x2cdatac, x2cdatamode2cmodescountc, all.x = TRUE)
  x2cdatac$query2clabelcountcustom = as.factor(x2cdatac$query2clabelcountcustom)
  x2cdatac
})
  x2cdata2<-reactive({
    x2cdata$labelcount<-as.character(x2cdata$labelcount)
    x2cdata <- x2cdata[with(x2cdata,order(labelcount)), ] #added
    x2cdata$labelcount = as.factor(x2cdata$labelcount)
    datab<-x2cdata 
    if (!is.null(input$apgcladeinput2c) && input$apgcladeinput2c != "") {
      datab = datab[which(datab$apgorder %in% c(input$apgcladeinput2c) 
                          & datab$parsed2c <= input$range2c[1]
      )
      ,]
      datab <- droplevels(datab)
    } else {
      datab <- x2cdata   
    }
  } ) #end reactive
  max2n= reactive({
    ceiling(max(ggplot_build(ggplot(x2ndatab(), aes(x = x2ndatab()$parsed2n)) + geom_histogram(aes(fill=x2ndatab()$apgorder)))[[1]][[1]][,6])) 
  })
  min2n= reactive({ 
    min2na<-floor(min(ggplot_build(ggplot(x2ndatab(), aes(x = x2ndatab()$parsed2n)) + geom_histogram(aes(fill=x2ndatab()$apgorder)))[[1]][[1]][,5])) # round(ggplot_build(ggplot(x2ndatab(), aes(x = x2ndatab()$parsed2n)) + geom_histogram(aes(fill=x2ndatab()$apgorder)) ) ) 
     if(min2na < 0)
       {
       min2na= 0
       } 
     else if ( min2na >= 0 && max2n() ==min2na) 
       {
       min2na = min2na - 2
     }
     else #if (min2n >= 0)
       { 
       min2na= min2na #round(ggplot_build(ggplot(x2ndatab(), aes(x = x2ndatab()$parsed2n)))$panel$ranges[[1]]$x.range[1])
     }
    min2na
  })
  max2c <- reactive ({ 
    max2c= ceiling(max(ggplot_build(ggplot(x2cdatab(), aes(x = x2cdatab()$parsed2c)) + 
                                      geom_histogram(aes(fill=x2cdatab()$apgorder)))[[1]][[1]][,6]) )    
    if (max2c < 5){#0    
    max2c<-5}
    else{max2c}
    max2c
  })
    max2nfam= reactive({ 
      if(!invalid(x2ndata2famb())){ 
      max2nfam= ceiling(max(ggplot_build(ggplot(x2ndata2famb(), aes(x = x2ndata2famb()$parsed2n)) + geom_histogram(aes(fill=x2ndata2famb()$family_apg)))[[1]][[1]][,6])) # round(ggplot_build(ggplot(x2ndatab(), aes(x = x2ndatab()$parsed2n)) + geom_histogram(aes(fill=x2ndatab()$apgorder)) ) ) 
      }
      })
    min2nfam= reactive({ 
      if(!invalid(x2ndata2famb())){ 
      min2nfam= floor(min(ggplot_build(ggplot(x2ndata2famb(), aes(x = x2ndata2famb()$parsed2n)) + geom_histogram(aes(fill=x2ndata2famb()$family_apg)))[[1]][[1]][,5])) 
      if(min2nfam < 0){min2nfam= 0} 
      else if ( min2nfam >= 0 && max2nfam() ==min2nfam) { 
        min2nfam = min2nfam -2
      }
      else #if (min2nfam >= 0)
        {    min2nfam= min2nfam # round(ggplot_build(ggplot(x2ndata2famb(), aes(x = x2ndata2famb()$parsed2n)))$panel$ranges[[1]]$x.range[1])
        }
      min2nfam
    }
  }) # end re
max2cfam<- reactive({
    if(!invalid(x2cdata2famb())){ 
      max2cfam= ceiling(max(ggplot_build(ggplot(x2cdata2famb(), aes(x = x2cdata2famb()$parsed2c)) + geom_histogram(aes(fill=x2cdata2famb()$family_apg)))[[1]][[1]][,6]) )    
      if(max2cfam < 5)
      {max2cfam= 5} 
    }
      max2cfam
})
      max2ngen   <- reactive({ 
        validate(
          need(x2ndata2genb() != "", "Wait 1221")
        )
      if(!invalid(x2ndata2genb())){ 
        max2ngen= ceiling(max(ggplot_build(ggplot(x2ndata2genb(), aes(x = x2ndata2genb()$parsed2n)) + geom_histogram(aes(fill=x2ndata2genb()$genus)))[[1]][[1]][,6])) # round(ggplot_build(ggplot(x2ndatab(), aes(x = x2ndatab()$parsed2n)) + geom_histogram(aes(fill=x2ndatab()$apgorder)) ) ) 
      }
      })
      min2ngen   <- reactive({ 
       if(!invalid(x2ndata2genb())){ 
        min2ngen= floor(min(ggplot_build(ggplot(x2ndata2genb(), aes(x = x2ndata2genb()$parsed2n)) + geom_histogram(aes(fill=x2ndata2genb()$genus)))[[1]][[1]][,5])) # round(ggplot_build(ggplot(x2ndatab(), aes(x = x2ndatab()$parsed2n)) + geom_histogram(aes(fill=x2ndatab()$apgorder)) ) ) 
        if (!invalid(min2ngen)) { 
          if(min2ngen < 0){
            min2ngen= 0} 
          else if ( min2ngen >= 0 && max2ngen() ==min2ngen) 
            {
            min2ngen = min(x2ndata2genb()$parsed2n) - 2 
          }
          else #if (min2ngen >= 0)
            {    min2ngen= min2ngen# round(ggplot_build(ggplot(x2ndata2genb(), aes(x = x2ndata2genb()$parsed2n)))$panel$ranges[[1]]$x.range[1])
          }
        }
        }
    }) # end observe
max2cgen<- reactive({
        validate(
          need(x2cdata2genb() != "", "Wait 1254")
        )   
if(!invalid(x2cdata2genb())){ 
max2cgen= ceiling(max(ggplot_build(ggplot(x2cdata2genb(), aes(x = x2cdata2genb()$parsed2c)) + 
                                     geom_histogram(aes(fill=x2cdata2genb()$genus)))[[1]][[1]][,6]) )    
if(max2cgen < 5)
{
      max2cgen= 5
}
}
max2cgen
})
  pb1b= pretty_breaks(15)
  pbx2n=reactive({
    pb1=pb1b 
    if (
      max2n() - min2n() <= 10 && max2n() - min2n()>=5 )
    {
      pb1=pretty_breaks(7)
    } 
    else if (
      max2n()-min2n()<5 )
  {
      pb1=pretty_breaks(3)
    } 
    else {
      pb1=pb1b   
    }
  })
  max2c2 <- reactive ({ 
    max2c= ceiling(max(ggplot_build(ggplot(x2cdata2(), aes(x = x2cdata2()$parsed2c)) + geom_histogram(aes(fill=x2cdata2()$apgorder)))[[1]][[1]][,6]) )    
  })
  min2c2<- reactive ({ 
    validate(
      need(x2cdata2() != "", "Wait 1341")
    )
    min2ca= tryCatch(floor(min(ggplot_build(ggplot(x2cdata2(), aes(x = x2cdata2()$parsed2c)) + geom_histogram(aes(fill=x2cdata2()$apgorder)))[[1]][[1]][,5]) 
                           ), error = function(e) {0 ; 0 } )    
    if(!invalid(min2ca)){ 
    if(min2ca < 0)
    {
      min2ca<- 0
    } 
    else if ( min2ca >= 0 && max2c2() == min2ca) 
    { 
      min2ca <- min2ca-2
    }
    else # if (min2ca >= 0)
    { 
      min2ca #= min2ca #min(floor(ggplot_build(ggplot(x2cdatab(), aes(x = x2cdatab()$parsed2c)) + geom_histogram(aes(fill=x2cdatab()$apgorder)))[[1]][[1]][,5]) )
    }
    }
    })
  pbx2c=reactive({
    pb1=pb1b 
    if (
      max2c2() - min2c2() <= 10 && max2c2() - min2c2() >= 5 
      ) {
      pb1=pretty_breaks(7)
    } 
    else if (
      max2c2() - min2c2() < 5
       ) {
      pb1=pretty_breaks(3)
    } 
    else {
      pb1=pb1b   
    }
  })
  mintc<-reactive({ 
    min= floor(min(ggplot_build(ggplot(graph5table(), aes(tc)) + geom_histogram(aes(fill=x2())))[[1]][[1]][,5]) )    
  })
  maxtc<-reactive({ 
    max= ceiling(max(ggplot_build(ggplot(graph5table(), aes(tc)) + geom_histogram(aes(fill=x2())))[[1]][[1]][,6]) )    
  })
  pbxtcl=reactive({
    pb1=pb1b 
    if (maxtc() - mintc() <=10 && maxtc() - mintc() >=5
    ) {
      pb1=pretty_breaks(7)
    } 
    else if (
      maxtc() - mintc() < 5
      ) 
    {
      pb1=pretty_breaks(3)
    } 
    else {
      pb1=pb1b   
    }
  })
  max2ny= reactive({
    ceiling(max(ggplot_build(ggplot(x2ndata2(), aes(x = x2ndata2()$parsed2n)) + 
                               geom_histogram(aes(fill=x2ndata2()$apgorder)))[[1]][[1]][,13])) # round(ggplot_build(ggplot(x2ndatab(), aes(x = x2ndatab()$parsed2n)) + geom_histogram(aes(fill=x2ndatab()$apgorder)) ) ) 
  })
  pby2n=reactive({
    pb1f=pb1b 
    if (max2ny()<=11 && max2ny()>=5
      ) {
      pb1f=pretty_breaks(7)
    } 
    else if ( max2ny()<5
      ) {
      pb1f=pretty_breaks(3)
    } 
    else {
      pb1f=pb1b   
    }
  })
  pby2c=reactive({
    pb1f=pb1b 
    if ( maxy2c() <=11 && maxy2c() >=5
        ) {
      pb1f=pretty_breaks(7)
    } 
    else if ( maxy2c() < 5
      ) {
      pb1f=pretty_breaks(3)
    } 
    else {
      pb1f=pb1b   
    }
  })
  mintcy<-reactive({ 
    min= floor(min(ggplot_build(ggplot(graph5table(), aes(tc)) + geom_histogram(binwidth=input$binstcl,aes(fill=x2())))[[1]][[1]][,12]) )    
  })
  maxtcy<-reactive({ 
    max = NULL
    max= ceiling(max(ggplot_build(ggplot(graph5table(), aes(tc)) + geom_histogram(binwidth=input$binstcl,aes(fill=x2())))[[1]][[1]][,13]) )
    max=max+(max/10)
  })
  pbytcl=reactive({
    pb1f=pb1b 
    if (maxtcy()-mintcy() <= 11 && maxtcy()-mintcy() >= 5
    ) {
      pb1f=pretty_breaks(7)
    } 
    else if (
      maxtcy()-mintcy() < 5
    ) {
      pb1f=pretty_breaks(3)
    } 
    else {
      pb1f=pb1b   
    }
  })
  maxy2n=reactive({
    maxyb=NULL
    maxyb=(max(ggplot_build(ggplot(x2ndata2(), aes(x = x2ndata2()$parsed2n)) + 
                              geom_histogram(binwidth=input$bins2n, aes(fill=x2ndata2()$apgorder) ))[[1]][[1]][,13]))
    maxyb=maxyb+(maxyb/10)
  })
  maxy2c=reactive({
    validate(
      need(x2cdata2() != "", "Wait x2cdata")
    )
    validate(
      need(x2cdatab() != "", "Wait x2cdatab")
    )
    max2c= ceiling(max(ggplot_build(ggplot(x2cdata2(), aes(x = x2cdata2()$parsed2c)) + 
            geom_histogram(binwidth=input$bins2c, aes(fill=as.factor(x2cdata2()$ordmedcoufamcougencou))))[[1]][[1]][,13]) )    
    maxyb=max2c+(max2c/10)
  })
  maxy2nfam=reactive({
    maxyb=NULL
    maxyb=ceiling(max(ggplot_build(ggplot(x2ndata2fam(), aes(x = x2ndata2fam()$parsed2n)) + 
                              geom_histogram(binwidth=input$bins2nfam, aes(fill=x2ndata2fam()$family_apg) ))[[1]][[1]][,13]))
    maxyb=maxyb+(maxyb/10)
  })
  maxy2cfam=reactive({
    maxyb=NULL
    maxyb=ceiling(max(ggplot_build(ggplot(x2cdata2fam(), aes(x = x2cdata2fam()$parsed2c)) + 
                              geom_histogram(binwidth=input$bins2cfam, aes(fill=x2cdata2fam()$family_apg) ) )[[1]][[1]][,13]))
    maxyb=maxyb+(maxyb/10)
  })
  maxy2ngen=reactive({ 
    validate(
      need(x2ndata2gen() != "", "Wait x2ndatagen")
    )
    maxyb=NULL
    maxyb=ceiling( max(ggplot_build(ggplot(x2ndata2gen(), aes(x = x2ndata2gen()$parsed2n)) + 
                              geom_histogram(binwidth=input$bins2ngen, aes(fill=x2ndata2gen()$genus) ) )[[1]][[1]][,13]))
    maxyb=maxyb+(maxyb/10)
  }) 
  maxy2cgen=reactive({
    validate(
      need(x2cdata2gen() != "", "Wait x2cdatagen")
    )
    validate(
      need(x2cdata2genb() != "", "Wait x2cdatagenb")
    )
    maxyb=NULL
    maxyb=(max(ggplot_build(ggplot(x2cdata2gen(), aes(x = x2cdata2gen()$parsed2c)) + 
                              geom_histogram(binwidth=input$bins2cgen, aes(fill=x2cdata2gen()$genus) ) )[[1]][[1]][,13])) 
    maxyb=maxyb+(maxyb/10)
  })
  max2nfamnob= reactive({ 
    if(!invalid(x2ndata2fam())){ 
      max2nfam= ceiling(max(ggplot_build(ggplot(x2ndata2fam(), aes(x = x2ndata2fam()$parsed2n)) + geom_histogram(aes(fill=x2ndata2fam()$family_apg)))[[1]][[1]][,6])) # round(ggplot_build(ggplot(x2ndatab(), aes(x = x2ndatab()$parsed2n)) + geom_histogram(aes(fill=x2ndatab()$apgorder)) ) ) 
    }
  })
  min2nfamnob= reactive({ 
    if(!invalid(x2ndata2fam())){ 
      min2nfam= floor(min  (ggplot_build(ggplot(x2ndata2fam(), aes(x = x2ndata2fam()$parsed2n)) + geom_histogram(aes(fill=x2ndata2fam()$family_apg)))[[1]][[1]][,5])) # round(ggplot_build(ggplot(x2ndatab(), aes(x = x2ndatab()$parsed2n)) + geom_histogram(aes(fill=x2ndatab()$apgorder)) ) ) 
    }
  })
  pbx2nfam=reactive({
    pb1=pb1b 
    if (max2nfamnob()-min2nfamnob() <=10 && max2nfamnob()-min2nfamnob() >=5 
      ) {
      pb1=pretty_breaks(7)
    } 
    else if (max2nfamnob()-min2nfamnob() <5
      ) {
      pb1=pretty_breaks(3)
    } 
    else {
      pb1=pb1b   
    }
  })
  max2cfamnob<- reactive({
    if(!invalid(x2cdata2fam())){ 
      max2cfam= ceiling(max(ggplot_build(ggplot(x2cdata2fam(), aes(x = x2cdata2fam()$parsed2c)) + geom_histogram(aes(fill=x2cdata2fam()$family_apg)))[[1]][[1]][,6]) )    
    }
  })
  min2cfamnob<- reactive({
    if(!invalid(x2cdata2fam())){ 
      min2cfam= floor(min(ggplot_build(ggplot(x2cdata2fam(), aes(x = x2cdata2fam()$parsed2c)) + geom_histogram(aes(fill=x2cdata2fam()$family_apg)))[[1]][[1]][,5]) )    
    }
  })
  pbx2cfam=reactive({
    pb1=pb1b 
    if (max2cfamnob()-min2cfamnob() <=10 && max2cfamnob()-min2cfamnob() >=5
        ) {
      pb1=pretty_breaks(7)
    } 
    else if (max2cfamnob()-min2cfamnob() <5 
       ) {
      pb1=pretty_breaks(3)
    }  
    else {
      pb1=pb1b   
    }
  })
  max2ngennob   <- reactive({ 
    if(!invalid(x2ndata2gen())){ 
      max2ngen= ceiling(max(ggplot_build(ggplot(x2ndata2gen(), aes(x = x2ndata2gen()$parsed2n)) + geom_histogram(aes(fill=x2ndata2gen()$genus)))[[1]][[1]][,6])) 
    }
  })
  min2ngennob   <- reactive({ 
    if(!invalid(x2ndata2gen())){ 
      min2ngen= floor(min(ggplot_build(ggplot(x2ndata2gen(), aes(x = x2ndata2gen()$parsed2n)) + geom_histogram(aes(fill=x2ndata2gen()$genus)))[[1]][[1]][,5])) # round(ggplot_build(ggplot(x2ndatab(), aes(x = x2ndatab()$parsed2n)) + geom_histogram(aes(fill=x2ndatab()$apgorder)) ) ) 
    }
  })
  pbx2ngen=reactive({
    validate(
      need(x2ndata2gen() != "", "Wait x2ndatagen x")
    )
    validate(
      need(x2ndata2genb() != "", "Wait x2ndata2genb x")
    )
    pb1=pb1b
    validate(
      need(max2ngennob() != "", "Wait 1722")
    ) 
      if(is.finite(
        max2ngennob()
      ) ) {  
      if (max2ngennob()-min2ngennob() <=10 && max2ngennob()-min2ngennob() >=5
        ) {
        pb1=pretty_breaks(7)
      } 
      else if (max2ngennob()-min2ngennob() < 5
        ) {
        pb1=pretty_breaks(3)
      } 
      else {
        pb1=pb1b   
      }
    } # end is finite
  }) 
  max2cgennob<- reactive({
    if(!invalid(x2cdata2gen())){ 
      max2cgen= ceiling(max(ggplot_build(ggplot(x2cdata2gen(), aes(x = x2cdata2gen()$parsed2c)) + geom_histogram(aes(fill=x2cdata2gen()$genus)))[[1]][[1]][,6]) )    
    }
  })
  min2cgennob<- reactive({
    if(!invalid(x2cdata2gen())){ 
      min2cgen= floor(min(ggplot_build(ggplot(x2cdata2gen(), aes(x = x2cdata2gen()$parsed2c)) + geom_histogram(aes(fill=x2cdata2gen()$genus)))[[1]][[1]][,5]) )    
    }
  })
  pbx2cgen=reactive({
    validate(
      need(x2cdata2gen() != "", "Wait x2cdatagen x")
    )
    validate(
      need(x2cdata2genb() != "", "Wait x2cdatagenb x")
    )
    pb1=pb1b
    if(is.finite(
      max2cgennob()
      )  ) {  
      if (max2cgennob()-min2cgennob() <=10 && max2cgennob()-min2cgennob() >=5 
        ) {
        pb1=pretty_breaks(7)
      } 
      else if (max2cgennob()-min2cgennob() < 5
               ) {
        pb1=pretty_breaks(3)
      } 
      else {
        pb1=pb1b   
      }
    }
  })
  max2nfamy= reactive({ 
    if(!invalid(x2ndata2fam())){ 
      max2nfam= ceiling(max(ggplot_build(ggplot(x2ndata2fam(), aes(x = x2ndata2fam()$parsed2n)) + geom_histogram(aes(fill=x2ndata2fam()$family_apg)))[[1]][[1]][,13])) # round(ggplot_build(ggplot(x2ndatab(), aes(x = x2ndatab()$parsed2n)) + geom_histogram(aes(fill=x2ndatab()$apgorder)) ) ) 
    }
  })
  pby2nfam=reactive({
    pb1f=pb1b 
    validate(
      need(max2nfamy() != "", "Wait 1832")
    )    
    if (max2nfamy()<=11 && max2nfamy()>=5
        ) {
      pb1f=pretty_breaks(7)
    } 
    else if (max2nfamy() < 5
       ) {
      pb1f=pretty_breaks(3)
    } 
    else {
      pb1f=pb1b   
    }
  })
  max2cfamy= reactive({ 
    if(!invalid(x2cdata2fam())){ 
      max2cfam= ceiling(max(ggplot_build(ggplot(x2cdata2fam(), aes(x = x2cdata2fam()$parsed2c)) + geom_histogram(aes(fill=x2cdata2fam()$family_apg)))[[1]][[1]][,13])) # round(ggplot_build(ggplot(x2ndatab(), aes(x = x2ndatab()$parsed2n)) + geom_histogram(aes(fill=x2ndatab()$apgorder)) ) ) 
    }
  })
  pby2cfam=reactive({
    pb1f=pb1b 
    validate(
      need(max2cfamy() != "", "Wait 1898")
    )    
    if (max2cfamy()<=11 && max2cfamy() >=5 
        ) {
      pb1f=pretty_breaks(7)
    } 
    else if (max2cfamy() <5
      ) {
      pb1f=pretty_breaks(3)
    } 
    else {
      pb1f=pb1b   
    }
  })
  max2ngeny   <- reactive({ 
    if(!invalid(x2ndata2gen())){ 
      max2ngen= ceiling(max(ggplot_build(ggplot(x2ndata2gen(), aes(x = x2ndata2gen()$parsed2n)) + geom_histogram(aes(fill=x2ndata2gen()$genus)))[[1]][[1]][,13])) 
    }
  })
  pby2ngen=reactive({
    pb1f=pb1b 
    validate(
      need(x2ndata2gen() != "", "Wait pby2ngen")
    )
    if(is.finite( 
      max2ngeny()
    )    ) {  
      if (max2ngeny() <=11 && max2ngeny() >=5   
        ) {
        pb1f=pretty_breaks(7)
      } 
      else if (
        max2ngeny() < 5        
        ) {
        pb1f=pretty_breaks(3)
      } 
      else {
        pb1f=pb1b   
      }
    }
  })
  max2cgeny   <- reactive({ 
    validate(
      need(x2cdata2gen() != "", "Wait 1976")
    )
     if(!invalid(x2cdata2gen())){ 
      max2cgen= ceiling(max(ggplot_build(ggplot(x2cdata2gen(), aes(x = x2cdata2gen()$parsed2c)) + geom_histogram(aes(fill=as.factor(x2cdata2gen()$genus))))[[1]][[1]][,13])) 
    }
  })
  pby2cgen=reactive({
    pb1f=pb1b 
    validate(
      need(max2cgeny() != "", "Wait 1987")
    )  
    if(is.finite( 
      max2cgeny()
      )    ) {  
      if (max2cgeny()<=11 && max2cgeny() >=5
        ) {
        pb1f=pretty_breaks(7)
      } 
      else if (
        max2cgeny()<5 
        ) {
        pb1f=pretty_breaks(3)
      } 
      else {
        pb1f=pb1b   
      }
    }
  })
  output$x2ctabbox2 = renderUI({
    validate(
      need(try(!is.null(input$histoordensity) ) , "wait please"  ) #x2ndata2gen histogenplot")
    )
    if(input$histoordensity!="Histogram") {
      sliderInput("x2cadjustdensity",
                  "bandwidth adjust:",
                  0.1,
                  1,
                  0.5, round=F, ticks=F, step=0.1)
      }
    else { 
      sliderInput("bins2c",
                  "Bin width:",
                  min = 0.1,
                  max = 0.5, 
                  value = 0.1, 
                  step=0.05, 
                  ticks=F)
    }
  })
  output$x2ctabbox3 = renderUI({
    radioButtons("x2clegendtype","Type of legend",c("median and counts per taxa","3 modes and counts") )
  })
  output$x2ctabbox4 = renderUI({  
    sliderInput("range2c",
                "2C:",
                min = 5,#min2c(),
                max = max2c(),
                value = c(max2c()), ticks=F, round=F, step=1)
  })
  output$x2ctabbox = renderUI({
    if(input$par2ctype=="histogram")
    {
      wellPanel(width=12, 
        radioButtons("histoordensity","Choose type of graph",c("Histogram","Density")),
        checkboxInput("par2cgrid","Show as grid",value=TRUE),
        checkboxGroupInput('apgcladeinput2c','Clades', 
                           levels(x2cdata$apgorder), 
                           selected=levels(x2cdata$apgorder) 
        ) ,
        actionButton("selectallclades2c", label="Select/Deselect all"),
        HTML("<br></br>"),
        actionButton("selectmonocots2c", label="Select/Deselect monocots"),
        actionButton("selecteudicots2c", label="Select/Deselect eudicots"),
        HTML("<br></br>"),
        span("If nothing is selected all the sample is shown")
    ) #end well 
    }
    else
      wellPanel(width = 12,
                       sliderInput("x2cadjust",
                                   "bandwidth adjust:",
                                   0.1,
                                   1,
                                   0.5, round=F, ticks=F, step=0.1)
    ) #well
  })
  output$optionsclades2nbox <- renderUI({
    if(input$par2ntype=="histogram")
    {
      wellPanel(width=12,
                checkboxInput("par2ngrid","Show as grid",value=TRUE),
                checkboxGroupInput('apgcladeinput','Select clades', levels(x2ndata$apgorder), selected=levels(x2ndata$apgorder) ) ,
                actionButton("selectallclades", label="Select/Deselect all"),
                HTML("<br></br>"),
                actionButton("selectmonocots", label="Select/Deselect monocots"),
                actionButton("selecteudicots", label="Select/Deselect eudicots"),
                HTML("<br></br>"),
                span("If nothing is selected all the sample is shown")
      ) # end 
    }
    else
      wellPanel(width = 12,
                sliderInput("size2ndotplot","Modify width of graph",20,40,28,step=2, ticks = FALSE)     
      )
  })
  output$tcltabbox2 = renderUI({
    sliderInput("binstcl",
                "Bin width:",
                min = 2,
                max = 5,
                value = 3, 
                step=1,  
                ticks=F)
  })
  output$tcltabbox = renderUI({
    box(width=12, title="Choose fill type", solidHeader =TRUE, status="primary",
        selectInput('predictorclade','Choose type of fill (clades or 2C)',  width="100%", 
                    c("Show predicted 2C","Show Clades"), selected="Show Clades"
        ) #end input 
        ,
        uiOutput("selectinputapgcladetcl"),
        uiOutput("tclselectall"),
        HTML("<br></br>"),
        uiOutput("tclselectalleudico"),
        uiOutput("tclselectallmonoco")
    ) #end box
  })
  output$x2ctabboxgen2 = renderUI({
    validate(
      need(try(!is.null(input$histoordensitygen) ) , "wait please"  ) #x2ndata2gen histogenplot")
    )
    if(input$histoordensitygen!="Histogram") {
        sliderInput("x2cadjustdensitygen",
                    "bandwidth adjust:",
                    0.1,
                    1,
                    0.5, round=F, ticks=F, step=0.1)
      }
    else { 
      sliderInput("bins2cgen",
                  "Bin width:",
                  min = 0.1,
                  max = 0.5,
                  value = 0.1, 
                  step=0.05,  
                  ticks=F)
    }
  })
  output$x2ctabboxgen3 = renderUI({
    radioButtons("x2clegendtypegen","Type of legend",c("median and counts per taxa","3 modes and counts"))
  })
  output$x2ctabboxgen4 = renderUI({  
    validate(
      need( try(max2cgen() ) , "wait please2107"  ) #x2ndata2gen histogenplot")
    )
    sliderInput("range2cgen",
                "2C:",
                min = 5,#min2cgen(),
                max = max2cgen(),
                value = c(max2cgen()), ticks=F, round=F, step=1)
  })
  output$x2ctabboxfam2 = renderUI({
    validate(
      need(try(!is.null(input$histoordensityfam) ) , "wait please"  ) #x2ndata2gen histogenplot")
    )
    if(input$histoordensityfam!="Histogram") {
      sliderInput("x2cadjustdensityfam",
                  "bandwidth adjust:",
                  0.1,
                  1,
                  0.5, round=F, ticks=F, step=0.1)
    }
    else { 
      sliderInput("bins2cfam",
                  "Bin width:",
                  min = 0.1,
                  max = 0.5,
                  value = 0.1, 
                  step=0.05, 
                  ticks=F)
    }
  })
  output$x2ctabboxfam3 = renderUI({
    radioButtons("x2clegendtypefam","Type of legend",c("median and counts per taxa","3 modes and counts"))
  })
  output$x2ctabboxfam4 = renderUI({  
    validate(
      need( try(max2cfam() ) , "wait please2135"  ) #x2ndata2gen histogenplot")
    )
    sliderInput("range2cfam",
                "2C:",
                min = 5,#min2cfam(),
                max = max2cfam(),
                value = c(max2cfam()), ticks=F, round=F, step=1)
  })
  output$selectfamilybox = renderUI({
     if(input$par2ntypefam=="histogram")
     {
      wellPanel(width=12,
         checkboxInput("par2ngridfam","Show as grid",value=TRUE),
         selectizeInput(
           inputId = "selectfamily", 
           label = "Choose families of selected clade",
           multiple  = T,
           choices  = levels(x2ndata2famclade()$family_apg),
           selected = levels(x2ndata2famclade()$family_apg)
         ), #select
         actionButton("selectallfam", label="Select/Deselect all"),
         HTML("<br></br>"),
         span("Mantain any family(ies), or clic in the field to delete (with keyboard) or reselect (mouse) any"),
         HTML("<br></br>"),
         span("If nothing is selected all the sample is shown")
    ) #box
    }
  }) #end renderUI
  output$selectgenbox = renderUI({
    if(input$par2ntypegen=="histogram")
    {
        if (length(levels(x2ndata2gencladefam()$`family_apg_aste`)) <=1 ) # if no subfamilies
    {
          wellPanel(width=12,
           checkboxInput("par2ngridgen","Show as grid",value=TRUE),
           selectizeInput(
             inputId = "selectgenus", 
             label = "",
             multiple  = T,
             choices  = levels(x2ndata2gencladefam()$genus),
             selected = levels(x2ndata2gencladefam()$genus)
           ), #select
           actionButton("selectallgen", label="Select/Deselect all"),
           HTML("<br></br>"),
           span("Mantain any genus(era), or clic in the field to delete (with keyboard) or reselect (mouse) any"),
           HTML("<br></br>"),
           span("If nothing is selected all the sample is shown")
      ) #well
    }
    else{ # if subfamilies present
      wellPanel(width=12,
             checkboxInput("par2ngridgen","Show as grid",value=TRUE),
             selectizeInput(
             inputId = "selectgenus",
             label = "",
             multiple  = T,
             choices  = levels(x2ndata2gencladesubfam()$genus),
             selected = levels(x2ndata2gencladesubfam()$genus)
           ), #select
           actionButton("selectallgen", label="Select/Deselect all"),
           HTML("<br></br>"),
           span("Mantain any genus(era), or clic in the field to delete (with keyboard) or reselect (mouse) any"),
           HTML("<br></br>"),
           span("If nothing is selected all the sample is shown")
      ) #box
    }
    }
  }) #end renderUI
  x2ndata2famclade<-reactive({
    validate(
      need(try(!invalid(input$apgcladeinputfam)), "Wait 658")
    )
    datab<-x2ndata 
    if (!is.null(input$apgcladeinputfam) && input$apgcladeinputfam != "") {
      datab = datab[which(datab$apgorder %in% c(input$apgcladeinputfam) 
      )
      ,]
      datab <- droplevels(datab)
      datab
    } else {
      datab <- datab[which(datab$apgorder %in% (levels(datab$apgorder)[2])   
      )
      ,]
      datab <- droplevels(datab)
      datab
    }
  } )
  x2cdata2famclade<-reactive({
    validate(
      need(try(!invalid(input$apgcladeinputfam2c)), "Wait 658")
    )
    datab<-x2cdata 
    if (!is.null(input$apgcladeinputfam2c) && input$apgcladeinputfam2c != "") {
      datab = datab[which(datab$apgorder %in% c(input$apgcladeinputfam2c)
      )
      ,]
      datab <- droplevels(datab)
      datab
    } else {
      datab <- datab[which(datab$apgorder %in% (levels(datab$apgorder)[2])   
      )
      ,]
      datab <- droplevels(datab)
      datab
    }
  } )
  output$selectinputapgcladeinputgensubfam <- renderUI({
    if (length(levels(x2ndata2gencladefam()$`family_apg_aste`)) <=1 )
    {}
    else { 
      selectInput('apgcladeinputgensubfam',
                  'subfamily or clade', 
                  choices  = levels(x2ndata2gencladefam()$`family_apg_aste`), 
                  selected=levels(x2ndata2gencladefam()$`family_apg_aste`)[1]
      )
    }
  }) # end render
  output$selectinputapgcladeinputgensubfam2c <- renderUI({
    if (length(levels(x2cdata2gencladefam()$`family_apg_aste`)) <=1 )
    {}
    else { 
      selectInput('apgcladeinputgensubfam2c',
                  'subfamily or clade', 
                  choices  = levels(x2cdata2gencladefam()$`family_apg_aste`), 
                  selected=levels(x2cdata2gencladefam()$`family_apg_aste`)[1]
      )
    }
  }) # end render
  output$tclselectall <- renderUI({
    if (input$predictorclade =="Show predicted 2C")
    {}
    else { 
      actionButton("selectallcladestcl", label="Select/Deselect all")
    }
  }) # end render
  output$tclselectalleudico <- renderUI({
    if (input$predictorclade =="Show predicted 2C")
    {}
    else { 
      actionButton("selecteudicotstcl", label="Select/Deselect eudicots")
    }
  }) # end render
  output$tclselectallmonoco <- renderUI({
    if (input$predictorclade =="Show predicted 2C")
    {}
    else { 
      actionButton("selectmonocotstcl", label="Select/Deselect monocots")
    }
  }) # end render
  x2ndata2gencladesubfam<-reactive({
    datab<-x2ndata 
    if (!is.null(input$apgcladeinputgensubfam) && input$apgcladeinputgensubfam != "") 
    { datab = datab[which(datab$`family_apg_aste` %in% c(input$apgcladeinputgensubfam) 
    )
    ,]
    datab <- droplevels(datab)
    datab
    }
    else {
      datab <- datab[which(datab$`family_apg_aste` %in% (levels(datab$`family_apg_aste`)[1])   
      )
      ,]
      datab <- droplevels(datab)
      datab
    }
  }  )
  x2cdata2gencladesubfam<-reactive({
    datab<-x2cdata 
    if (!is.null(input$apgcladeinputgensubfam2c) && input$apgcladeinputgensubfam2c != "") 
    { datab = datab[which(datab$`family_apg_aste` %in% c(input$apgcladeinputgensubfam2c) 
    )
    ,]
    datab <- droplevels(datab)
    datab
    }
    else {
      datab <- datab[which(datab$`family_apg_aste` %in% (levels(datab$`family_apg_aste`)[1])   
      )
      ,]
      datab <- droplevels(datab)
      datab
    }
  }  ) #end reactive
  x2ndata2gencladefam<-reactive({
    datab <-x2ndata 
    datab <- datab[which(datab$family_apg %in% c(input$apgcladeinputgenfam) 
    )
    ,]
    datab <- droplevels(datab)
    datab
  }  )
  x2cdata2gencladefam<-reactive({
    datab<-x2cdata 
    datab = datab[which(datab$family_apg %in% c(input$apgcladeinputgenfam2c) 
    )
    ,]
    datab <- droplevels(datab)
    datab
  }  )
  x2ndata2famb<-reactive({
    datab<-x2ndata 
    if (!is.null(input$selectfamily) && input$selectfamily != "") {
      datab = datab[which(datab$family_apg %in% c(input$selectfamily) 
      )
      ,]
      datab <- droplevels(datab)
      datab
    } else {
      datab <- datab[which(datab$apgorder %in% input$apgcladeinputfam    
      )
      ,]
      datab <- droplevels(datab)
      datab
    }
  } )
  x2cdata2famb<-reactive({
    datab<-x2cdata 
    if (!is.null(input$selectfamily2c) && input$selectfamily2c != "") {
      datab = datab[which(datab$family_apg %in% c(input$selectfamily2c) 
      )
      ,]
      datab <- droplevels(datab)
      datab
    } else {
      datab <- datab[which(datab$apgorder %in% input$apgcladeinputfam    
      )
      ,]
      datab <- droplevels(datab)
      datab
    }
  } )
  x2ndata2genb<-reactive({
    datab<-x2ndata 
    if (!is.null(input$selectgenus) && input$selectgenus != "") {
      datab = datab[which(datab$genus %in% c(input$selectgenus) 
      )
      ,]
      datab <- droplevels(datab)
      datab
    } else {
      datab <- datab[which(datab$family_apg %in% input$apgcladeinputgenfam   
      )# family_apg_aste
      ,]
      datab <- droplevels(datab)
      datab
    }
  } )
  x2cdata2genb<-reactive({
    datab<-x2cdata 
    if (!is.null(input$selectgenus2c) && input$selectgenus2c != "") {
      datab = datab[which(datab$genus %in% c(input$selectgenus2c) 
      )
      ,]
      datab <- droplevels(datab)
      datab
    } else {
      datab <- datab[which(datab$family_apg %in% input$apgcladeinputgenfam2c    
      )# family_apg_aste
      ,]
      datab <- droplevels(datab)
      datab
    }
  } )
  x2ndata2fam<-reactive({
    x2ndata$famlabelcount<-as.character(x2ndata$famlabelcount)
    x2ndata <- x2ndata[with(x2ndata,order(famlabelcount)), ] #added
    x2ndata$famlabelcount = as.factor(x2ndata$famlabelcount)
    datab<-x2ndata
    if(input$par2ntypefam=="histogram"){
    if (!is.null(input$selectfamily) && input$selectfamily != "") {
      datab = datab[which(datab$family_apg %in% c(input$selectfamily) 
                          & datab$parsed2n <= input$range2nfam[1]
      )
      ,]
      datab <- droplevels(datab)
      datab
    } else {
      datab <- datab[which(datab$apgorder %in% input$apgcladeinputfam    
      )
      ,]
      datab <- droplevels(datab)
      datab
    }
    } # end histogram
    else{ # dotplot
      datab <- datab[which(datab$apgorder %in% input$apgcladeinputfam    
      )
      ,]
      datab <- droplevels(datab)
      datab
    }
  } )
  x2cdata2fam<-reactive({
    x2cdata$fam2clabelcount<-as.character(x2cdata$fam2clabelcount)
    x2cdata <- x2cdata[with(x2cdata,order(fam2clabelcount)), ] #added
    x2cdata$fam2clabelcount = as.factor(x2cdata$fam2clabelcount)
    datab<-x2cdata 
    if(input$par2ctypefam=="histogram"){
    if (!is.null(input$selectfamily2c) && input$selectfamily2c != "") {
      datab = datab[which(datab$family_apg %in% c(input$selectfamily2c) 
                          & datab$parsed2c <= input$range2cfam[1]
      )
      ,]
      datab <- droplevels(datab)
      datab
    } else {
      datab <- datab[which(datab$apgorder %in% input$apgcladeinputfam2c    
      )
      ,]
      datab <- droplevels(datab)
      datab
    }
    }# end histogram
      else{ # violin
        datab <- datab[which(datab$apgorder %in% input$apgcladeinputfam2c    
        )
        ,]
        datab <- droplevels(datab)
        datab
      }
  } )
  x2ndata2gen<-reactive({ 
    x2ndata$genlabelcount<-as.character(x2ndata$genlabelcount)
    x2ndata <- x2ndata[with(x2ndata,order(genlabelcount)), ] #added
    x2ndata$genlabelcount = as.factor(x2ndata$genlabelcount)
    datab<-x2ndata 
    if(input$par2ntypegen=="histogram"){
    if (!is.null(input$selectgenus) && input$selectgenus != "") {
      datab = datab[which(datab$genus %in% c(input$selectgenus) 
                          & datab$parsed2n <= input$range2ngen[1]
      )
      ,]
      datab <- droplevels(datab)
      datab
    } 
    else if (length(levels(x2ndata2gencladefam()$`family_apg_aste`)) >1 )
    {
      datab <- datab[which(datab$`family_apg_aste` %in% input$apgcladeinputgensubfam
      )#family_apg_aste
      ,]
      datab <- droplevels(datab)
      datab
    }
    else
    {
      datab <- datab[which(datab$family_apg %in% input$apgcladeinputgenfam
      )#family_apg_aste
      ,]
      datab <- droplevels(datab)
      datab
    }
    } # end histogram
    else{ # dotplot
      if (length(levels(x2ndata2gencladefam()$`family_apg_aste`)) >1 )
      {
        datab <- datab[which(datab$`family_apg_aste` %in% input$apgcladeinputgensubfam
        )#family_apg_aste
        ,]
        datab <- droplevels(datab)
        datab
      }
      else
      {
        datab <- datab[which(datab$family_apg %in% input$apgcladeinputgenfam
        )#family_apg_aste
        ,]
        datab <- droplevels(datab)
        datab
      }
    } # end dotplot
  } )
  x2cdata2gen<-reactive({
    x2cdata$gen2clabelcount<-as.character(x2cdata$gen2clabelcount)
    x2cdata <- x2cdata[with(x2cdata,order(gen2clabelcount)), ] #added
    x2cdata$gen2clabelcount = as.factor(x2cdata$gen2clabelcount)
    datab<-x2cdata 
    if(input$par2ctypegen=="histogram"){
    if (!is.null(input$selectgenus2c) && input$selectgenus2c != "") {
      datab = datab[which(datab$genus %in% c(input$selectgenus2c) 
                          & datab$parsed2c <= input$range2cgen[1]#was 2
      )
      ,]
      datab <- droplevels(datab)
      datab
    } 
    else if (length(levels(x2cdata2gencladefam()$`family_apg_aste`)) >1 )
    {
      datab <- datab[which(datab$`family_apg_aste` %in% input$apgcladeinputgensubfam2c
      )#family_apg_aste
      ,]
      datab <- droplevels(datab)
      datab
    }
    else
    {
      datab <- datab[which(datab$family_apg %in% input$apgcladeinputgenfam2c
      )#family_apg_aste
      ,]
      datab <- droplevels(datab)
      datab
    } 
    } # end histogram
    else{ # violinplot
      if (length(levels(x2cdata2gencladefam()$`family_apg_aste`)) >1 )
      {
        datab <- datab[which(datab$`family_apg_aste` %in% input$apgcladeinputgensubfam2c
        )#family_apg_aste
        ,]
        datab <- droplevels(datab)
        datab
      }
      else
      {
        datab <- datab[which(datab$family_apg %in% input$apgcladeinputgenfam2c
        )#family_apg_aste
        ,]
        datab <- droplevels(datab)
        datab
      }
    } # end dotplot
  } )
  output$hover_dados <-  renderUI({
    dadosparatabela()
  })
  output$hover_dadosauthor <-  renderUI({
    datafortableauthors()
  })
  text4=reactive({
    test_true=list(a(),b)
    if (all(!is.null(input$selectcountry),!input$selectcountry == "", !comp_list4(test_true)  )  )  
    {
      if (length(input$selectcountry)==2 || length(input$selectcountry)==1 ){
        samplet= paste(", collected in:",paste(c(input$selectcountry),collapse=" and " )  )
      } 
      else {
        samplet= paste(", collected in:",paste(c(input$selectcountry[[1]],", ",input$selectcountry[[2]]," and ",input$selectcountry[[3]] ),collapse="" ) )
      }
    } #end 1st if
    else {
      samplet=paste("")
    }
  }) # end text4
  bubbleselected= reactive({
    if(!is.null(input$hoverinput))
    {
      if(length(levels(as.factor(summarytech$range))<round(input$hoverinput$x)) & round(input$hoverinput$x) > 0 
         & length(levels(as.factor(summarytech$variable))<round(input$hoverinput$y)) & round(input$hoverinput$y) > 0
         ) {
      hover=input$hoverinput
      x= summarydata()$variable
      x <- factor(x, levels = standardnamestechniques)
      summaryselected=tryCatch(summarydata()[which(summarydata()$range  %in% levels(as.factor(summarytech$range))[[round(hover$x)]] & 
                                            summarydata()$variable %in% levels(x)[[round(hover$y)]] 
                                          &
                                            (summarydata()$N >0)  
      ) #end which
      ,  ],error = function(e) {print(paste("Select a bubble")) ; "Select a bubble" } 
      ) # end trycatch
      }
    }
  }) # end reactive bubble chart 1
  output$outauthor=renderUI({
    if(!invalid(input$plot_clickauthor) && !invalid(tblres$tblauthors) ){ 
    if(!is.null(input$plot_clickauthor) && nrow(tblres$tblauthors) > 0){     
      HTML('<div id="texthtml333"  style="width: 90%;padding: 5px 0px 0px 10px" >',
           paste0("Cites in table tab: "),
           paste(tblres$cite, collapse=", ")
           , # end paste 
           '</div>' 
      ) # end html
        }
    }
      else
    {""}
  })# 
    observeEvent({ 
      input$plot_clickauthor
    }, { 
    validate(
      need(try(!invalid(coauthorrelated$plot)), " 2495" ) 
    )
    if(!is.null(input$plot_clickauthor))
    {
      hover=input$plot_clickauthor
      gdata<- tryCatch(coauthorrelated$plot$data, error = function(e) {print(paste("not enough data")) ; "not enough data, select more countries" } )
      textau<-tryCatch(gdata$label[(round(gdata$x*5,1)  %in% round(hover$x*5,1 )) & (round(gdata$y*5,1)  %in% round(hover$y*5,1 ))  ], 
                       error = function(e) {print(paste("not enough data")) ; "not enough data, select more countries" } )
      if (typeof(textau) == "character"){ 
            cite<-authorduplicated$uniquecite[(authorduplicated$authoralone %in% textau) & (authorduplicated$year >= values[["yearauthormin"]]) &
                                        (authorduplicated$year <= values[["yearauthormax"]]) # isolate(input$rangeyearnetwork[2]) )
                                        ]
    selected<-  techmatch[(techmatch$uniquecite %in% cite),] # dataframe
    selected <- droplevels(selected)
    tblres$tblauthors<-selected
    tblres$tblauthors<- tblres$tblauthors[, c(2:5,21,20,14,10,12,16:19,22,23,24,25), with=FALSE] #datatable
       tblres$centeredauthor<- c(11:12)
    tblres$linkedcolauthor<-c(15)
    tblres$renamedcolsauthor<-c("Clade"="apgorder", "parsed 2n"="parsed2n","parsed 2C"="parsed2c")#,"FISH (molec. cyto.)"="FISH (molecular cyt.)")
    tblres$cite<-cite
      }
      else{}
    }
  }) # end reactive bubble chart 1
  bubbleaffiselected= reactive({
    if(!is.null(input$hoverinput2))
    {
      hover=input$hoverinput2
      x=factor(summaryaffi()$affispecific)
      droplevels(x)
      standardnamescountry<-intersect(standardnamescountryaffi,levels(x) )
      x <- factor(x, levels = standardnamescountry)   
      droplevels(x)
      summaryselected=tryCatch(summaryaffi()[which(summaryaffi()$range2  %in% levels(as.factor(summaryaffi()$range2))[[round(hover$x)]] & 
                                          summaryaffi()$affispecific %in% levels(x)[[round(hover$y)]] 
                                        &
                                          !is.null(summaryaffi()$count) 
      ) #end which
      ,  ],   error = function(e) {print(paste("not enough data")) ; "not enough data, select more countries" } ) # end subset
    }
  }) # end reactive bubble chart 1
  dadosparatabela= reactive({
    if(!is.null(input$hoverinput) & !invalid(bubbleselected() )  )
    {
      if (typeof(bubbleselected())=="character"){} 
      else{
      if (!is.na(bubbleselected()$N) & bubbleselected()$N >0) { 
        hover=input$hoverinput
        x= summarytech$variable #cambiado por data()
        x <- factor(x, levels = standardnamestechniques )
        HTML('<div id="text3"  style="width: 50%;padding: 5px 0px 0px 10px" >',
             paste("Clicking there you will generate a table of sampled plants of the year interval:",
                   levels(as.factor(summarytech$range))[[round(hover$x)]],"with:",
                   levels(x)[[round(hover$y)]], 
                   text4(), sep = ' '
             ), # end paste 
             '</div>' 
        ) # end html
      } #end second if
      else {h3("") }
    }
            } #end 1st if
  } ) # end reactive
datafortableauthors= reactive({  
  tryCatch(
    if(!is.null(input$hoverinputauthor) ) # & !invalid(bubbleselected() )  )
    {
        hover=input$hoverinputauthor
        gdata<- tryCatch(coauthorrelated$plot$data,error = function(e) {print(paste("not enough data")) ; "not enough data, please select more countries" } )
        textau<-gdata$label[(round(gdata$x*5,1)  %in% round(hover$x*5,1 )) & (round(gdata$y*5,1)  %in% round(hover$y*5,1 ))  ]
        if(!invalid(textau) )
        { 
        HTML('<div id="text3"  style="width: 50%;padding: 5px 0px 0px 10px" >',
             paste0("Clicking there you will generate a table of articles by: "),
             paste(textau, collapse=", ")
             , # end paste 
             '</div>' 
        ) # end html
        }  
     } #end second if
      else {
        h3("") 
        }, error = function(e) {print(paste("not enough data")) ; "not enough data, select more countries please" } )
  }  )
  x2ndatanrow = reactive ({nrow(x2ndata2() )})
  x2cdatanrow = reactive ({nrow(x2cdata2() )})
  x2ndatafamnrow = reactive ({nrow(x2ndata2fam() )})
  x2cdatafamnrow = reactive ({nrow(x2cdata2fam() )})
  x2ndatagennrow = reactive ({nrow(x2ndata2gen() )})
  x2cdatagennrow = reactive ({nrow(x2cdata2gen() )})
  tcnrow = reactive ({
    if 
    (input$predictorclade =="Show predicted 2C")
      rown<-nrow( graph5table()  )-length(tcreference)
    else {
      rown<-nrow( graph5table()   )
    } 
  })
  output$titlehisto=renderText({
    if(input$par2ntype=="histogram")
    samplet=paste(c("Parsed 2n in" ,x2ndatanrow()," angiosperms"))
    else
    title<-"Parsed 2n in angiosperms"  
  })# end 
  output$titlehisto2c=renderText({
    samplet=paste(c("Parsed 2C in" ,x2cdatanrow()," angiosperms"))
  })# end 
  angiospermperson = reactive ({
    if (x2ndatafamnrow() >1) {"angiosperms"}
    else {"angiosperm"}
  })
  angiospermgenperson = reactive ({
    if (x2ndatagennrow() >1) {"angiosperms"}
    else {"angiosperm"}
  })
  angiospermperson2c = reactive ({
    if (x2cdatafamnrow() >1) {"angiosperms"}
    else {"angiosperm"}
  })
  angiospermgenperson2c = reactive ({
    if (x2cdatagennrow() >1) {"angiosperms"}
    else {"angiosperm"}
  })
  angiospermpersontc = reactive ({
    if (tcnrow() >1) {"angiosperms"}
    else {"angiosperm"}
  })
  output$titlehistofam=renderText({
    if(input$par2ntypefam=="histogram")
      samplet=paste(c("Parsed 2n in" ,x2ndatafamnrow(), angiospermperson() ) )
    else
      title<-"Parsed 2n in angiosperms"  
  })# end 
  output$titlehistofam2c=renderText({
    samplet=paste(c("Parsed 2C in" ,x2cdatafamnrow(), angiospermperson2c() ) )
  })# end text1
  output$titlehistogen=renderText({
    samplet=paste(c("Parsed 2n in" ,x2ndatagennrow(), angiospermgenperson() ) )
  })# end text1
  output$titlehistogen2c=renderText({
    samplet=paste(c("Parsed 2C in" ,x2cdatagennrow(), angiospermgenperson2c() ) )
  })# end text1
  output$titlehistotc=renderText({
    if (input$inputTCAorTCL=="TCL and Genome size")
    { 
      samplet=paste(c("TCL in",tcnrow(), angiospermpersontc(),"with unknown 2C-value"  ) )
    }
    else
    {
      samplet=paste(c("TCA in",tcnrow(), angiospermpersontc(),"with unknown 2C-value"  ) )
    }
  })# end text1
  
  output$markdown <- renderUI({
    HTML(markdown::markdownToHTML(knit('about.Rmd', quiet = TRUE)))
  })
  
  output$abouttext=renderUI({
    HTML('<div id="text30"  style="width: 50%;padding: 20px 20px 20px 20px" >
           <br><span>-',paste(myper),' of the Cerrado plant list has been searched</span></br>
           - Several search engines used</br>
          </br>
Data are from original sources, the genera included until now are:
          </span><i>   ', 
         paste( 
           levels(as.factor(techg$genus) )  , collapse= ", "
         ), # end paste 
         '</i></div> 
     <strong style ="padding: 20px 20px;">How to cite: </strong>
<span>
<a target=\"top\" href=\"https://doi.org/10.3897/CompCytogen.v11i2.11395">
Roa, F; Telles, MPC, 2017. The Cerrado (Brazil) Plant cytogenetics database. COMPARATIVE CYTOGENETICS, v. 11, p. 285-297</a></span>
                          <br></br>
<span style ="padding: 20px 20px;"><a target=\"top\" href=\"https://gitlab.com/ferroao/cerradocytopu">Code of this shiny app in gitlab</a></span>
         <br></br>
                          <strong style ="padding: 20px 20px;">Other sources:</strong>
<br></br>      
<span style ="padding: 20px 20px;"><a target=\"top\" href=\"http://ccdb.tau.ac.il/">Rice et al. 2015. Chromosome counts database CCDB</a></span>
<br></br>
<span style ="padding: 20px 20px;"><a target=\"top\" href=\"http://data.kew.org/cvalues/">Leitch and Bennett 2012. Kew C-value database</a></span>
    <br></br> 
<span style ="padding: 20px 20px;"><a target=\"top\" href=\"http://www.plantminer.com">plantminer</a></span>
<br></br>
<span style ="padding: 20px 20px;">Cerrado : Ecologia e Flora. vol. 2 2008 by Sueli Matiko Sano et al.</span>
      ') #end html
}) #end rendertext
  
  output$abouttable<-renderUI({
    HTML('  
          <div id="text31"  style="width: 50%;padding: 20px 20px 20px 20px" >
          <span><b>Use species (without author), genus or family. Infraspecific level is ignored. Approximate searching allowed</b></span><br>',
         "<br>", 
         paste("- Histograms for 2n and 2C in next tab"),
         '<br><span>-',paste(myper),' of the Cerrado plant list has been searched</span>
<br>- Several search engines used<br>
<span>- Data are from original sources
<br>- For the genera included see "About" at the left menu</span>
          </div> 
     <strong style ="padding: 20px 20px;">How to cite: </strong>
<span>
<a target=\"top\" href=\"https://doi.org/10.3897/CompCytogen.v11i2.11395">
Roa, F; Telles, MPC, 2017. The Cerrado (Brazil) plant cytogenetics database. COMPARATIVE CYTOGENETICS, v. 11, p. 285-297</a></span>
<br></br>
<span style ="padding: 20px 20px;"><a target=\"top\" href=\"https://gitlab.com/ferroao/cerradocytopu">Code of this shiny app in gitlab</a></span>
<br></br>
<strong style ="padding: 20px 20px;">Other links:</strong>
<br></br>                          
<span style ="padding: 20px 20px;"><a target=\"top\" href=\"http://ccdb.tau.ac.il/">Rice et al. 2015. Chromosome counts database CCDB</a></span>
<br></br>
<span style ="padding: 20px 20px;"><a target=\"top\" href=\"http://data.kew.org/cvalues/">Leitch and Bennett 2012. Kew C-value database</a></span>
<br></br> 
<span style ="padding: 20px 20px;"><a target=\"top\" href=\"http://www.plantminer.com">plantminer</a></span>
<br></br>
<span style ="padding: 20px 20px;">Cerrado : Ecologia e Flora. vol. 2 2008 by Sueli Matiko Sano et al.</span>
      <br><br>
<!-- hitwebcounter Code START -->
<img style ="padding: 20px 20px;" src="http://hitwebcounter.com/counter/counter.php?page=6534861&style=0025&nbdigits=5&type=page&initCount=0" title="" Alt=""   border="0" >
<span>visits</span><br/>
<!-- hitwebcounter.com -->
         ') #end html
  }) #end rendertext
  title5="Chronology of cytogenetic data published for Cerrado plant species collected in:"
  title6="Chronology of cytogenetic data published for Cerrado plant species"
  output$text1=renderText({
    test_true=list(a(),b)
    if (all(!is.null(input$selectcountry),!input$selectcountry == "", !comp_list4(test_true)  )  )  
    {
      if (length(input$selectcountry)==2 || length(input$selectcountry)==1 ){
        samplet= c(paste(c(title5)),paste(c(input$selectcountry),collapse=" and " ))
      } 
      else {
        samplet= paste(c(title5,input$selectcountry[[1]],", ",input$selectcountry[[2]]," and ",input$selectcountry[[3]]
        ) )  
      }
    } #end 1st if
    else {
      samplet=paste(title6)
    }
  })# end text1
  output$bubbletechniquebox = renderUI({
    box(width=12,  height="580px",
        title = textOutput("text1"),
        status="primary", 
        solidHeader=T,  
        plotOutput("bubbletechniqueplot", hover=hoverOpts(id="hoverinput"), click =clickOpts(id="plot_click") 
        ),
        HTML('Pass mouse over bubbles and click to generate table')
        ,
        htmlOutput("hover_dados")
    )# box
  }) #renderUI BOX
  output$bubbleaffiboxes <- renderUI({  
    div(id = "bubbleaffiboxesdiv",
        HTML("<div class='col-sm-3' style='min-width: 200px !important; padding:5px 5px 0px 5px;'>"),
        uiOutput("bubbleprovenancebox"),
        HTML("</div>"),
        HTML("<div class='col-sm-7' style='padding:5px 5px 0px 5px;'>"),
        tryCatch(uiOutput("bubbleaffibox"),error = function(e) {print(paste("not enough data")) ; "not enough data 3028" } ),
        HTML("</div>") 
    ) # end div
  })
  output$bubbleaffibox = renderUI({
    box(width=12,  height="580px",
        title = "Chronology and author affiliation of cytogenetic studies of Cerrado plant species", # collected in Brazil",
        status="primary", 
        solidHeader=T,  
        plotOutput("bubbleaffiplot", hover=hoverOpts(id="hoverinput2"), click =clickOpts(id="plot_click2") 
        ),
        htmlOutput("bubblehtml"),
        HTML("<br>"),
        HTML('Pass mouse over bubbles and click to generate table')
        ,
        htmlOutput("hover_dados2")
    )# box
  }) #renderUI BOX  
  output$x2nhistobox = renderUI({
    box(width=12,  height=lenplots$h,#"650px",
      title = textOutput("titlehisto"),
      status="primary", 
      solidHeader=T,  
      plotOutput("x2nhistoplot", height = lenplots$heightofplot1#)height= 530 #,  hover=hoverOpts(id="hoverinput2"), click =clickOpts(id="plot_click2") 
      )
      ,             htmlOutput("modedisclaimerclade")   
  )# box
})
output$modedisclaimerclade<-renderUI({
  if(input$par2ntype=="histogram"){
  if(!invalid(input$x2nlegendtype)){
  if (input$x2nlegendtype == "3 modes and counts" )# | input$x2nlegendtypefam == "3 modes and counts" | input$x2nlegendtype == "3 modes and counts")
  {
    HTML('<div  style="width: 50%;padding: 25px 15px 15px 10px" >',
         paste("Modes are simply the three most common numbers; frequency ties separated by comma") , '</div>' )
  }
  }
  }
  else if(input$par2ntype=="dotplot")
    HTML('<div  style="width: 50%;padding: 25px 15px 15px 10px" >',
         paste("Three most common 2n and sample size in parentheses; frequency ties separated by comma") , '</div>' )
})
output$modedisclaimergen<-renderUI({
  if(input$par2ntypegen=="histogram"){
    if(!invalid(input$x2nlegendtypegen)){
      if (input$x2nlegendtypegen == "3 modes and counts" )# | input$x2nlegendtypefam == "3 modes and counts" | input$x2nlegendtype == "3 modes and counts")
      {
        HTML('<div style="width: 50%;padding: 25px 15px 15px 10px" >',
             paste("Modes are simply the three most common numbers") , '</div>' )
      }
    }
  }
  else if(input$par2ntypegen=="dotplot")
    HTML('<div  style="width: 50%;padding: 25px 15px 15px 10px" >',
         paste("Three most common 2n and sample size in parentheses; frequency ties separated by comma") , '</div>' )
})
output$modedisclaimerfam<-renderUI({
if(input$par2ntypefam=="histogram"){
  if(!invalid(input$x2nlegendtypefam)){
    if (input$x2nlegendtypefam == "3 modes and counts" )# | input$x2nlegendtypefam == "3 modes and counts" | input$x2nlegendtype == "3 modes and counts")
    {
      HTML('<div style="width: 50%;padding: 25px 15px 15px 10px" >',
           paste("Modes are simply the three most common numbers") , '</div>' )
    }
  }
}
else if(input$par2ntypefam=="dotplot")
  HTML('<div  style="width: 50%;padding: 25px 15px 15px 10px" >',
       paste("Three most common 2n and sample size in parentheses; frequency ties separated by comma") , '</div>' )
})
output$modedisclaimerclade2c<-renderUI({
  if(input$par2ctype=="violinplot"){
    HTML('<div style="width: 50%;padding: 25px 15px 15px 10px" >',
         paste("Numbers are the three highest peaks of the Gaussian kernel density and sample size in parentheses") , '</div>' )
    }
  else if(!invalid(input$x2clegendtype)){
    if (input$x2clegendtype == "3 modes and counts" )# | input$x2nlegendtypefam == "3 modes and counts" | input$x2nlegendtype == "3 modes and counts")
    {
      HTML('<div style="width: 50%;padding: 25px 15px 15px 10px" >',
           paste("Modes are the three highest peaks of the Gaussian kernel density") , '</div>' )
    }
  }
})
output$modedisclaimergen2c<-renderUI({
  if(input$par2ctypegen=="violinplot"){
    HTML('<div style="width: 50%;padding: 25px 15px 15px 10px" >',
         paste("Numbers are the three highest peaks of the Gaussian kernel density and sample size in parentheses") , '</div>' )
  }
  else if(!invalid(input$x2clegendtypegen)){
    if (input$x2clegendtypegen == "3 modes and counts" )# | input$x2nlegendtypefam == "3 modes and counts" | input$x2nlegendtype == "3 modes and counts")
    {
      HTML('<div style="width: 50%;padding: 25px 15px 15px 10px" >',
           paste("Modes are the three highest peaks of the Gaussian kernel density") , '</div>' )
    }
  }
})
output$modedisclaimerfam2c<-renderUI({
  if(input$par2ctypefam=="violinplot"){
    HTML('<div style="width: 50%;padding: 25px 15px 15px 10px" >',
         paste("Numbers are the three highest peaks of the Gaussian kernel density and sample size in parentheses") , '</div>' )
  }
  else if(!invalid(input$x2nlegendtypefam)){
    validate(
      need(try(!invalid(input$x2clegendtypefam)), "Wait 3855")
    )
    if (input$x2clegendtypefam == "3 modes and counts" )# | input$x2nlegendtypefam == "3 modes and counts" | input$x2nlegendtype == "3 modes and counts")
    {
      HTML('<div style="width: 50%;padding: 25px 15px 15px 10px" >',
           paste("Modes are the three highest peaks of the Gaussian kernel density") , '</div>' )
    }
  }
})
output$x2chistobox = renderUI({
  box(width=12,  height=lenplots$h2c,#"650px",
      title = textOutput("titlehisto2c"),
      status="primary", 
      solidHeader=T,  
      plotOutput("x2chistoplot", height= lenplots$heightofplot12c# 530 
      )
      ,             htmlOutput("modedisclaimerclade2c")  
  )# box
}) #renderUI BOX
output$x2nhistofambox = renderUI({
  validate(
    need(try(!invalid(input$par2ntypefam)), "Wait 3621")
  )
  box(width=12,  
      height=lenplots$hfam,#"760px",
      title = textOutput("titlehistofam"),
      status="primary", 
      solidHeader=T,  
      plotOutput("x2nhistofamplot" ,
                 height= lenplots$heightofplot1fam#640 #,  hover=hoverOpts(id="hoverinput2"), click =clickOpts(id="plot_click2") 
      )
     ,             htmlOutput("modedisclaimerfam")  
  )# box
}) #renderUI BOX
output$x2chistofambox = renderUI({
  box(width=12,  
      height=lenplots$h2cfam,#"760px",
      title = textOutput("titlehistofam2c"),
      status="primary", 
      solidHeader=T,  
      plotOutput("x2chistofamplot" ,
                 height= lenplots$heightofplot12cfam#640 #,  hover=hoverOpts(id="hoverinput2"), click =clickOpts(id="plot_click2") 
      )
     , htmlOutput("modedisclaimerfam2c")  
  )# box
}) #renderUI BOX
output$x2nhistogenbox = renderUI({
  validate(
    need(try(!invalid(input$par2ntypegen)), "Wait 3850")
  )
  validate(
    need(try(!invalid(lenplots$hgen)), "Wait 3853")
  )
  validate(
    need(try(!invalid(lenplots$heightofplot1gen)), "Wait 3853")
  )
  box(width=12,  
      height=lenplots$hgen,#"760px",
      title = textOutput("titlehistogen"),
      status="primary", 
      solidHeader=T,  
      plotOutput("x2nhistogenplot" ,
                 height= lenplots$heightofplot1gen#
      )
      ,  htmlOutput("modedisclaimergen")  
  )# box
}) #renderUI BOX
output$x2chistogenbox = renderUI({
  validate(
    need(try(!invalid(lenplots$h2cgen)), "Wait 4641")
  )
  if(lenplots$h2cgen=="px"){"not enough data for violinplot, choose histogram or other taxon"} else
  box(width=12,  
      height=lenplots$h2cgen,#"760px",
      title = textOutput("titlehistogen2c"),
      status="primary", 
      solidHeader=T,  
      plotOutput("x2chistogenplot" ,
                 height= lenplots$heightofplot12cgen#640 #,  hover=hoverOpts(id="hoverinput2"), click =clickOpts(id="plot_click2") 
      )
     ,             htmlOutput("modedisclaimergen2c")  
  )# box
}) #renderUI BOX
  output$histodbbox = renderUI({
    validate(
      need(tblres$tblquerygenus$genus != "", "Please make a valid search on the previous tab" )
    )    
    validate(
      need(try(!invalid(lenplots$hquery)), "Wait 4769")
    )
    if(lenplots$hquery=="px"){"not enough data, choose histogram or other taxon"} else
    box(width=12,  
        height=lenplots$hquery,#"730px",
        title = textOutput("titlehistodb"),
        status="primary", 
        solidHeader=T,  
        plotOutput("histoqueryplot" ,
                   height= lenplots$heightofplot1query# 640 #,  hover=hoverOpts(id="hoverinput2"), click =clickOpts(id="plot_click2") 
        ),
        htmlOutput("disclaimer"),       
        htmlOutput("querydisclaimer")   
        )# box
  }) #renderUI BOX
  output$disclaimer<-renderUI({
    if (input$radiohisto =="2n - Chromosome number"){
    validate(
      need(try(!invalid(input$par2ntypequery)) , "wait please 4697 " )
    )      
    if(input$par2ntypequery=="dotplot"){
        HTML('<div id="textdisclaimer"  style="width: 50%;padding: 15px 15px 15px 10px" >',
             paste("3 most frequent values, sample size in parentheses, frequency ties separated by comma") , '</div>' )
    } else {
      validate(
        need(try(!invalid(input$par2ngridquery)) , "wait please 4697 " )
      )      
            if(input$par2ngridquery){}else{#==FALSE){
            if (length(levels(tblres$tblquerygenus$genus)) > 16)
            {
              HTML('<div id="textdisclaimer"  style="width: 50%;padding: 15px 15px 15px 10px" >',
                   paste("Only 16 genera are shown") , '</div>' )
            }} } }
  }) 
  output$querydisclaimer<-renderUI({
    validate(
      need(try(!invalid(input$par2ntypequery)) , "wait please 4697 " )
    )      
    if(input$par2ntypequery=="dotplot"){} else {
    validate(
      need(try(!invalid(input$x2c2nlegendtype)) , "wait please 4722 " )
    )  
    if (input$x2c2nlegendtype=="3 modes and counts") #" length(levels(tblres$tblquery$genus)) > 16)
     { 
      if (input$radiohisto =="2C - Genome size" & nrow(tblres$tblquery[which(!is.na(tblres$tblquery$parsed2c)),])>0 )
        {
        HTML('<div id="textdisclaimer"  style="width: 50%;padding: 15px 15px 15px 10px" >',
             paste("Modes are the highest peaks of the Gaussian kernel density") , '</div>' ) 
      }
      else if (input$radiohisto =="2n - Chromosome number" & nrow(tblres$tblquery[which(!is.na(tblres$tblquery$parsed2n)),])>0 ){
        HTML('<div id="textdisclaimer"  style="width: 50%;padding: 15px 15px 15px 10px" >',
             paste("Modes (max 3) are just the three most frequent numbers, and (counts)") , '</div>' ) 
      }
    }
    else {  }
    }
  })
  output$statsdisclaimer<-renderUI({ 
    textcover<-if(input$statsample=="only one data per species") { 
      "-% cover: Related to the searched fraction not the whole Flora 2020 Cerrado list"} else{""}
    HTML('<div id="textdisclaimerstat"  style="width: 35%;padding: 15px 15px 15px 10px" >',
         paste(textcover),"<br>",
         paste( "-If \"Show modes\" is selected, the 3 most frequent 2n numbers and 3 highest 2C Gaussian kernel density peaks appear"),
         "<br>",
         paste("-\"n (haploid) Even/Odd ratio > mode of n\" shows the proportion even / odd n (haploid) numbers in the sample;
                  as high n numbers (n > mode) are sometimes caused by polyploidy, they should be more frequently even
than the n numbers below the genus mode (n < mode)"),
         "<br>",
         paste( "-\"2n (>=  2x) Multiple/non-multiple of x*\" shows the proportion of 2n multiple/non-multiple of a putative* x for the  
            genus.  In some genera all large 2n (>=  2x) are of duplicated origin and the proportion is Infinite"),
         "<br>",
         paste("* The putative x was calculated as the smallest among the most frequent 2n divided by 2, it is one of the base numbers of the genus" ),
         "<br>" ,
         paste("-\"Diversity of 2n observed\" those are the number of different 2n observed inside genera"), '</div>' )
  })
  output$titlehistodb=renderText({
    samplet=paste(c(input$radiohisto,"in your query" ) )
  })# end text1
  querygenera <- reactiveValues(tbl = NULL,title=NULL)
  observe ({
      validate(
        need(try(!invalid(tblres$tblquery)), "make search in previous tab" ) #x2ndata2gen histogenplot")
      )
    validate(
      need(input$radiohisto != "", "stat form and click button" ) #x2ndata2gen histogenplot")
    )
      if (input$radiohisto =="2C - Genome size") 
      { 
        validate(
          need(try(!invalid(tblres$tblquery)), "make search in previous tab" ) #x2ndata2gen histogenplot")
        ) 
                validate( 
          need(nrow(tblres$tblquery[which(!is.na(tblres$tblquery$parsed2c)),])>0 , "No 2C-value data available" )
        )  
        data<-x2cdata 
        data <- droplevels(data)
        data = data[which(data$species %in% levels( tblres$tblquerygenus$species) ),] # selection
        data <- droplevels(data)
        data = data[which(data$genus %in% levels(data$genus)[1:16] ),] 
        data = droplevels(data)
        x2cdatagenmodes<-get.3modes.andcounts(data,"genus","parsed2c",0.5)
        x2cdatagenmodes<-as.data.frame(x2cdatagenmodes)
        validate( 
          need(nrow(x2cdatagenmodes)>0 , "No 2C-value data available" )
        )  
        title<-make.title.legend(x2cdatagenmodes)
        querygenera$titlemod<-title
        data2<-x2cdatagenmodes
        namecol<-"gen2clabelcountquery"
        x2cdatagenmodes<-make.legend.withstats(data2,namecol)
        data<-merge(data, x2cdatagenmodes, all.x = TRUE)
        data$gen2clabelcountquery = as.factor(data$gen2clabelcountquery)
        genusmedian2ccount<-data %>% group_by_at("genus") %>% 
          summarise(median = format(round(median(parsed2c), 2), nsmall = 2), counts=n() ) #, max_mpg = max(mpg) , max_disp = max(disp))
        genusmedian2ccount<-as.data.frame(genusmedian2ccount)
        title<-make.title.legend(genusmedian2ccount)
        querygenera$titlemed<-title
        data2<-genusmedian2ccount
        namecol<-"genusmediancountquery"
        genusmedian2ccount<-make.legend.withstats(data2,namecol)
        data=merge(data, genusmedian2ccount, all.x = TRUE)
        data$genusmediancountquery<-as.factor(data$genusmediancountquery)
        querygenera$tbl<-data
      }
    else { # data for 2n plot
      validate(
        need(!invalid(tblres$tblquery), "make search in previous tab" ) #x2ndata2gen histogenplot")
      )
      validate( 
        need(nrow(tblres$tblquery[which(!is.na(tblres$tblquery$parsed2n)),])>0 , "No 2n-value data available" )
      )  
      datab<-x2ndata 
      datab <- droplevels(datab)
      datab  =  datab[which(datab$species %in% levels(tblres$tblquerygenus$species) ),] # SELECTION
      datab <- droplevels(datab)
      validate(
        need(try(!invalid(input$par2ntypequery)), "Wait 4831")
        )
      if(input$par2ntypequery!="dotplot"){
        validate(
        need(try(!invalid(input$par2ngridquery)), "Wait 4912")
        )
      if(input$par2ngridquery!=TRUE){
      datab <- datab[which(datab$genus %in% levels(datab$genus)[1:16] ),] 
      datab <- droplevels(datab)
      }
      }
      genx2nmodesandcount<-get.3modes.andcounts2n(datab,"genus","parsed2n")
      genx2nmodesandcount<-as.data.frame(genx2nmodesandcount)
      validate( 
        need(nrow(genx2nmodesandcount)>0 , "No 2n-value data available" )
      )  
      title<-make.title.legend(genx2nmodesandcount)
      querygenera$titlemod<-title
      data<-genx2nmodesandcount
      namecol<-"genlabelcountquery"
      genx2nmodesandcount<-make.legend.withstats(data,namecol)
      datab=merge(datab, genx2nmodesandcount, by ="genus")
      datab$genlabelcountquery = as.factor(datab$genlabelcountquery)
      genusmedian2ncount<-datab %>% group_by_at("genus") %>% 
        summarise("median 2n" = median(parsed2n), counts=n() ) #, max_mpg = max(mpg) , max_disp = max(disp))
      genusmedian2ncount<-as.data.frame(genusmedian2ncount)
      title<-make.title.legend(genusmedian2ncount)
      querygenera$titlemed<-title
      data<-genusmedian2ncount
      namecol<-"genmedgencouquery"
      genusmedian2ncount<-make.legend.withstats(data,namecol)
      datab=merge(datab, genusmedian2ncount, all.x = TRUE)
      datab$genmedgencouquery<-as.factor(datab$genmedgencouquery)
      querygenera$tbl<-datab
    }
  })
  output$histoqueryplot <- renderPlot({
    if (input$radiohisto =="2C - Genome size")
    { 
      validate( 
        need(nrow(tblres$tblquery[which(!is.na(tblres$tblquery$parsed2c)),])>0 , "No 2C-value data available" )
      )  
      validate(
        need(try(!invalid(input$par2ctypequery)), "Wait 4831")
      )
      if(input$par2ctypequery=="violinplot"){
        print(x2cviolinplotquery())
      }
      else{ # histogram or density
        validate(
          need(try(!invalid(input$histoordensityquery)), "Wait 304")
        )
      if (input$histoordensityquery =="Histogram") 
      {
      validate(
        need(try(!invalid(input$x2c2nlegendtype)), "Wait 4855")
      )
        if(input$x2c2nlegendtype== "median and counts per taxa")
        { 
          df<-querygenera$tbl
          x <-df$genusmediancountquery
          x= as.factor(x)
          df$dummy1 <- df[, "genusmediancountquery"]
          title<-querygenera$titlemed
        }
      else{ # modes
        df<-querygenera$tbl
        x <-df$gen2clabelcountquery
        x= as.factor(x)
        df$dummy1 <- df[, "gen2clabelcountquery"]
        title<-querygenera$titlemod
      }
       plota <- ggplot(df, aes(parsed2c)) + geom_histogram( position="dodge", aes(fill=x) ) + 
        scale_fill_manual(name=paste0("  ",title), values=cbPalette) + 
        scale_y_continuous(expand=c(0,0), breaks = scales::pretty_breaks(n=15)
      ) + scale_x_continuous(breaks = scales::pretty_breaks(n=15) ) + guides(fill = guide_legend(keywidth = 1, keyheight = 1))
      plotb<- plota+ labs(x = "parsed 2C (pg)",  y = " Frequency ")  + themePGMyblank #theme(panel.grid.minor.y = element_blank())
      g3 <- plotb +themeslxS05CblyS05Cb +themelegJtPbDvBh+themelegtitCou13texCou+themePGMwPBe4tityA0S15titxS15LtexS13PtitS2AxtexS10
      } # end histogram
      else { # begin Density
        if(input$x2c2nlegendtype== "median and counts per taxa")
        { 
          title<-querygenera$titlemed
          df<-x2cdatacustomlabeldensityquery()
          x= df$genusmediancountquery
          x= as.factor(x)
          df$dummy1 <- df[, "genusmediancountquery"]
        }
        else{
          title<-querygenera$titlemod
          df<-x2cdatacustomlabeldensityquery()
          x <-df$query2clabelcountcustom
          x= as.factor(x)
          df$dummy1 <- df[, "query2clabelcountcustom"]
        }
        plota <- ggplot(df, aes(parsed2c, fill = x) ) + 
             geom_density(adjust=input$x2cadjustdensityquery,#x2cadjustreacdensityquery(), #
              alpha = 0.4 )  + guides(fill = guide_legend(keywidth = 1, keyheight = 1) ) + 
          scale_y_continuous(expand=c(0,0), breaks = scales::pretty_breaks(n=10) ) + 
          scale_x_continuous(breaks = scales::pretty_breaks(n=10) )   
        plotb<- plota + scale_fill_manual(name=paste0("  ",title), values=cbPalette)
        plotc<- plotb+ labs(x = "parsed 2C (pg)", y = " Frequency ") + themePGMyblank#theme(panel.grid.minor.y = element_blank())
        g3 <- plotc +themeslxS05CblyS05Cb +themelegJtPbDvBh+themelegtitCou13texCou+themePGMwPBe4tityA0S15titxS15LtexS13PtitS2AxtexS10  +expandl050#d1
      } # end density
        if(input$par2cgridquery)
        {
          g3+ facet_wrap(~dummy1, ncol = 3, scales = "free") +
            theme(strip.text.x = element_text(size = 14) )#
            }
        else # GRID FALSE
        {
          g3
        }
      } # histo or density
    } # 2c
    else #if (input$radiohisto =="2n - Chromosome number")
    {
      validate(
        need(try(!invalid(input$par2ntypequery)), "Wait 4939")
      )
    validate( 
      need(nrow(querygenera$tbl[which(!is.na(querygenera$tbl$parsed2n)),])>0 , "No 2n-value data available" )
    )  
    if(input$par2ntypequery=="dotplot"){
       validate(
         need(try(!invalid(input$size2ndotplotquery)), "Wait 787")
       )
      datasetplotx2nquery<-
        gridplot(querygenera$tbl,
                 "genlabelcount",
                 "parsed2n",9,
                 "Chromosome number 2n",1,105,input$size2ndotplotquery) # 2.5
      print(datasetplotx2nquery)
    } # end dotplot
    else{ # histogram
      validate(
        need(try(!invalid(input$par2ngridquery)), "Wait 1313")
      )  
      validate(
        need(try(!invalid(input$x2c2nlegendtype)), "Wait 4987")
      ) 
      if(input$x2c2nlegendtype== "median and counts per taxa")
      { 
        title<-querygenera$titlemed
        df<-querygenera$tbl#
        x <-df$genmedgencouquery
        x= as.factor(x)
        df$dummy1 <- df[, "genmedgencouquery"]
      }
      else { # modes label
        title<-querygenera$titlemod
        df<-querygenera$tbl#
        x <-df$genlabelcountquery
        x= as.factor(x)
        df$dummy1 <- df[, "genlabelcountquery"]
      }
      validate(
        need(try(!invalid(querygenera$tbl)), "Wait 4987")
      ) 
      plota<-ggplot(df, aes(parsed2n)) + geom_histogram( position="dodge", aes(fill=x  ) ) + # guides(fill = guide_legend(keywidth = 1, keyheight = 1) ) + #scale_y_continuous(
         scale_y_continuous(expand=c(0,0), breaks = scales::pretty_breaks(n=15)
        ) + scale_x_continuous(breaks = scales::pretty_breaks(n=15) )
      plotb<- plota +labs(x = "parsed 2n", y = " Frequency ") + themePGMyblank# theme(panel.grid.minor.y = element_blank())
      g3 = plotb+themeslxS05CblyS05Cb+themelegtitCou13texCou+themePGMwPBe4tityA0S15titxS15LtexS13PtitS2AxtexS10
      if(input$par2ngridquery)
      {
        validate(
          need(try(!invalid(querygenera$tbl)), "Wait 1364")
        ) 
          g3+ guides(fill=FALSE) + facet_wrap(~ dummy1, ncol = 3, scales = "free") + 
             theme(strip.text.x = element_text(size = 14) )#, #colour = "orange"))#, angle = 90))
      }
      else
      {
        g3 + themelegJtPbDvBh+ scale_fill_manual(name=paste0("  ",title), values=cbPalette) + 
          guides(fill = guide_legend(keywidth = 1, keyheight = 1) )
      }
    } # end histogram
    } # end 2n
  } ) #end plot 
  output$bubblehtml <-  renderUI({ 
  legendbubble<-reactive({
    if(input$referencesoracc=="ref"){
     legendbubble<-  "bubble size indicates number of references"
    }
    else if(input$referencesoracc=="acc")
    legendbubble<-  "bubble size indicates number of accessions"
    })
  legendbubble()
  })
  standardnamescountry<-reactive({
  x=factor(summaryaffi()$affispecific)
  droplevels(x)
  standardnamescountry<-intersect(standardnamescountryaffi,levels(x) )
  })
  output$hover_dados2 <-  renderUI({
    dadosparatabelaaffi()
  })
  dadosparatabelaaffi= reactive({
    try( 
      if(typeof(bubbleaffiselected()) =="character"){} else {
      if(!is.null(input$hoverinput2) & !invalid(bubbleaffiselected()$count) )
      {
        if (bubbleaffiselected()$count > 0  )
        { 
          hover=input$hoverinput2
          x=factor(summaryaffi()$affispecific)
          droplevels(x)
          standardnamescountry<-intersect(standardnamescountryaffi,levels(x) )
          x <- factor(x, levels = standardnamescountry)
          droplevels(x)
          HTML('<div id="text3"  style="width: 50%;padding: 5px 0px 0px 10px" >',paste("Clicking there you will generate a table of sampled plants of the year interval:"
                                                                                       ,
                                                                                       levels(as.factor(summaryaffi()$range2))[[round(hover$x)]],"by authors from:",
                                                                                       levels(x)[[round(hover$y)]]
          ), '</div>' )
        } #end if
        else {h3("") }
      } #end  if   
      else {h3("") }
      }
      , silent= T)
  } ) # end reactive
  tblb<-reactive({
    validate(
      need(input$plot_click != "", "Please click on the Plot to select bubble dataset" )
    )
    x= summarydata()$variable
    x <- factor(x, levels = standardnamestechniques)
    if(length(levels(as.factor(summarytech$range)) ) < round(input$plot_click$x) | round(input$plot_click$x)<1 |
        round(input$plot_click$y) <1  | length(levels(as.factor(countrydataselected()$variable) ) ) < round(input$plot_click$y) )   {}
    else{
    if("All years" == levels(as.factor(summarytech$range))[[round(input$plot_click$x)]])
    {
    tb=countrydataselected()[which(countrydataselected()$variable %in% levels(x)[[round(input$plot_click$y)]] 
                                   &
                                     !is.na(countrydataselected()$value ) 
    )
    ,  ]
    }
    else
    {
      tb=countrydataselected()[which(countrydataselected()$range  %in% levels(as.factor(summarytech$range))[[round(input$plot_click$x)]] & 
                                       countrydataselected()$variable %in% levels(x)[[round(input$plot_click$y)]] 
                                     &
                                       !is.na(countrydataselected()$value ) 
      )
      ,  ]
    }
    }
  } ) # end tblb
  tblaffi=reactive({
    validate(
      need(input$plot_click2 != "", "Please click on the Plot to select bubble dataset" )  
    )
    x=factor(summaryaffi()$affispecific)
    droplevels(x)
    standardnamescountry<-intersect(standardnamescountryaffi,levels(x) )
    x <- factor(x, levels = standardnamescountry)
    droplevels(x)
    if (!invalid(input$provenancecheckbox)){ 
        if("All years" == levels(as.factor(summaryaffi()$range2))[[round(input$plot_click2$x)]] )
        {
          tb=tryCatch(countryaffiduplicated[which(countryaffiduplicated$affispecific %in% levels(x)[[round(input$plot_click2$y)]] 
                                                  &  countryaffiduplicated$provspecific %in% input$provenancecheckbox 
          )
          ,  ],   error = function(e) {print(paste("not enough data")) ; "not enough data, select more countries" } )
          tb
          }
        else # not all years
        {
          tb=tryCatch(countryaffiduplicated[which(countryaffiduplicated$range2  %in% levels(as.factor(summaryaffi()$range2))[[round(input$plot_click2$x)]] 
                                                  & 
                                                    countryaffiduplicated$affispecific %in% levels(x)[[round(input$plot_click2$y)]] 
                                                  & 
                                                    countryaffiduplicated$provspecific %in% input$provenancecheckbox 
          )
          ,  ],   error = function(e) {print(paste("not enough data")) ; "not enough data, select more countries" } )
          tb
        }
    } # end provenance
    else {
      if("All years" == levels(as.factor(summaryaffi()$range2))[[round(input$plot_click2$x)]] )
      {
        tb=tryCatch(countryaffiduplicated[which(countryaffiduplicated$affispecific %in% levels(x)[[round(input$plot_click2$y)]] 
        )
        ,  ], error = function(e) {print(paste("not enough data")) ; "not enough data, select more countries" } )
        tb
      }
      else # not all years
      {
        tb=tryCatch(countryaffiduplicated[which(countryaffiduplicated$range2  %in% levels(as.factor(summaryaffi()$range2))[[round(input$plot_click2$x)]] 
                                                & 
                                                  countryaffiduplicated$affispecific %in% levels(x)[[round(input$plot_click2$y)]] 
        )
        ,  ], error = function(e) {print(paste("not enough data")) ; "not enough data, select more countries" } )
        tb
      }
    }
  } ) # end tblaffi
  tblauthor=reactive({
    validate(
      need(input$plot_clickauthor != "", "Please click on the network to select author publications" )  
    )
    x=factor(summaryaffi()$affispecific)
    droplevels(x)
    standardnamescountry<-intersect(standardnamescountryaffi,levels(x) )
    x <- factor(x, levels = standardnamescountry)
    droplevels(x)    
    tb=tech[which(tech$uniquecite  %in% levels(as.factor(summaryaffi()$range2))[[round(input$plot_click2$x)]] & 
                                     tech$affispecific %in% levels(x)[[round(input$plot_click2$y)]] 
    )
    ,  ]
  } ) # end tb
  tbl=reactive({
    if(typeof(tblb())=="list"){
    x= summarydata()$variable #cambiado data()
    x <- factor(x, levels = standardnamestechniques )
    d <- tblb()
    colnames(d)[colnames(d)=='value'] <- levels(x)[[round(input$plot_click$y)]]
    d
    }
  })
  output$table1 = DT::renderDataTable({
    validate(
      need(typeof(tbl())=="list", "Please select genera in previous tab and click generate table" )
    )
                DT::datatable(tbl()[, c(6,4,2,5,16,10,11,9,8), with = FALSE],   
              colnames= c("parsed 2C"="parsed2c", "parsed 2n"="parsed2n"), rownames=F,
                  extensions = c('Responsive', 'ColReorder'), 
                  options = list(  
                    dom = 'Rlfrtip',
                    scrollX = TRUE, 
                    columnDefs = list(list(
                      className = 'dt-center', targets = c(4,5,6)
                    ), #sublist
                    list(targets = 7,
                         render = JS(
                           "       function(data, type, row, meta) {",
                           "if(data == null){return '';}else{return '<a target=\"top\" href=\"http://' +data+ ' \">Download</a>';}",
                           "       } "
                         ) # end render
                    ) #sublist #ok   
                    )#BIGLIST
                  )# optionslist ,
                  , escape = TRUE
    )  %>%
      formatRound(columns=c('parsed 2C'), digits=2)
  } 
  ) #rendertable
  output$table2 = DT::renderDataTable({
    validate(
      need(typeof(tblaffi())=="list", "Please select genera in previous tab and click generate table" )
    )
    DT::datatable(tblaffi()[, columnstoshow()], #, with = FALSE], #WITH false, but now is not a data.table only dataframe  
                  colnames= c("parsed 2C"="parsed2c", "parsed 2n"="parsed2n"), 
                  rownames=F,
                  extensions = c('Responsive', 'ColReorder'), 
                  options = list(  
                    dom = 'Rlfrtip',
                    scrollX = TRUE, 
                    columnDefs = list(list(
                      className = 'dt-center', targets = c(4,5)
                    )
                    , #sublist
                    list(targets = 1,
                         render = JS(
                           "       function(data, type, row, meta) {",
                           "if(data == null){return '';}else{return '<a target=\"top\" href=\"http://' +data+ ' \">Download</a>';}",
                           "       } "
                         ) # end render
                    ) #sublist #ok   
                    )#BIGLIST
                  )# optionslist ,
                  , escape = TRUE
    )          %>%
      formatRound(columns=c('parsed 2C'), digits=2)
  } 
  ) #rendertable
  output$table3histogen = DT::renderDataTable({
    validate(
      need(!is.null(input$tablehistogenbutton), "Please select genera in previous tab and click generate table" )
    )
    DT::datatable(
      tblhistogen()[, columnstoshowtblhistogen(), ], #with = FALSE],   
      colnames= c("parsed 2C"="parsed2c", "parsed 2n"="parsed2n"), 
      rownames=F,
      extensions = c('Responsive', 'ColReorder'), 
      options = list(  
        dom = 'Rlfrtip',
        scrollX = TRUE, 
        columnDefs = list(list(
          className = 'dt-center', targets = c(4,5)
        )
        , #sublist
        list(targets = 1,
             render = JS(
               "       function(data, type, row, meta) {",
               "if(data == null){return '';}else{return '<a target=\"top\" href=\"http://' +data+ ' \">Download</a>';}",
               "       } "
             ) # end render
        ) #sublist #ok   
        )#BIGLIST
      )# optionslist ,
      , escape = TRUE
    )          %>%
      formatRound(columns=c('parsed 2C'), digits=2)
  } # 
  ) #rendertable
  output$table4histogen = DT::renderDataTable({
    validate(
      need(!is.null(input$tablehistogenbutton2c), "Please select genera in previous tab and click generate table" )
    )
    DT::datatable(
      tblhistogen2c()[, columnstoshowtblhistogen2c(),],   
      colnames= c("parsed 2C"="parsed2c", "parsed 2n"="parsed2n"), 
      rownames=F,
      extensions = c('Responsive', 'ColReorder'), 
      options = list(  
        dom = 'Rlfrtip',
        scrollX = TRUE, 
        columnDefs = list(list(
          className = 'dt-center', targets = c(4,5)
        )
        , #sublist
        list(targets = 1,
             render = JS(
               "       function(data, type, row, meta) {",
               "if(data == null){return '';}else{return '<a target=\"top\" href=\"http://' +data+ ' \">Download</a>';}",
               "       } "
             ) # end render
        ) #sublist #ok   
        )#BIGLIST
      )# optionslist ,
      , escape = TRUE
    )          %>%
      formatRound(columns=c('parsed 2C'), digits=2)
  } # 
  ) #rendertable   
  output$table5histotcl = DT::renderDataTable({
    validate(
      need(try(tblhistotcl$tbtcl != ""), "Wait please " ) #x2ndata2gen histogenplot")
    )
    validate(
      need(try(!is.null(input$tablehistotclbutton) | !is.null(input$tablehistotclstatbutton ) ), "Wait please "  ) #x2ndata2gen histogenplot")
    )
    DT::datatable(
      tblhistotcl$tbtcl
      ,
      colnames= tblhistotcl$renamed,
      rownames=F,
      extensions = c('Responsive', 'ColReorder'),
      options = list(
        dom = 'Rlfrtip',
        scrollX = TRUE,
        columnDefs = list(list(
          className = 'dt-center', targets = tblhistotcl$centered
        )
        , #sublist
        list(targets = tblhistotcl$linked,
             render = JS(
               "       function(data, type, row, meta) {",
               "if(data == null){return '';}else{return '<a target=\"top\" href=\"http://' +data+ ' \">Download</a>';}",
               "       } "
             ) # end render
        ) #sublist #ok
        )#BIGLIST
      )# optionslist ,
      , escape = TRUE
    )#        
  }) #rendertable   
  output$titletable1=renderText({
    if(!is.null(input$plot_click)){
        if(length(levels(as.factor(summarytech$range   )) )<round(input$plot_click$x) & 
           length(levels(as.factor(summarytech$variable)) )<round(input$plot_click$y) 
                      ){
        x= summarytech$variable #cambiado data()
      x <- factor(x, levels = standardnamestechniques )
      samplet= paste(c("Table of the selected bubble for the year interval:", 
                       levels(as.factor(summarytech$range))[[round(input$plot_click$x)]],
                       "with:", 
                       levels(x)[[round(input$plot_click$y)]],text2()
      ) 
      ) #end paste   
    } #end if     
    }
  }  ) #end reactive
  output$titletable2=renderText({
    if(!is.null(input$plot_click2)){
      x=factor(summaryaffi()$affispecific)
      droplevels(x)
      standardnamescountry<-intersect(standardnamescountryaffi,levels(x) )
      x <- factor(x, levels = standardnamescountry)
      droplevels(x)      
      samplet= paste(c("Cytogenetic data of Cerrado plants collected in Brazil for the year interval:", 
                       levels(as.factor(summaryaffi()$range2))[[round(input$plot_click2$x)]],
                       "by authors from:", 
                       levels(x)[[round(input$plot_click2$y)]] 
      ) 
      ) #end paste   
    } #end if     
  }  ) #end reactive
  output$titletable3=renderText({
    if(!is.null(input$tablehistogenbutton)){
      samplet= paste(c("Cytogenetic data of Cerrado plants from selected genera of ", 
                       input$apgcladeinputgenfam
      ) 
      ) #end paste   
    } #end if     
  }  ) #end reactive
  output$titletable4=renderText({
    if(!is.null(input$tablehistogenbutton2c)){
      samplet= paste(c("2C value of Cerrado plants from selected genera of ", 
                       input$apgcladeinputgenfam2c
      ) 
      ) #end paste   
    } #end if     
  }  ) #end reactive
  output$titletable5=renderText({
    if( tblhistotcl$current == "tclwithout2c"
    )
    { 
      samplet= paste(c("TCL in μm of species with unknown 2C-value"
      ) #end c 
      ) #end paste
    }
    else if( tblhistotcl$current == "tcawithout2c"
    )
    { 
      samplet= paste0("TCA in μm² of species with unknown 2C-value"
      ) #end paste
    }
    else if (  tblhistotcl$current == "tcland2c"
    )
    {
      samplet= paste(c("Species with known TCL and 2C-value"
      ) #end c 
      ) #end paste
    }
    else if (  tblhistotcl$current == "tcaand2c"
    )
    {
      samplet= paste(c("Species with known TCA (μm²) and 2C-value"
      ) #end c 
      ) #end paste
    }
  }  ) #end reactive
  text2=reactive({
    test_true=list(a(),b)
    if (all(!is.null(input$selectcountry),!input$selectcountry == "", !comp_list4(test_true)  )  )  
    {
      if (length(input$selectcountry)==2 || length(input$selectcountry)==1 ){
        samplet= c(paste(c(", collected in:")),paste(c(input$selectcountry),collapse=" and " ))
      } 
      else {
        samplet= paste(c(", collected in:",input$selectcountry[[1]],", ",input$selectcountry[[2]]," and ",input$selectcountry[[3]]
        ) )  
      }
    } #end 1st if
    else {
      samplet=paste("")
    }
  })# end text2
  output$tabletechniquebox = renderUI({
    if(typeof(tblb())=="list"){
    if (!is.null(tblb() ) & nrow(tblb()) > 0  ){
      box(width=12, 
          title = textOutput("titletable1"), #debe tener solo 1 elemento
          status="primary", 
          solidHeader=T,  
          if (!is.null(tblb() ) & nrow(tblb()) > 0  ){
            DT::dataTableOutput('table1')
          }
          else {
          }
      )# box
    }
    else {
      updateTabsetPanel(session, "tabsetpanel1", selected = "Plot")
    }
    }
  }) #renderUI BOX
  output$tableaffibox = renderUI({
    if(typeof(tblaffi())=="list"){
    if (!is.null(tblaffi() )  & nrow(tblaffi() ) > 0  )
    {
      box(width=12, 
          title = textOutput("titletable2"), 
          status="primary", 
          solidHeader=T,  
            DT::dataTableOutput('table2')
      )# box
    } 
    else {
      updateTabsetPanel(session, "tabsetpanel2", selected = "Plot for plants collected in Brazil")
    }
    }
  }) #renderUI BOX
  output$tablehistogenbox = renderUI({
    if (!is.null(tblhistogen() )  & nrow(tblhistogen() ) > 0  )
    {
      box(width=12, 
          title = textOutput("titletable3"), # ok
          status="primary", 
          solidHeader=T,  
          DT::dataTableOutput('table3histogen'),
          downloadButton('downloadCsv', 'Download table as .csv')  
      )# box
    } 
    else {
      updateTabsetPanel(session, "tabsetpanel3", selected = "Histogram per genus")
    }
  }) #renderUI BOX
  output$tablehistogenbox2c = renderUI({
    if (!is.null(tblhistogen2c() )  & nrow(tblhistogen2c() ) > 0  )
    {
      box(width=12, 
          title = textOutput("titletable4"), # ok
          status="primary", 
          solidHeader=T,  
          DT::dataTableOutput('table4histogen'),
          downloadButton('downloadCsv2', 'Download table as .csv')  
      )# box
    } 
    else {
      updateTabsetPanel(session, "tabsetpanel4", selected = "Histogram per genus")
    }
  }) #renderUI BOX
  output$tablehistotclbox = renderUI({
    if (!invalid(tblhistotcl$tbtcl) ) { 
      if (!is.null(tblhistotcl$tbtcl )  & nrow(tblhistotcl$tbtcl ) > 0  )
      {
        box(width=12, 
            title = textOutput("titletable5"), # ok
            status="primary", 
            solidHeader=T,  
            DT::dataTableOutput('table5histotcl'),
            downloadButton('downloadCsv3', 'Download table as .csv')  
        )# box
      } 
    }
    else {
    }
  }) #renderUI BOX
    observeEvent(input$plot_click2,{
      validate(
        need(typeof(tblaffi()) == "list", "Please click on the Plot to select bubble dataset" )
      )
    if (is.null(tblaffi() )  | nrow(tblaffi() ) == 0  ) { 
      updateTabsetPanel(session, "tabsetpanel2", selected = "Plot for plants collected in Brazil")
    }  
    else if (nrow(tblaffi())>0){
      updateTabsetPanel(session, "tabsetpanel2", selected = "Table for plants collected in Brazil")
    }
  } )
    observeEvent(input$plot_click,{
      validate(
        need(typeof(tblb()) == "list", "Please click on the Plot to select bubble dataset" )
      )
    if (is.null(tblb()) | nrow(tblb() ) == 0 ) { 
      updateTabsetPanel(session, "tabsetpanel1", selected = "Plot")
    }  
    else if (nrow(tblb())>0){
      updateTabsetPanel(session, "tabsetpanel1", selected = "Table")
    }
  } ) #end observe
  output$plotregressionbox = renderUI({
    box(width=9,  
        background = "light-blue",
        status="primary", 
        plotOutput("plotregression" 
        ),
        uiOutput("notegraphcorr")
    )# box
  }) #renderUI BOX
  micro<-reactive(
    {
      if (tcaortcl()=="TCL")
      {"μm"}
      else
      {"μm²"}
    }   
  )
  output$plotregression <- renderPlot({
    plot(corrvalues$y ~ corrvalues$x, las=1, axes=FALSE, xlab =paste("known",tcaortcl(),micro() ), ylab ="known 2C (pg)", xlim=c(0,max(corrvalues$x)+10),
         ylim=c(0,8), main="Regression with confidence and prediction interval" )
    axis(1,pos=0, at=seq(0,max(corrvalues$x)+20, 20), las=1)
    axis(2,pos=0, at=seq(0,8, 1), las=1)
    results <- reactive({ 
      r <- data.frame(y=corrvalues$y,x=corrvalues$x)
    })
    lmod <- reactive ({ 
      validate(
        need(try(input$inputlinorlog != ""), "Wait please for inputlinorlog " )
      )
      if (input$inputlinorlog!="linear"
      ){ 
        lmod <- lm(y~log(x), data = results() )
      }
      else
      {
        lmod <- lm(y~x, data = results() )
      }
    })
    limita<-max(corrvalues$x)
    x<-seq(from=1,to=200,length.out=100)
    y<-predict(lmod(),newdata=list(x=seq(from=1,to=limita,length.out=100)),  interval="confidence")
    y2<-predict(lmod(),newdata=list(x=seq(from=1,to=limita,length.out=100)),  interval="predict")
    matlines(x,y,lwd=2)
    matlines(x,y2,lwd=2)
  }) #end renderplot
  output$plotresidualsbox = renderUI({
    box(width=12,  
        status="primary", 
        title = "Residuals",
        solidHeader=T, 
        collapsible = T,
        collapsed = T,
        plotOutput("plotresiduals" 
        )
    )# box
  }) #renderUI BOX
  output$plotresiduals <- renderPlot({
    validate(
      need(try(input$inputlinorlog != ""), "Wait please for inputlinorlog plotresiduals " )
    )
    if (input$inputlinorlog=="linear to log transformed TCL"  || input$inputlinorlog=="linear to log transformed TCA"){
      results = lm(corrvalues$y ~ log(corrvalues$x) )
      resid(lm(corrvalues$y ~ log(corrvalues$x) ))
    }
    else
    {
      results = lm(corrvalues$y ~ corrvalues$x)
      resid(lm(corrvalues$y ~ corrvalues$x ) )
    }
    plot(corrvalues$x,results$res, las=1, axes=T, xlab = paste("known",tcaortcl(),micro()), ylab ="Residuals", main="Residuals" )
  })
  output$quantilequantileresbox = renderUI({
    box(width=12,  
        status="primary", 
        title = "Qualtile-quantile plot for residuals",
        solidHeader=T, 
        collapsible = T,
        collapsed = T,
        plotOutput("quantilequantileres" 
        )
    )# box
  }) #renderUI BOX
  output$quantilequantileres <- renderPlot({
    validate(
      need(try(input$inputlinorlog != ""), "Wait please " )
    )
    if (input$inputlinorlog=="linear to log transformed TCL" || input$inputlinorlog=="linear to log transformed TCA"){ 
      results = lm(corrvalues$y ~ log(corrvalues$x) )
    }
    else
    {
      results = lm(corrvalues$y ~ corrvalues$x) 
    } 
    qqnorm(results$res, main="Quantile-quantile plot for residuals")
    qqline(results$res)
  }) # end render
  output$historesbox = renderUI({
    box(width=12,  
        title = "Histogram of residuals",
        status="primary", 
        solidHeader=T, 
        collapsible = T,
        collapsed = T,
        plotOutput("histores" 
        ),
        htmlOutput("normalitytestui")
    )# box
  }) #renderUI BOX
  output$histores <- renderPlot({
    validate(
      need(try(input$inputlinorlog != ""), "Wait please " )
    )
    if (input$inputlinorlog=="linear to log transformed TCL" || input$inputlinorlog=="linear to log transformed TCA"){
      results = lm(corrvalues$y ~ log(corrvalues$x ) )
    }
    else
    {
      results = lm(corrvalues$y ~ corrvalues$x )
    }
    hist(results$res, main="Histogram of residuals", xlab="Residuals")
  }) #end render
  output$normalitytestui<-renderUI({
    validate(
      need(try(input$inputlinorlog != ""), "Wait please " )
    )
    if (input$inputlinorlog=="linear to log transformed TCL" || input$inputlinorlog=="linear to log transformed TCA"){
      results = lm(corrvalues$y ~ log(corrvalues$x ) )
    }
    else
    {
      results = lm(corrvalues$y ~ corrvalues$x)
    }
    st<-shapiro.test(results$res)
    presult<-(if(st[2]>0.05) {"As p > 0.05, the null hypothesis of a normal distribution cannot be rejected  "} 
              else {"the null hypothesis of a normal distribution is rejected"}) 
    pvalue=paste("p value:",format(signif(st[[2]],3) ) )
    testname<-paste(st[3]) 
    HTML(paste("Normality check",testname,pvalue, presult, sep= '<br/>') )
  })
  graph5table<-reactive({
    if(input$predictorclade=="Show predicted 2C")
    {  tcltable<-graph5tableorig() }
    else if(input$predictorclade=="Show Clades") { 
      if (!is.null(input$tclapgcladeinput) && input$tclapgcladeinput != "") 
      {
        tcltable2 <-graph5tableorignona()
        tcltable2 <- tcltable2[which(tcltable2$apgorder %in% c(input$tclapgcladeinput) 
        )
        ,]
        tcltable2 <- droplevels(tcltable2)
      } 
      else {
        tcltable3 <-graph5tableorignona()
      } #2nd else
    } # 1st else if
  } ) #end reactive 
  output$textcorr <-  renderUI({
    htmltextcorr()
  })
  output$notegraphcorr <-  renderUI({
    str1 <- paste("Graph considers species with known 2C and ")
    str2 <-  tcaortcl()
    HTML('<br><strong style ="padding: 20px 20px;">Outliers modified by winsorize </strong>
<span>Khan et al (2007), Alfons (2016)</span><br> <span style ="padding: 20px 20px;">',paste(str1, str2),
         '</span><br></br> ')
  })
  htmltextcorr=reactive({
    str2 <- paste("Pearson's product-moment correlation")
    str3 <- paste("r =", signif(corrtc()[[4]],4),"so, we found a medium positive correlation"
    )
    str5 <- paste("t=",signif(corrtc()[[1]],4))
    str6 <- paste("p=", signif(corrtc()[[3]],4),"so, we can reject the null hypothesis of no correlation between 2C and",tcaortcl() )
    HTML(paste(str2, str3,str5,str6, sep = '<br/>'))
  })
  output$tclhistobox = renderUI({
    box(width=12,  
        height="700px",
        title = textOutput("titlehistotc"),
        status="primary", 
        solidHeader=T,  
        plotOutput("histotc2c" ,
                   height= 640 #,  hover=hoverOpts(id="hoverinput2"), click =clickOpts(id="plot_click2") 
        )
    )# box
  }) #renderUI BOX
  x2=reactive({ 
    if(input$predictorclade=="Show predicted 2C")
    {
      x= graph5table()$predicted2c
      x=as.factor(x) 
    }
    else
    {
      x= graph5table()$ordmedcount
      x=as.factor(x)
    }
  })
  titletcl2c= reactive({
    if(input$predictorclade=="Show predicted 2C")
    {
      x= "Some predicted 2C based on linear model, see next tab"
    }
    else
    {
      x=paste("Major Clades, species with unknown 2C        median",tcaortcl(),"sample")
    }
  })
  tcaortcl<- reactive({
    if (input$inputTCAorTCL=="TCL and Genome size")
    {tc<-"TCL" }
    else 
    {tc<-"TCA" } 
  })
  output$histotc2c <- renderPlot({
    plot<- ggplot(graph5table(), aes(tc) ) + geom_histogram(binwidth=input$binstcl, aes(fill=x2()  ) ) + 
    scale_y_continuous( limits=c(0, maxtcy()), expand=c(0,0),breaks = pbytcl() ) + 
      scale_x_continuous(limits=c(0,200),breaks = pbxtcl() ) +
    scale_fill_manual(name=titletcl2c(), values=cbPalette) + # , breaks=levels(as.factor(x2cdata2fam()$family_apg) ) )    
    labs(x = paste(tcaortcl(),"in",micro() ) , y = " Frequency ") + themePGMyblank#theme(panel.grid.minor.y = element_blank())
    plota<- plot+themeslxS05CblyS05Cb+themelegJtPbDvBh+themelegtitCou13texCou+themePGMwPBe4tityA0S15titxS15LtexS13PtitS15AxtexS15#h+i+j+k 
    plota #+ coord_fixed(ratio = 0.8)
  }) #end plot 2c gen
  output$selectinputapgcladetcl <- renderUI({
    if (input$predictorclade =="Show predicted 2C")
    {}
    else { 
      checkboxGroupInput('tclapgcladeinput',
                         'Clades', 
                         choices  = levels(graph5tableorignona()$apgorder),
                         selected = levels(graph5tableorignona()$apgorder),
                         width='98%'
      )
    }
  }) # end render
  mediantcl<-techtc %>% group_by_at("apgorder") %>% filter(!is.na(tcl)) %>% 
    summarize(med = format(round(median(tcl, na.rm=TRUE), 1), nsmall = 1), 
              ctcl= n())
  mediantcl<-as.data.frame(mediantcl)  
  data<-mediantcl
  namecol<-"ordmedcounttcl"
  mediantclcount<-make.legend.withstats(data,namecol)
  mediantca<-techtc %>% group_by_at("apgorder") %>% filter(!is.na(tca)) %>% 
    summarize(med = format(round(median(tca, na.rm=TRUE), 1), nsmall = 1), 
              ctca= n())
  mediantca<-as.data.frame(mediantca)
  data<-mediantca
  namecol<-"ordmedcounttca"
  mediantcacount<-make.legend.withstats(data,namecol)
  techtcsummary<-merge(techtc, mediantclcount, by="apgorder", all = TRUE) # is dataframe
  techtcsummary<-merge(techtcsummary, mediantcacount, by="apgorder", all = TRUE)
  techtcsummary<-as.data.frame(techtcsummary)
  techtcsummary$ordmedcounttca <- as.factor(techtcsummary$ordmedcounttca)
  techtcsummary$ordmedcounttcl <- as.factor(techtcsummary$ordmedcounttcl)
  techtcsummary$apgorder <- as.factor(techtcsummary$apgorder)
  techtcor2ctc <- reactive ({
    validate(
      need(try(techtcsummary != ""), "Wait please for 3085" )
    )
    validate(
      need(try(input$inputTCAorTCL != ""), "Wait please for tcaortcl in techtcor2ctc " )
    )
    if ( input$inputTCAorTCL=="TCL and Genome size")  
    { 
      techtclor2ctcl<-techtcsummary
      techtclor2ctcl <- subset(techtclor2ctcl, select=-c(tca,parsed2c) )
      techtclor2ctcl <- techtclor2ctcl[!(is.na(techtclor2ctcl$tcl)  ), ]
      colnames(techtclor2ctcl)[(names(techtclor2ctcl) == "tcl")] <- "tc"
      colnames(techtclor2ctcl)[(names(techtclor2ctcl) == "ordmedcounttcl")] <- "ordmedcount"
      techtclor2ctcl
    }
    else if ( input$inputTCAorTCL=="TCA and Genome size")  
    { 
      techtcaor2ctca<-techtcsummary
      techtcaor2ctca <- subset(techtcaor2ctca, select=-c(tcl,parsed2c))
      techtcaor2ctca <- techtcaor2ctca[!(is.na(techtcaor2ctca$tca)  ), ]
      colnames(techtcaor2ctca)[(names(techtcaor2ctca) == "tca")] <- "tc"
      colnames(techtcaor2ctca)[(names(techtcaor2ctca) == "ordmedcounttca")] <- "ordmedcount"
      techtcaor2ctca
    }
  })
  tcand2c <- reactive({
    tcand2c<-merge(techtcor2ctc(), techtconly2c, by.x="species", by.y="species", all.x = TRUE)
      tcand2c <- tcand2c[!(is.na(tcand2c$tc)) & !(is.na(tcand2c$parsed2c) ), ]
    tcand2c$comp <- ifelse(is.na(tcand2c$ploidy.x) ==TRUE | is.na(tcand2c$ploidy.y) ==TRUE,NA,tcand2c$ploidy.x-tcand2c$ploidy.y) #crear columna nueva comparando los que no tienen na
    tcand2c <- tcand2c[tcand2c$comp==0 | (is.na(tcand2c$comp) ), ] #eliminate the ones with different ploidy levels
    tcand2c$comp2 <- ifelse(tcand2c$uniquecite.x==tcand2c$uniquecite.y,"samecite","assigned2c") #crear columna nueva comparando los que no tienen na
    tcand2c <- tcand2c[, c("apgorder","family_apg","species","uniquecite.x","link.x",
                           "parsed2n","tc","parsed2c","uniquecite.y","link.y","comp2","ordmedcount")] #
    colnames(tcand2c)[which(names(tcand2c) == "uniquecite.x")] <- "Main source"
    colnames(tcand2c)[which(names(tcand2c) == "link.x")] <- "Main source link"
    colnames(tcand2c)[which(names(tcand2c) == "uniquecite.y")] <- "2C source"
    colnames(tcand2c)[which(names(tcand2c) == "link.y")] <- "2C source link"
    tcand2c<-as.data.frame(tcand2c)
    tcand2c
  })
  tcand2cex <- reactive({
    validate(
      need(try(tcand2c() != ""), "Wait please for 3130" )
    )
    tcand2cex <- subset(tcand2c(), select=c(species,`Main source`,comp2))
  })
  tcnot2c <- reactive({
    tcnot2c <- merge(techtcor2ctc(), tcand2cex(), by.x=c("species","uniquecite"), by.y=c("species","Main source"), all.x =TRUE, allow.cartesian = TRUE)
    tcnot2c <- tcnot2c[(is.na(tcnot2c$comp2) ), ]
    tcnot2c[,"predicted2c"] <- "Species with unknown 2C" #create column
    tcnot2cb <- tcnot2c[, c("apgorder","family_apg","species","uniquecite","link","tc","parsed2n","predicted2c","ordmedcount")] #, with=FALSE] # 
    tcnot2cb$apgorder <- as.factor(tcnot2cb$apgorder)
    tcnot2cb <- as.data.frame(tcnot2cb)
    tcnot2cb
  })
  corrvalues <- reactiveValues(y = NULL,x=NULL)  
  observe({
    validate(
      need(try(tcand2c() != ""), "Wait please for corr" )
    )
    corrvalues$y<- robustHD::winsorize(tcand2c()$parsed2c)
    corrvalues$x<- robustHD::winsorize(tcand2c()$tc)
  })
  corrtc<- reactive({ 
    validate(
      need(try(input$inputlinorlog != ""), "Wait please corrtc " )
    )
    if (input$inputlinorlog=="linear to log transformed TCL" || input$inputlinorlog=="linear to log transformed TCA"){
      corr<-   cor.test(corrvalues$y, log(corrvalues$x), method = "pearson", continuity=T)
    }
    else
    {
      corr<-   cor.test(corrvalues$y, corrvalues$x, method = "pearson", continuity=T)
    }
  })
  results <- reactive({ 
    r <- data.frame(y=corrvalues$y,x=corrvalues$x)
  })   
  lmod <- reactive ({ 
    if (invalid(input$inputlinorlog) || input$inputlinorlog=="" ) {
      lmod <- lm(y ~ log(x), data = results() )
    }
    else { 
      if (input$inputlinorlog=="linear to log transformed TCL" || input$inputlinorlog=="linear to log transformed TCA"){
        lmod <- lm(y ~ log(x), data = results() )
      }
      else
      {
        lmod <- lm(y ~ x, data = results() )
      }
    }
  })
  predict.list2 <- reactive ({  
    predict2 <- predict(lmod(),newdata ) 
    predict2<-format(round(predict2,2),nsmall=2)
  })
  predictedtc2c<- reactive ({  
    predictedtc2cdf<-  data.frame(tc=tcreference, predicted2c=predict.list2() ) 
    predictedtc2cdf$predicted2c<-paste0(predictedtc2cdf$predicted2c,"pg")
    predictedtc2cdf
  })
  output$text100 <- renderTable(
    predictedtc2c()
  )
  output$correlationbox = renderUI({
    validate(
      need(try(tcaortcl() != ""), "Wait please for tcaortcl " )
    )
    box( id ="boxcorr",
         width  = 12, 
         height = "100%",
         solidHeader = TRUE, 
         status = "info",
         title= paste("Correlation for known",tcaortcl()," and 2C"),
         div(id="greeting",
             selectInput("inputlinorlog", "Select type of model to fit",
                         choices = c(paste0("linear to log transformed ",tcaortcl() ),"linear" ), 
                         multiple=F, 
                         selected = paste0("linear to log transformed",tcaortcl() ), 
                         width = '70%'),
             htmlOutput("textcorr")
         )#end div ,
    ) #box
  }) 
  graph5tableorig<- reactive ({
    graph5tableorig2 <- merge(x = tcnot2c(), y = predictedtc2c(), by=c("tc","predicted2c"), all=TRUE)
    graph5tableorig2
  })
  graph5tableorignona <- reactive ({ 
    table <- graph5tableorig()[!(is.na(graph5tableorig()$species) ), ]
    table <- droplevels(table)
    table
  })
  observe({
    if (!invalid(input$selectallcladestcl) #&  input$selectallcladestcl > 0) 
    )
    {
      if (input$selectallcladestcl %% 2 == 0){
        updateCheckboxGroupInput(session=session, 
                                 'tclapgcladeinput',
                                 'Clades', 
                                 levels(graph5tableorignona()$apgorder), 
                                 selected= c()  
        )
      }
      else {
        updateCheckboxGroupInput(session=session, 
                                 'tclapgcladeinput',
                                 'Clades', 
                                 levels(graph5tableorignona()$apgorder),
                                 selected = levels(graph5tableorignona()$apgorder)
        )
      }
    }
  }) # end observe
  observe({
    if (!invalid(input$selecteudicotstcl) #&  input$selecteudicotstcl > 0) 
    ) {
      if (input$selecteudicotstcl %% 2 == 0){
        updateCheckboxGroupInput(session=session, 
                                 'tclapgcladeinput',
                                 'Clades', 
                                 levels(graph5tableorignona()$apgorder), #  
                                 selected = c() #levels(x2ndata$apgorder)
        )
      }
      else {
        updateCheckboxGroupInput(session=session, 
                                 'tclapgcladeinput',
                                 'Clades', 
                                 levels(graph5tableorignona()$apgorder),
                                 selected= grep(glob2rx("*udicot*"), levels(graph5tableorignona()$apgorder), value = TRUE)  
        )
      }
    }
  } ) #end observe
  observe({
    if (!invalid(input$selectmonocotstcl) #&  input$selectmonocotstcl > 0) 
    ) {
      if (input$selectmonocotstcl %% 2 == 0){
        updateCheckboxGroupInput(session=session, 
                                 'tclapgcladeinput',
                                 'Clades', 
                                 levels(graph5tableorignona()$apgorder),#  
                                 selected = c() #levels(x2ndata$apgorder)
        )
      }
      else {
        updateCheckboxGroupInput(session=session, 
                                 'tclapgcladeinput',
                                 'Clades',
                                 levels(graph5tableorignona()$apgorder),#  
                                 selected= grep(glob2rx("*onocot*"),levels(graph5tableorignona()$apgorder), value = TRUE)  
        )
      }
    }
  } ) # end observe
  output$searchdbbox = renderUI({
    box(style = "padding:0px 0px 10px 10px", id ="dbbox",
        title = "Your query(ies)" , 
        width = 12, 
        solidHeader = TRUE, 
        collapsible = FALSE,
        tags$textarea(id = "query", rows= 15 , cols = 22, style="overflow:auto;resize:none",
                      "panicum aquaticus\nPaspalum\nfabacea"
        ),
        tags$br(),
        actionButton('tablequerybutton',strong('Generate table of query'),  icon("hand-pointer-o") , style="padding:10px 5px 10px 5px"
        ),
        uiOutput("errorhandler")
    ) # end box
  } ) # end output
  output$statsbox = renderUI({
    box(id ="statsbox",
        title = "Summary configuration", 
        width = NULL, 
        solidHeader = TRUE, 
        collapsible = FALSE,
        radioButtons("statsample","Sample per species: Allow",c("only one data per species","more than one (excl. 2n dupl.)") ),
        radioButtons("statlevel", "Select grouping level", c("major clades","family","genus")),
        checkboxInput("allowmodes", "Show modes", value=FALSE  )             
    ) # end box
  } ) # end output
  output$statsresultsbox = renderUI({
    box(id ="statstablebox",
        title = "Stats", width = NULL, 
        solidHeader = TRUE, 
        collapsible = TRUE,
        status="primary", 
        div(style = 'overflow-x: scroll;font-size:80%', DT::dataTableOutput('tablestats') ), #style = 'overflow-x: scroll;
        HTML("<br>"),
        uiOutput('downloadbutton7'),
        htmlOutput("statsdisclaimer")      
    ) # end box
  }) # end output
  simpletaxa<-reactive({
    simpletaxa<- as.data.frame(techg)[, c(2,3,4,25,20,21)]
    simpletaxa$speciessimple <- gsub("(\\w+)(\\s)(\\w+).*$", "\\1\\2\\3", simpletaxa$species) #get fist 2 words of species name
    simpletaxa<- as.data.frame(simpletaxa)[, c(1,2,4,5,6,7)]
    if(input$statsample=="only one data per species") {     
      onedataperspecies<-ddply(simpletaxa, .(apgorder, family_apg, genus,speciessimple), summarize, 
                               mode2n=if(isEmpty(as.numeric(names(sort(-table(na.omit(parsed2n))))[1] ) )){ #the most common 2n is gotten
                                 NA
                               } else as.numeric(names(sort(-table(na.omit(parsed2n))))[1] ),
                               mode2C=if(isEmpty(as.numeric(names(sort(-table(na.omit(parsed2c))))[1] ) )){
                                 NA
                               } else as.numeric(names(sort(-table(na.omit(parsed2c))))[1] ) )
      simpletaxa<-onedataperspecies
      simpletaxa
    } # end if
    else { 
      colnames(simpletaxa)[(names(simpletaxa) == "parsed2n")] <- "mode2n"
      colnames(simpletaxa)[(names(simpletaxa) == "parsed2c")] <- "mode2C"
      simpletaxa<-simpletaxa[!duplicated(simpletaxa[,c('mode2n','speciessimple')]),]
    }
  }) # end reactive
  stattable<- reactive({
    simpleta<-simpletaxa()
    simpleta<- merge(simpleta, parsed2xpergenus, all = TRUE )
    simpleta$basemultiple<-with(simpleta,ifelse (is.wholenumber((simpleta$mode2n/(simpleta$parsed2x/2))), 1, 0) )
    simpleta$notbasemultiple<-with(simpleta,ifelse (!is.wholenumber((simpleta$mode2n/(simpleta$parsed2x/2))), 1, 0) )
    genus2nmedian<-simpleta %>%
      group_by_at(.vars=c("apgorder","family_apg","genus")) %>%
      summarise ( genusmedian2n=tryCatch(median(mode2n, na.rm=TRUE),
                                         error = function(e) {"NA" ; "NA" } )
                  ,
                  genusmode2n=tryCatch(paste(names(sort(table(mode2n, useNA="no"),decreasing=TRUE)[1])),
                                       error = function(e) {"NA" ; "NA" } )
      )
    genus2nmedian <-as.data.frame(genus2nmedian)  
    simpleta<- merge(simpleta, genus2nmedian, all = TRUE )
    family2nmedian<-simpleta %>%
      group_by_at(.vars=c("apgorder","family_apg")) %>%
      summarise(fammedian2n=tryCatch(median(mode2n, na.rm=TRUE),
                                     error = function(e) {"NA" ;"NA" } )
                ,
                fammode2n=tryCatch(paste(names(sort(table(mode2n, useNA="no"),decreasing=TRUE)[1])),
                                   error = function(e) {"NA" ;"NA" } )
      )
    family2nmedian<-as.data.frame(family2nmedian)
    # family2nmedian
    simpleta<- merge(simpleta, family2nmedian, all = TRUE )
    clade2nmedian<-simpleta %>%
      group_by_at(.vars=c("apgorder")) %>%
      summarise(clademedian2n=tryCatch(median(mode2n, na.rm=TRUE),
                                       error = function(e) {"NA" ;"NA" } )
                ,
                clademode2n=tryCatch(paste(names(sort(table(mode2n, useNA="no"),decreasing=TRUE)[1])),
                                     error = function(e) {"NA" ;"NA" } )
      )
    clade2nmedian<-as.data.frame(clade2nmedian)
    simpleta<- merge(simpleta, clade2nmedian, all = TRUE )
    simpleta$oddn<-ifelse((is.odd(simpleta$mode2n/2)),1,0)
    simpleta$evenn<-ifelse((!is.odd(simpleta$mode2n/2)),1,0)
    simpleta$lessmodegenus<-ifelse((simpleta$mode2n<simpleta$genusmode2n), 1, 0)
    simpleta$moremodegenus<-ifelse((simpleta$mode2n>simpleta$genusmode2n), 1, 0)
    simpleta$oddlessmode<-ifelse((simpleta$lessmodegenus==1 & simpleta$oddn==1), 1, 0)
    simpleta$evenlessmode<-ifelse((simpleta$lessmodegenus==1 & simpleta$evenn==1), 1, 0)
    simpleta$oddmoremode<-ifelse((simpleta$moremodegenus==1 & simpleta$oddn==1), 1, 0)
    simpleta$evenmoremode<-ifelse((simpleta$moremodegenus==1 & simpleta$evenn==1), 1, 0)
    simpletab<-as.data.frame(simpleta[,c("apgorder","family_apg","genus","mode2n","parsed2x","basemultiple","notbasemultiple")])
    simpletab<-unique(simpletab)
    simpletab$bless1.5basegenus<-ifelse((simpletab$mode2n<2*(simpletab$parsed2x)), 1, 0)
    simpletab$bmore1.5basegenus<-ifelse((simpletab$mode2n>=2*(simpletab$parsed2x)), 1, 0)
    simpletab$bwholelessbase<-ifelse((simpletab$bless1.5basegenus==1 & simpletab$basemultiple==1), 1, 0)
    simpletab$bfraclessbase<-ifelse((simpletab$bless1.5basegenus==1 & simpletab$notbasemultiple==1), 1, 0)
    simpletab$bwholemorebase<-ifelse((simpletab$bmore1.5basegenus==1 & simpletab$basemultiple==1), 1, 0)
    simpletab$bfracmorebase<-ifelse((simpletab$bmore1.5basegenus==1 & simpletab$notbasemultiple==1), 1, 0)
    simpletabsummarygenus<-simpletab %>%
      group_by_at(.vars=c("apgorder","family_apg","genus")) %>%
      summarise(n.3 = if(n()>2)"m3" else "small",
                countnums = n(),
                bgensumwholeover1.5base= sum(bwholemorebase,na.rm=TRUE),
                bgensumwholeunder1.5base= sum(bwholelessbase,na.rm=TRUE),
                bgensumfracover1.5base= sum(bfracmorebase,na.rm=TRUE),
                bgensumfracunder1.5base= sum(bfraclessbase,na.rm=TRUE)
      )
    simpletabsummarygenus$wholefracoverbase<-
      suppressWarnings(as.numeric(format(round(simpletabsummarygenus$bgensumwholeover1.5base/simpletabsummarygenus$bgensumfracover1.5base,2),nsmall=2) ) )#ifelse(
    simpletabsummarygenus$wholefracunderbase<- 
      suppressWarnings(as.numeric(format(round(simpletabsummarygenus$bgensumwholeunder1.5base/ simpletabsummarygenus$bgensumfracunder1.5base,2),nsmall=2) ) )#)
    simpletabsummaryfam<-simpletabsummarygenus[which(simpletabsummarygenus$n.3 %in% "m3"),] %>%
      group_by_at(.vars=c("apgorder","family_apg")) %>%
      summarise(countnums = sum(countnums, na.rm=TRUE),    
                bfamsumwholeover1.5base= sum(bgensumwholeover1.5base,na.rm=TRUE),
                bfamsumwholeunder1.5base= sum(bgensumwholeunder1.5base,na.rm=TRUE),
                bfamsumfracover1.5base= sum(bgensumfracover1.5base,na.rm=TRUE),
                bfamsumfracunder1.5base= sum(bgensumfracunder1.5base,na.rm=TRUE)
      )
    simpletabsummaryfam<-as.data.frame(simpletabsummaryfam)
    simpletabsummaryfam$wholefracoverbase<-
      suppressWarnings(as.numeric(format(round(simpletabsummaryfam$bfamsumwholeover1.5base/simpletabsummaryfam$bfamsumfracover1.5base,2),nsmall=2) ) )#ifelse(
    simpletabsummaryfam$wholefracunderbase<- 
      suppressWarnings(as.numeric(format(round(simpletabsummaryfam$bfamsumwholeunder1.5base/ simpletabsummaryfam$bfamsumfracunder1.5base,2),nsmall=2)) )#)
    simpletabsummaryclade<-simpletabsummarygenus[which(simpletabsummarygenus$n.3 %in% "m3"),] %>%
      group_by_at(.vars=c("apgorder")) %>%
      summarise(countnums = sum(countnums, na.rm=TRUE),    
                bcladesumwholeover1.5base= sum(bgensumwholeover1.5base,na.rm=TRUE),
                bcladesumwholeunder1.5base= sum(bgensumwholeunder1.5base,na.rm=TRUE),
                bcladesumfracover1.5base= sum(bgensumfracover1.5base,na.rm=TRUE),
                bcladesumfracunder1.5base= sum(bgensumfracunder1.5base,na.rm=TRUE)
      )
    simpletabsummaryclade<-as.data.frame(simpletabsummaryclade)
    simpletabsummaryclade$wholefracoverbase<-
      suppressWarnings(as.numeric(format(round(simpletabsummaryclade$bcladesumwholeover1.5base/simpletabsummaryclade$bcladesumfracover1.5base,2),nsmall=2) ) )#ifelse(
    simpletabsummaryclade$wholefracunderbase<- 
      suppressWarnings(as.numeric(format(round(simpletabsummaryclade$bcladesumwholeunder1.5base/ simpletabsummaryclade$bcladesumfracunder1.5base,2),nsmall=2) ) )#)
    simpletabsummaryall<-simpletabsummarygenus[which(simpletabsummarygenus$n.3 %in% "m3"),] %>%
      group_by(.) %>%
      summarise(countnums = sum(countnums, na.rm=TRUE),    
                ballsumwholeover1.5base= sum(bgensumwholeover1.5base,na.rm=TRUE),
                ballsumwholeunder1.5base= sum(bgensumwholeunder1.5base,na.rm=TRUE),
                ballsumfracover1.5base= sum(bgensumfracover1.5base,na.rm=TRUE),
                ballsumfracunder1.5base= sum(bgensumfracunder1.5base,na.rm=TRUE)
      )
    simpletabsummaryall<-as.data.frame(simpletabsummaryall)
    simpletabsummaryall$wholefracoverbase<-
      suppressWarnings(as.numeric(format(round(simpletabsummaryall$ballsumwholeover1.5base/simpletabsummaryall$ballsumfracover1.5base,2),nsmall=2) ) )#ifelse(
    simpletabsummaryall$wholefracunderbase<- 
      suppressWarnings(as.numeric(format(round(simpletabsummaryall$ballsumwholeunder1.5base/ simpletabsummaryall$ballsumfracunder1.5base,2),nsmall=2) ) )#)
    simpletabsummaryall<-as.data.frame(simpletabsummaryall)[,c(1,6,7)]
    simpletabsummaryclade<-as.data.frame(simpletabsummaryclade)[,c(1,2,7,8)]
    simpletabsummaryfam<-as.data.frame(simpletabsummaryfam[,c(2,3,8,9)])
    simpletabsummarygenus<-as.data.frame(simpletabsummarygenus[,c(3,5,10,11)])
    simpletasummarygenus<-simpleta %>%
      group_by_at(.vars=c("apgorder","family_apg","genus")) %>%
      summarise(n = n(),
                median2n=suppressWarnings(as.numeric(format(round(median(mode2n,na.rm=TRUE),1),nsmall=1))),
                m12n = suppressWarnings(as.numeric(paste(names(sort(table(mode2n, useNA="no"),decreasing=TRUE)[1]), collapse=", "))),
                m22n = suppressWarnings(as.numeric(paste(names(sort(table(mode2n, useNA="no"),decreasing=TRUE)[2]), collapse=", "))),
                m32n = suppressWarnings(as.numeric(paste(names(sort(table(mode2n, useNA="no"),decreasing=TRUE)[3]), collapse=", "))),
                gensumevenovermode= sum(evenmoremode,na.rm=TRUE),
                gensumevenundermode= sum(evenlessmode,na.rm=TRUE),
                gensumoddovermode= sum(oddmoremode,na.rm=TRUE),
                gensumoddundermode= sum(oddlessmode,na.rm=TRUE),
                median2c=suppressWarnings(as.numeric(format(round(median(mode2C,na.rm=TRUE),1),nsmall=1) ))
                ,
                m12c = tryCatch(paste(format(round(get.modes(mode2C[!is.na(mode2C)],bw.nrd0(mode2C[!is.na(mode2C)])/2,0.1)[1],2),nsmall=2)),
                                error = function(e) {NA ; NA} ) ,
                m22c = tryCatch(paste(format(round(get.modes(mode2C[!is.na(mode2C)],bw.nrd0(mode2C[!is.na(mode2C)])/2,0.1)[2],2),nsmall=2)),
                                error = function(e) {NA ; NA }  ) ,
                m32c = tryCatch(paste(format(round(get.modes(mode2C[!is.na(mode2C)],bw.nrd0(mode2C[!is.na(mode2C)])/2,0.1)[3],2),nsmall=2)),
                                error = function(e) {NA;NA} ) 
      )
    simpletasummarygenus<-as.data.frame(simpletasummarygenus)
    simpletasummarygenus$evenoddovermode<- 
      suppressWarnings(as.numeric(format(round(simpletasummarygenus$gensumevenovermode/simpletasummarygenus$gensumoddovermode,2),nsmall=2) ) ) #,NA)
    simpletasummarygenus$evenoddundermode<-   
      suppressWarnings(as.numeric(format(round(   simpletasummarygenus$gensumevenundermode/simpletasummarygenus$gensumoddundermode,2),nsmall=2) ) )#)
    simpletasummarygenus<-as.data.frame(simpletasummarygenus)[,c(1:8,13:18)]
    simpletasummarygenus<- merge(simpletasummarygenus, simpletabsummarygenus, all = TRUE ) # 2 rows
    # genfam <- fread("genusfam.csv", header = TRUE, stringsAsFactors = FALSE)
    # genfam <- as.data.frame(genfam)
    # colnames(genfam)[(names(genfam) == "family")] <- "family_apg"
    # colnames(genfam)[(names(genfam) == "big_clade")] <- "apgorder"
    famcheck<-genfam %>%
      group_by_at(.vars=c("apgorder","family_apg")) %>%
      summarise(species_number_checklist=sum(species_number_checklist, na.rm=TRUE))
    famcheck<-as.data.frame(famcheck)
    cladecheck<-genfam %>%
      group_by_at(.vars=c("apgorder")) %>%
      summarise(species_number_checklist=sum(species_number_checklist, na.rm=TRUE))
    cladecheck<-as.data.frame(cladecheck)
    allcheck<-genfam %>%
      group_by(.) %>%
      summarise(species_number_checklist=sum(species_number_checklist, na.rm=TRUE))
    allcheck<-as.data.frame(allcheck)
    merged<- merge(simpletasummarygenus, genfam, all = TRUE ) # 2 rows
    merged$n[is.na(merged$n)] <- 0
    merged$percent<-suppressWarnings(as.numeric(format(round((merged$n/merged$species_number_checklist*100),1),nsmall=1) ) )
    merged<-    arrange(merged,desc(species_number_checklist))
    mergedgenus<-merged
    mergedgenus$species_number_checklist[is.na(mergedgenus$species_number_checklist)] <- "not in checklist"
    mergedgenus<-as.data.frame(mergedgenus)[,c(1:4,18,19,5:16)] #WHOLEFRACunderbase not included
    mergedgenusall<-as.data.frame(mergedgenus)[,c(1:4,7:18)]
    mergedgenus[mergedgenus==Inf] <- "Inf"
    mergedgenusall[mergedgenusall==Inf] <- "Inf"
    simpletasummaryall<-simpleta %>%
      group_by(.) %>%
      summarise(n = n(),
                median2n=suppressWarnings(as.numeric(format(round(median(mode2n,na.rm=TRUE),1),nsmall=1))),
                m12n = suppressWarnings(as.numeric(paste(names(sort(table(mode2n, useNA="no"),decreasing=TRUE)[1]), collapse=", "))),
                m22n = suppressWarnings(as.numeric(paste(names(sort(table(mode2n, useNA="no"),decreasing=TRUE)[2]), collapse=", "))),
                m32n = suppressWarnings(as.numeric(paste(names(sort(table(mode2n, useNA="no"),decreasing=TRUE)[3]), collapse=", "))),
                allsumevenovermode= sum(evenmoremode,na.rm=TRUE),
                allsumevenundermode= sum(evenlessmode,na.rm=TRUE),
                allsumoddovermode= sum(oddmoremode,na.rm=TRUE),
                allsumoddundermode= sum(oddlessmode,na.rm=TRUE),
                median2c=suppressWarnings(as.numeric(format(round(median(mode2C,na.rm=TRUE),1),nsmall=1) ))
                ,
                m12c = tryCatch(paste(format(round(get.modes(mode2C[!is.na(mode2C)],bw.nrd0(mode2C[!is.na(mode2C)])/2,0.1)[1],2),nsmall=2)),
                                error = function(e) {NA ; NA} ) ,
                m22c = tryCatch(paste(format(round(get.modes(mode2C[!is.na(mode2C)],bw.nrd0(mode2C[!is.na(mode2C)])/2,0.1)[2],2),nsmall=2)),
                                error = function(e) {NA ; NA }  ) ,
                m32c = tryCatch(paste(format(round(get.modes(mode2C[!is.na(mode2C)],bw.nrd0(mode2C[!is.na(mode2C)])/2,0.1)[3],2),nsmall=2)),
                                error = function(e) {NA;NA} ) 
      )
    simpletasummaryall<-as.data.frame(simpletasummaryall)
    simpletasummaryall$evenoddovermode<-#ifelse((is.finite(
      suppressWarnings(as.numeric(format(round(simpletasummaryall$allsumevenovermode/simpletasummaryall$allsumoddovermode,2),nsmall=2)) )
    simpletasummaryall$evenoddundermode<-#ifelse((is.finite(
      suppressWarnings(as.numeric(format(round(simpletasummaryall$allsumevenundermode/simpletasummaryall$allsumoddundermode,2),nsmall=2)) )
    simpletasummaryall<-as.data.frame(simpletasummaryall)[,c(1:5,10:15)]
    simpletasummaryall<- merge(simpletasummaryall, simpletabsummaryall, all = TRUE ) # 2 rows
        mergedall<- merge(simpletasummaryall, allcheck, all = TRUE ) # 2 rows
    mergedall$percent<-suppressWarnings(as.numeric(format(round((mergedall$n/mergedall$species_number_checklist*100),1),nsmall=1) ) )
    mergedall<-as.data.frame(mergedall)[,c(1,15,16,2:13)] #(1:3,14,15,4:13)]
    mergedallall<-as.data.frame(mergedall)[,c(1,4:15)] #1:3,6:15)]
    mergedallall<-cbind(apgorder="Total",mergedallall)
    mergedall<-cbind(apgorder="Total",mergedall)
    simpletasummaryfam<-simpleta %>%
      group_by_at(.vars=c("apgorder","family_apg")) %>%
      summarise(n = n(),
                median2n=suppressWarnings(as.numeric(format(round(median(mode2n,na.rm=TRUE),1),nsmall=1) ) ),
                m12n = paste(names(sort(table(mode2n, useNA="no"),decreasing=TRUE)[1]), collapse=", "),
                m22n = paste(names(sort(table(mode2n,useNA="no"),decreasing=TRUE)[2]), collapse=", "),
                m32n = paste(names(sort(table(mode2n,useNA="no"),decreasing=TRUE)[3]), collapse=", "),
                famsumevenovermode= sum(evenmoremode,na.rm=TRUE),
                famsumevenundermode= sum(evenlessmode,na.rm=TRUE),
                famsumoddovermode= sum(oddmoremode,na.rm=TRUE),
                famsumoddundermode= sum(oddlessmode,na.rm=TRUE),
                median2c=suppressWarnings(as.numeric(format(round(median(mode2C, na.rm=TRUE),1),nsmall=1) ) ),
                m12c = tryCatch(paste(format(round(get.modes(mode2C[!is.na(mode2C)],bw.nrd0(mode2C[!is.na(mode2C)])/2,0.1)[1],2),nsmall=2)),
                                error = function(e) {NA ; NA} ) ,
                m22c = tryCatch(paste(format(round(get.modes(mode2C[!is.na(mode2C)],bw.nrd0(mode2C[!is.na(mode2C)])/2,0.1)[2],2),nsmall=2)),
                                error = function(e) {NA ; NA }  ) ,
                m32c = tryCatch(paste(format(round(get.modes(mode2C[!is.na(mode2C)],bw.nrd0(mode2C[!is.na(mode2C)])/2,0.1)[3],2),nsmall=2)),
                                error = function(e) {NA;NA} ) 
      )
    simpletasummaryfam<-as.data.frame(simpletasummaryfam)
    simpletasummaryfam$evenoddovermode<-#ifelse((is.finite(
      suppressWarnings(as.numeric(format(round(simpletasummaryfam$famsumevenovermode/simpletasummaryfam$famsumoddovermode,2),nsmall=2)) )
    simpletasummaryfam$evenoddundermode<-#ifelse((is.finite(
      suppressWarnings(as.numeric(format(round(simpletasummaryfam$famsumevenundermode/simpletasummaryfam$famsumoddundermode,2),nsmall=2)) )
    simpletasummaryfam<-as.data.frame(simpletasummaryfam)[,c(1:7,12:17)]
    simpletasummaryfam<- merge(simpletasummaryfam, simpletabsummaryfam, all = TRUE ) # 2 rows
    mergedfam<- merge(simpletasummaryfam, famcheck, all = TRUE ) # 2 rows
    mergedfam$n[is.na(mergedfam$n)] <- 0
    mergedfam$percent<-suppressWarnings(as.numeric(format(round((mergedfam$n/mergedfam$species_number_checklist*100),1),nsmall=1) ) )
    mergedfam<- arrange(mergedfam,desc(species_number_checklist))
    mergedfam$species_number_checklist[is.na(mergedfam$species_number_checklist)] <- "not in checklist"
    mergedfam<-as.data.frame(mergedfam)[,c(1:3,17,18,4:15)] #(1:3,14,15,4:13)]
    mergedfamall<-as.data.frame(mergedfam)[,c(1:3,6:17)] #1:3,6:15)]
    mergedfam[mergedfam==Inf] <- "Inf"
    mergedfamall[mergedfamall==Inf] <- "Inf"
    simpletasummaryclade<-simpleta %>%
      group_by_at(.vars=c("apgorder")) %>%
      summarise(n = n(),
                median2n=suppressWarnings(as.numeric(format(round(median(mode2n,na.rm=TRUE),1),nsmall=1) ) ),
                m12n = paste(names(sort(table(mode2n, useNA="no"),decreasing=TRUE)[1]), collapse=", "),
                m22n = paste(names(sort(table(mode2n,useNA="no"),decreasing=TRUE)[2]), collapse=", "),
                m32n = paste(names(sort(table(mode2n,useNA="no"),decreasing=TRUE)[3]), collapse=", "),
                cladesumevenovermode= sum(evenmoremode,na.rm=TRUE),
                cladesumevenundermode= sum(evenlessmode,na.rm=TRUE),
                cladesumoddovermode= sum(oddmoremode,na.rm=TRUE),
                cladesumoddundermode= sum(oddlessmode,na.rm=TRUE),
                median2c=suppressWarnings(as.numeric(format(round(median(mode2C, na.rm=TRUE),1),nsmall=1) ) ),
                m12c = tryCatch(paste(format(round(get.modes(mode2C[!is.na(mode2C)],bw.nrd0(mode2C[!is.na(mode2C)])/2,0.1)[1],2),nsmall=2)),
                                error = function(e) {NA ; NA} ) ,
                m22c = tryCatch(paste(format(round(get.modes(mode2C[!is.na(mode2C)],bw.nrd0(mode2C[!is.na(mode2C)])/2,0.1)[2],2),nsmall=2)),
                                error = function(e) {NA ; NA }  ) ,
                m32c = tryCatch(paste(format(round(get.modes(mode2C[!is.na(mode2C)],bw.nrd0(mode2C[!is.na(mode2C)])/2,0.1)[3],2),nsmall=2)),
                                error = function(e) {NA;NA} ) 
      )
    simpletasummaryclade<-as.data.frame(simpletasummaryclade)
    simpletasummaryclade$evenoddovermode<-#ifelse((is.finite(
      suppressWarnings(as.numeric(format(round(simpletasummaryclade$cladesumevenovermode/simpletasummaryclade$cladesumoddovermode,2),nsmall=2) ) )
    simpletasummaryclade$evenoddundermode<-#ifelse((is.finite(
      suppressWarnings(as.numeric(format(round(simpletasummaryclade$cladesumevenundermode/simpletasummaryclade$cladesumoddundermode,2),nsmall=2) ) )#)
    simpletasummaryclade<-as.data.frame(simpletasummaryclade)[,c(1:6,11:16)] #c(1:6,11:16)]
    simpletasummaryclade<- merge(simpletasummaryclade, simpletabsummaryclade, all = TRUE ) # 2 rows
    mergedclade<- merge(simpletasummaryclade, cladecheck, all = TRUE ) # 2 rows
    mergedclade$n[is.na(mergedclade$n)] <- 0
    mergedclade$percent<-suppressWarnings(as.numeric(format(round((mergedclade$n/mergedclade$species_number_checklist*100),1),nsmall=1)) )
    mergedclade<- arrange(mergedclade,desc(species_number_checklist))
    mergedclade$species_number_checklist[is.na(mergedclade$species_number_checklist)] <- "not in checklist"    
    mergedclade<-as.data.frame(mergedclade)[,c(1,2,16,17,3:14)] # [,c(1,2,13,14,3:12)]  ,c(1:3,16,17,4:15)] #(1:3,14,15,4:13)]
    mergedcladeall<-as.data.frame(mergedclade)[,c(1:2,5:16)]
    mergedclade[mergedclade==Inf] <- "Inf"
    mergedcladeall[mergedcladeall==Inf] <- "Inf"
    mergedcladeall<-rbind(mergedcladeall,mergedallall)
    mergedclade<-rbind(mergedclade,mergedall)
    if (input$statlevel=="major clades")
    {
      if(input$allowmodes==FALSE)
      {
        if(input$statsample=="only one data per species")
        {
          mergedclade<-as.data.frame(mergedclade)[,c(1:5,9,13:16)] #14
        }
        else{ # MORE THAN 1
          mergedcladeall<-as.data.frame(mergedcladeall)[,c(1:3,7,11:14)] #12
        }
      }
      else # WITH MODES
      {
        if(input$statsample=="only one data per species")
        {
          mergedclade
        }
        else{ # MORE THAN 1
          mergedcladeall #<-as.data.frame(mergedcladeall)[,c(1:3,7,11:12)]
        }
      } # else more than one
    } # end if major clades
    else if (input$statlevel=="family")
    {
      if(input$allowmodes==FALSE)
      {
        if(input$statsample=="only one data per species")
        {
          mergedfam<-as.data.frame(mergedfam)[,c(1:6,10,14:17)] #15
          mergedfam
        }
        else #    more than 1
        {
          mergedfamall<-as.data.frame(mergedfamall) [,c(1:4,8,12:15)]
        }
      }
      else # with modes
      {
        if(input$statsample=="only one data per species")
        {
          mergedfam
        }
        else{
          mergedfamall
        }
      } # allow modes true end
    } # end else if family
    else if (input$statlevel=="genus")
    {
      if(input$allowmodes==FALSE)
      {
        if(input$statsample=="only one data per species")
        {
          mergedgenus<-as.data.frame(mergedgenus)[,c(1:7,11,15:18)] #16
        }
        else # more than 1
        {
          mergedgenusall<-as.data.frame(mergedgenusall)[,c(1:5,9,13:16)] #14
        }
      }
      else # #    allow mmodes true
      {  
        if(input$statsample=="only one data per species")
        {
          mergedgenus
        }
        else
        {
          mergedgenusall
        }
      } # end allow modes true
    } # end else if genus 
  })
  stats <- reactiveValues(colnames = NULL,centered=NULL)
  observe ({ 
    validate(
      need(try(!invalid(input$statlevel)), "Wait 5097")
    ) 
    validate(
      need(try(!invalid(input$statsample)), "Wait 5100")
    ) 
    if (input$statlevel=="major clades" & input$allowmodes==TRUE)
    { 
      stats$pagel=15    
      if(input$statsample=="only one data per species") 
      {   
        stats$colnames=c("Major clade"="apgorder", "median 2n"="median2n","median 2C"="median2c", " Species sampled"="n",
                         "searched sample"="species_number_checklist","% cover"="percent",
                         "n (haploid) Even/Odd ratio > mode of n"="evenoddovermode","n (haploid) Even/Odd ratio < mode of n"="evenoddundermode",
                         "2n (>=  2x) Multiple/non-multiple of x*"="wholefracoverbase",#"Multiple/non-multiple of x < 1.5 x"="wholefracunderbase",
                         "2n mode1"="m12n","2n mode2"="m22n","2n mode3"="m32n",
                         "2C mode1"="m12c","2C mode2"="m22c","2C mode3"="m32c"
                         ,"Diversity of 2n observed"="countnums"
        )
        stats$centered= c(1,2)
        stats$right= c(3:15)
      } # 2nd if
      else { # MORE THAN 1
        stats$colnames=c("Major clade"="apgorder", "median 2n"="median2n","median 2C"="median2c", "Sampled"="n",
                         "n (haploid) Even/Odd ratio > mode of n"="evenoddovermode","n (haploid) Even/Odd ratio < mode of n"="evenoddundermode",
                         "2n (>=  2x) Multiple/non-multiple of x*"="wholefracoverbase",#"Multiple/non-multiple of x < 1.5 x"="wholefracunderbase",
                         "2n mode1"="m12n","2n mode2"="m22n","2n mode3"="m32n",
                         "2C mode1"="m12c","2C mode2"="m22c","2C mode3"="m32c"
                         ,"Diversity of 2n observed"="countnums"
        )
        stats$centered= c(1)
        stats$right= c(2:13)
      } 
    } # 1st if
    else if (input$statlevel=="family" & input$allowmodes==TRUE)
    {
      stats$pagel=10 
      if(input$statsample=="only one data per species") 
      { 
        stats$colnames=c("Major clade"="apgorder", "family"="family_apg","median 2n"="median2n",
                         "median 2C"="median2c", "Species sampled"="n",
                         "searched sample"="species_number_checklist","% cover"="percent",
                         "n (haploid) Even/Odd ratio > mode of n"="evenoddovermode","n (haploid) Even/Odd ratio < mode of n"="evenoddundermode",
                         "2n (>=  2x) Multiple/non-multiple of x*"="wholefracoverbase",#"Multiple/non-multiple of x < 1.5 x"="wholefracunderbase",
                         "2n mode1"="m12n","2n mode2"="m22n","2n mode3"="m32n",
                         "2C mode1"="m12c","2C mode2"="m22c","2C mode3"="m32c"
                         ,"Diversity of 2n observed"="countnums")
        stats$centered= c(2,3)
        stats$right= c(4:16)
      }
      else{ # MORE THAN 1
        stats$colnames=c("Major clade"="apgorder", "family"="family_apg","median 2n"="median2n","median 2C"="median2c", "Sampled"="n",
                         "n (haploid) Even/Odd ratio > mode of n"="evenoddovermode","n (haploid) Even/Odd ratio < mode of n"="evenoddundermode",
                         "2n (>=  2x) Multiple/non-multiple of x*"="wholefracoverbase",#"Multiple/non-multiple of x < 1.5 x"="wholefracunderbase",
                         "2n mode1"="m12n","2n mode2"="m22n","2n mode3"="m32n",
                         "2C mode1"="m12c","2C mode2"="m22c","2C mode3"="m32c"
                         ,"Diversity of 2n observed"="countnums")
        stats$centered= c(2)
        stats$right= c(3:14)
      }
    } # else if
    else if (input$statlevel=="genus"   & input$allowmodes==TRUE)
    {
      stats$pagel=10   
      if(input$statsample=="only one data per species") 
      { 
        stats$colnames=c("Major clade"="apgorder", "family"="family_apg","median 2n"="median2n",
                         "median 2C"="median2c", "Species sampled"="n",
                         "searched sample"="species_number_checklist","% cover"="percent",
                         "n (haploid) Even/Odd ratio > mode of n"="evenoddovermode","n (haploid) Even/Odd ratio < mode of n"="evenoddundermode",
                         "2n (>=  2x) Multiple/non-multiple of x*"="wholefracoverbase",#"Multiple/non-multiple of x < 1.5 x"="wholefracunderbase",
                         "2n mode1"="m12n","2n mode2"="m22n","2n mode3"="m32n",
                         "2C mode1"="m12c","2C mode2"="m22c","2C mode3"="m32c"
                         ,"Diversity of 2n observed"="countnums")
        stats$centered= c(3,4)
        stats$right= c(5:17)
      }
      else{ # MORE THAN 1
        stats$colnames=c("Major clade"="apgorder", "family"="family_apg","median 2n"="median2n",
                         "median 2C"="median2c", "Sampled"="n",
                         "n (haploid) Even/Odd ratio > mode of n"="evenoddovermode","n (haploid) Even/Odd ratio < mode of n"="evenoddundermode",
                         "2n (>=  2x) Multiple/non-multiple of x*"="wholefracoverbase",#"Multiple/non-multiple of x < 1.5 x"="wholefracunderbase",
                         "2n mode1"="m12n","2n mode2"="m22n","2n mode3"="m32n",
                         "2C mode1"="m12c","2C mode2"="m22c","2C mode3"="m32c"
                         ,"Diversity of 2n observed"="countnums")
        stats$centered= c(3)
        stats$right= c(4:15)
      }
    } # else if
    else if (input$statlevel=="major clades" & input$allowmodes==FALSE)
    { 
      stats$pagel=15
      if(input$statsample=="only one data per species") 
      {   
        stats$colnames=c("Major clade"="apgorder", "median 2n"="median2n","median 2C"="median2c", 
                         "Species sampled"="n",
                         "searched sample"="species_number_checklist","% cover"="percent",
                         "n (haploid) Even/Odd ratio > mode of n"="evenoddovermode","n (haploid) Even/Odd ratio < mode of n"="evenoddundermode",
                         "2n (>=  2x) Multiple/non-multiple of x*"="wholefracoverbase"#,#"Multiple/non-multiple of x < 1.5 x"="wholefracunderbase"
                         ,"Diversity of 2n observed"="countnums"
        )
        stats$centered= c(1,2)
        stats$right= c(3:9)
      } # 2nd if
      else { # MORE THAN 1
        stats$colnames=c("Major clade"="apgorder", "median 2n"="median2n","median 2C"="median2c", "Sampled"="n",
                         "n (haploid) Even/Odd ratio > mode of n"="evenoddovermode","n (haploid) Even/Odd ratio < mode of n"="evenoddundermode",
                         "2n (>=  2x) Multiple/non-multiple of x*"="wholefracoverbase"#,#"Multiple/non-multiple of x < 1.5 x"="wholefracunderbase"
                         ,"Diversity of 2n observed"="countnums"
        )
        stats$centered= c(1)
        stats$right= c(2:7)
      } 
    } # 1st if
    else if (input$statlevel=="family" & input$allowmodes==FALSE)
    {   
      stats$pagel=15
      if(input$statsample=="only one data per species") 
      {
        stats$colnames=c("Major clade"="apgorder", "family"="family_apg","median 2n"="median2n",
                         "median 2C"="median2c", "Species sampled"="n",
                         "searched sample"="species_number_checklist","% cover"="percent",
                         "n (haploid) Even/Odd ratio > mode of n"="evenoddovermode","n (haploid) Even/Odd ratio < mode of n"="evenoddundermode",
                         "2n (>=  2x) Multiple/non-multiple of x*"="wholefracoverbase"#,#"Multiple/non-multiple of x < 1.5 x"="wholefracunderbase"
                         ,"Diversity of 2n observed"="countnums"
        )
        stats$centered= c(2,3)
        stats$right= c(4:9)
      }
      else{ # MORE THAN 1
        stats$colnames=c("Major clade"="apgorder", "family"="family_apg","median 2n"="median2n","median 2C"="median2c", 
                         "Sampled"="n",
                         "n (haploid) Even/Odd ratio > mode of n"="evenoddovermode","n (haploid) Even/Odd ratio < mode of n"="evenoddundermode",
                         "2n (>=  2x) Multiple/non-multiple of x*"="wholefracoverbase"#,#"Multiple/non-multiple of x < 1.5 x"="wholefracunderbase"
                         ,"Diversity of 2n observed"="countnums"
        )
        stats$centered= c(2)
        stats$right= c(3:7)
      }
    } # else if
    else if (input$statlevel=="genus"   & input$allowmodes==FALSE)
    {   
      stats$pagel=15
      if(input$statsample=="only one data per species") 
      { 
        stats$colnames=c("Major clade"="apgorder", "family"="family_apg","median 2n"="median2n",
                         "median 2C"="median2c", "Species sampled"="n",
                         "searched sample"="species_number_checklist","% cover"="percent",
                         "n (haploid) Even/Odd ratio > mode of n"="evenoddovermode","n (haploid) Even/Odd ratio < mode of n"="evenoddundermode",
                         "2n (>=  2x) Multiple/non-multiple of x*"="wholefracoverbase"#,"Multiple/non-multiple of x < 1.5 x"="wholefracunderbase"
                         ,"Diversity of 2n observed"="countnums"
        )
        stats$centered= c(3,4)
        stats$right= c(5:11)
      }
      else{ # MORE THAN 1
        stats$colnames=c("Major clade"="apgorder", "family"="family_apg","median 2n"="median2n","median 2C"="median2c",
                         "Sampled"="n",
                         "n (haploid) Even/Odd ratio > mode of n"="evenoddovermode","n (haploid) Even/Odd ratio < mode of n"="evenoddundermode",
                         "2n (>=  2x) Multiple/non-multiple of x*"="wholefracoverbase"#,"Multiple/non-multiple of x < 1.5 x"="wholefracunderbase"
                         ,"Diversity of 2n observed"="countnums"
        )
        stats$centered= c(3)
        stats$right= c(4:9)
      }
    } # else if
  }) # END OBSERV
  output$tablestats = DT::renderDataTable({
    validate(
      need(try(stattable() != ""), "stat form and click button" ) #x2ndata2gen histogenplot")
    )
    DT::datatable(
      stattable()
      ,
      colnames=  stats$colnames,
      rownames= F,
      extensions  = c('ColReorder'),#"FixedColumns"),#,'Responsive'),#,"FixedColumns"), #, 
      options = list(
        pagingType = "full_numbers", 
        pageLength = stats$pagel,
        dom = 'Rlfrtip',
        columnDefs = list(
          list(
            className = 'dt-right', targets = stats$right
          ), #sublist
          list(
            className = 'dt-center', targets = stats$centered
          ) #, #sublist
        )#BIGLIST
      )# optionslist ,
      , escape = TRUE #FALSE# TRUE
    )   %>% formatRound(columns=c("n (haploid) Even/Odd ratio > mode of n", "n (haploid) Even/Odd ratio < mode of n", 
                                  "2n (>=  2x) Multiple/non-multiple of x*"#, "Multiple/non-multiple of x < 1.5 x"
    ), digits=2)
  } )#rendertable 
  output$dbhisto = renderUI({
    box(id ="dbhistobox",
        title = "Select data for histogram", 
        width = NULL, 
        solidHeader = TRUE, 
        collapsible = TRUE,
        status = "primary",
        radioButtons("radiohisto", "Select attribute", c("2n - Chromosome number","2C - Genome size"), selected = NULL, inline = FALSE,
                     width = NULL)
    ) # end box
  } ) # end output
  output$dbhisto2 = renderUI({
    validate(
      need(try(!is.null(input$radiohisto) ) , "wait please"  ) #x2ndata2gen histogenplot")
    )
    if(input$radiohisto!="2C - Genome size") { # if 2n
      box(id ="dbhistobox2", width=NULL,  solidHeader = FALSE,
          radioButtons("par2ntypequery","",c("dotplot","histogram"),"dotplot")
          ,uiOutput("x2ntabboxquery")
      )
      }
    else { # if 2c
      box(id ="dbhistobox2",
          width = NULL, 
          solidHeader = FALSE
          ,radioButtons("par2ctypequery","",c("violinplot","histogram or density plot"="histogram"),"violinplot")
          ,uiOutput("x2ctabboxquery")
          )   
    }
  } ) # end output
  output$x2ntabboxquery = renderUI({
    if(input$par2ntypequery=="histogram")
    {
      wellPanel(width=12, 
                checkboxInput("par2ngridquery","Show as grid",value=TRUE),
                radioButtons("x2c2nlegendtype","Type of legend",c("median and counts per taxa","3 modes and counts")
                             )
      )
    }
    else
    wellPanel(width = 12,
              sliderInput("size2ndotplotquery","Modify width of graph",20,40,28,step=2, ticks = FALSE)     
    )
  })  
  output$x2ctabboxquery = renderUI({
    if(input$par2ctypequery=="histogram")
    {
        wellPanel(width=12, 
  radioButtons("histoordensityquery","Choose type of 2C graph",c("Histogram","Density")),
  checkboxInput("par2cgridquery","Show as grid",value=TRUE),
  radioButtons("x2c2nlegendtype","Type of legend",c("median and counts per taxa","3 modes and counts")
               ),
  uiOutput("x2ctabboxquery2")
        )
    }
    else{#violinplot
      wellPanel(width = 12,
                sliderInput("x2cadjustquery",
                            "bandwidth adjust:",
                            0.1,
                            1,
                            0.5, round=F, ticks=F, step=0.1)
      ) #well
    }
  })
  output$x2ctabboxquery2 = renderUI({
    validate(
      need(try(!is.null(input$histoordensityquery) ) , "wait please"  ) #x2ndata2gen histogenplot")
    )
    if(input$histoordensityquery!="Histogram") {
      sliderInput("x2cadjustdensityquery",
                  "bandwidth adjust:",
                  0.1,
                  1,
                  0.5, round=F, ticks=F, step=0.1)
    }
  })
  output$resultsdbtablebox = renderUI({
    validate(
      need(nrow(tblres$tblquery) > 0, "Please make a search, click button \"Generate table of query\" " )
    )  
    box(style = "padding:0px 0px 10px 10px", id ="resultsbox",
        title = "Table of results", width = NULL, 
        solidHeader = TRUE, 
        collapsible = FALSE,
        status="primary", 
        div(DT::dataTableOutput('tableresults'), style = "font-size:80%")
        ,
        uiOutput('downloadbutton')
    ) # end box
  }) # end output
  
  
  output$video <- renderUI({
    HTML(paste0(
      '<div style="position:relative;padding-top:60.6%;">
      <iframe src="https://www.youtube.com/embed/IZZybDBtLcI" frameborder="0" allowfullscreen
      style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe>
      </div>'
      ) # paste 
      )# H
  })
    
  output$videobox = renderUI({
    box(style = "padding:0px 0px 10px 10px", id ="videobox",
        title = "", width = NULL, 
        solidHeader = F, 
        collapsible = FALSE,
        status="primary", 
        uiOutput('video')
    ) # end box
  }) # end output  
  
  output$resultsauthortablebox = renderUI({
    validate(
      need(nrow(tblres$tblauthors) > 0, "Please make a click in previous tab" )
    )  
    box(style = "padding:0px 0px 10px 10px", id ="resultsauthorsbox",
        title = "Table for selected author", width = NULL, 
        solidHeader = TRUE, 
        collapsible = FALSE,
        status="primary", 
        div(DT::dataTableOutput('tableauthors'), style = "font-size:80%")
        ,
        uiOutput('downloadbuttonauthor')
    ) # end box
  }) # end output
  output$downloadbutton <- renderUI({
    if(invalid(tblres$tblquery)){return()}
    else
      downloadButton('downloadCsv4', 'Download table as .csv')
  })
  output$downloadbuttonauthor <- renderUI({
    if(invalid(tblres$authors)){return()}
    else
      downloadButton('downloadCsvauthor', 'Download table as .csv')
  })
  output$downloadbutton7 <- renderUI({
    if(invalid(stattable())){return()}
    else
      downloadButton('downloadCsv7', 'Download table as .csv')
  })
  output$tableresults = DT::renderDataTable({
    DT::datatable(
      tblres$tblquery
      ,
      colnames= tblres$renamed,
      rownames=F,
      extensions = c('Responsive', 'ColReorder'),
      options = list(
        dom = 'Rlfrtip',
        scrollX = TRUE,
        columnDefs = list(list(
          className = 'dt-center', targets = tblres$centered
        )
        , #sublist
        list(targets = tblres$linked,
             render = JS(
               "       function(data, type, row, meta) {",
               "if(data == null ||  data == \"NA\"||  data == '' ){return '';}else{return '<a target=\"top\" href=\"http://' +data+ ' \">Download</a>';}",
               "       } "
             ) # end render
        ) #sublist #ok
        )#BIGLIST
      )# optionslist ,
      , escape = TRUE
    )#        
  }) #rendertable 
  output$tableauthors = DT::renderDataTable({
    DT::datatable(
      tblres$tblauthors
      ,
      colnames= tblres$renamedcolsauthor,
      rownames=F,
      extensions = c('Responsive', 'ColReorder'),
      options = list(
        dom = 'Rlfrtip',
        scrollX = TRUE,
        columnDefs = list(list(
          className = 'dt-center', targets = tblres$centeredauthor
        )
        , #sublist
        list(targets = tblres$linkedcolauthor,
             render = JS(
               "       function(data, type, row, meta) {",
               "if(data == null ||  data == \"NA\"||  data == '' ){return '';}else{return '<a target=\"top\" href=\"http://' +data+ ' \">Download</a>';}",
               "       } "
             ) # end render
        ) #sublist #ok
        )#BIGLIST
      )# optionslist ,
      , escape = TRUE
    )#        
  }) #rendertable 
  tblres <- reactiveValues(tblquery = NULL,centered=NULL,linked=NULL, tblquerygenus=NULL)
  observeEvent(input$tablequerybutton , {
    matchtable2 <- reactive({
      query <- unlist(strsplit(input$query, "\n" ) ) # separate different lines
      query <- unlist(strsplit(query, "," ) ) # separate by comma
      query2<-eliminatespaces(query)
      cn.table <- ncol(techmatch)
      matchtableempty <- data.frame(matrix(vector(), 0, cn.table+2,
                                      dimnames=list(c(), c(names(techmatch),"your_query","type_of_match" ) ) ),
                               stringsAsFactors=F)
      matchtableall<-matchtableempty
      for (taxon in query2) { # open for
        matchtable<-matchtableempty
        taxonuser<-taxon
        taxon<-firstlettercaps(taxon)
        taxon <- gsub("\\s[a|c]f+\\.", "", taxon) # eliminate pattern " af." " cf." in taxon whole string
        taxon <- gsub("(\\w+)(\\s)(\\w+).*$", "\\1\\2\\3", taxon) # get only first 2 words
        taxon <- gsub("(\\w+)(\\s)(sp\\.|sp)", "\\1", taxon) # eliminate pattern "sp." in taxon whole string
          if (str_count(taxon, "\\S+")>1 ) { # if more than one word search species names
          querymatches <- techmatch[with(techmatch, {
            which(taxon == speciessimple ) #& taxon.status == "accepted")
          }), ]
          querymatches$your_query<-taxonuser
          querymatches$type_of_match<-"with species accepted name"
          l<-list(matchtable,querymatches)#,querymatchesapproxsp,querymatchescytoname,synomatchesb)
          matchtable<-  rbindlist(l)
          if (nrow(querymatches) == 0) { # if no match with species
            dist<- adist(taxon, techmatch$speciessimple)
            if (min(dist)<4){
              querymatchesapproxsp <- techmatch[with(techmatch, {
                which(speciessimple[dist==min(dist)] == speciessimple ) #& taxon.status == "accepted")
              }), ]
              querymatchesapproxsp$your_query<-taxonuser 
              querymatchesapproxsp$type_of_match<-"with species approx. match"
              l<-list(matchtable,querymatchesapproxsp)#,querymatchesapproxsp,querymatchescytoname,synomatchesb)
              matchtable<-  rbindlist(l)
            } # end if approx match
         }
              querymatchescytoname <- techmatch[with(techmatch, {
                which(taxon == speciessimpleorig ) #& taxon.status == "accepted")
              }), ]
              querymatchescytoname$your_query<-taxonuser
              querymatchescytoname$type_of_match<-"with species name in cytogenetic literature"
              l<-list(matchtable,querymatchescytoname)#,querymatchesapproxsp,querymatchescytoname,synomatchesb)
              matchtable<-  rbindlist(l)
              if (nrow(querymatchescytoname) == 0) { # if no match with species orig name
                dist<- adist(taxon, techmatch$speciessimpleorig)
                if (min(dist)<4){
                  querymatchescytoname <- techmatch[with(techmatch, {
                    which(speciessimpleorig[dist==min(dist)] == speciessimpleorig ) #& taxon.status == "accepted")
                  }), ]
                  querymatchescytoname$your_query<-taxonuser
                  querymatchescytoname$type_of_match<-"with species name in cyto-public approx. match"
                  l<-list(matchtable,querymatchescytoname)#,querymatchesapproxsp,querymatchescytoname,synomatchesb)
                  matchtable<-  rbindlist(l)
                } # end if approx match cyto
              } # end no match with sp orig name
              synomatches <- datac[with(datac, {
                which(taxon == namesimple ) #& taxon.status == "accepted")
              }), ]
              if (nrow(synomatches) > 0) { #                
                where <- sapply(synomatches$parnamesimple, function (x) which(x == techmatch$speciessimple))
                selected <- unlist(where)
                selected <- unique( selected )
                synomatchesb <- techmatch[ selected, ]
                synomatchesb$your_query<-taxonuser
                synomatchesb$type_of_match<-"with species probable synonym"
                l<-list(matchtable,synomatchesb)#,querymatchesapproxsp,querymatchescytoname,synomatchesb)
                matchtable<-  rbindlist(l)
              } # end syno match
               else if (nrow(synomatches) == 0){ # no match with syno
                dist<- adist(taxon, datac$namesimple)
                if (min(dist)<4){ 
                  synomatches <- datac[with(datac, {
                    which(namesimple[dist==min(dist)] == namesimple ) #& taxon.status == "accepted")
                  }), ]
                  where <- sapply(synomatches$parnamesimple, function (x) which(x == techmatch$speciessimple))
                  selected <- unlist(where)
                  selected <- unique( selected )
                  synomatchesb <- techmatch[selected,]
                  synomatchesb$your_query<-taxonuser 
                  synomatchesb$type_of_match<-"approx match with probable synonym"
                  l<-list(matchtable,synomatchesb)#,querymatchesapproxsp,querymatchescytoname,synomatchesb)
                  matchtable<-  rbindlist(l)
                } # end if approx match syno
               } # end no match with syno
        } # end more than one word
        else { # if only one word look for genera or family 
          querymatches <- techmatch[with(techmatch, {
            which(taxon == genus ) #& taxon.status == "accepted")
          }), ]
          querymatches$your_query<-taxonuser
          querymatches$type_of_match<-"with genus"
          l<-list(matchtable,querymatches)#,querymatchesapproxsp,querymatchescytoname,synomatchesb)
          matchtable<-  rbindlist(l)
          if (nrow(querymatches) == 0) { # if no match with genus
            dist<- adist(taxon, techmatch$genus)
            if (min(dist)<3){ 
              querymatchesapproxgenus <- techmatch[with(techmatch, {
                which(genus[dist==min(dist)] == genus ) #& taxon.status == "accepted")
              }), ]
              querymatchesapproxgenus$your_query<-taxonuser
              querymatchesapproxgenus$type_of_match<-"with genus approx. match"
              l<-list(matchtable,querymatchesapproxgenus)#,querymatchesapproxsp,querymatchescytoname,synomatchesb)
              matchtable<-  rbindlist(l)
            }
          } #end approx match genus
              querymatchesfam <- techmatch[with(techmatch, {
                which(taxon == family_apg) #& taxon.status == "accepted")
              }), ]
              querymatchesfam$your_query<-taxonuser
              querymatchesfam$type_of_match<-"with family"
              l<-list(matchtable,querymatchesfam)#,querymatchesapproxsp,querymatchescytoname,synomatchesb)
              matchtable<-  rbindlist(l) 
            if (nrow(querymatchesfam) == 0) { # if no match with family 
              dist<- adist(taxon, techmatch$family_apg)
              if (min(dist)<3){ 
                querymatchesfam <- techmatch[with(techmatch, {
                  which(family_apg[dist==min(dist)] == family_apg ) #& taxon.status == "accepted") # error object length is not multiple of shorter object
                }), ]
                querymatchesfam$your_query<-taxonuser 
                querymatchesfam$type_of_match<-"with family approx. match"
                l<-list(matchtable,querymatchesfam)#,querymatchesapproxsp,querymatchescytoname,synomatchesb)
                matchtable<-  rbindlist(l) 
              }
            } # end approx match fam
        } # end else one word
        if (nrow(matchtable) == 0)  { # no match with anything
          newrow = c(rep("N.D.", 2),rep("", cn.table-2),taxon,"no match")
          l<-list(matchtable,as.list(newrow) )
          matchtable<-  rbindlist(l)
        } # end no match anything
        l<-list(matchtableall,matchtable)#,querymatchesapproxsp,querymatchescytoname,synomatchesb)
        matchtableall<-  rbindlist(l)
      } #end for
      matchtableall<-as.data.table(as.data.frame(matchtableall)[!duplicated(as.data.frame(matchtableall)[,c(1:(cn.table+1))]),]) # with=FALSE]
      matchtableall <- droplevels(matchtableall)
      matchtableall
    }) # END REACTIVE MATCHTABLE
    tblres$tblquery  <-matchtable2()
    tblres$tblquerygenus <- tblres$tblquery  
    if (!invalid(input$query) ){ 
      if (input$query !="")
      { if (nrow(tblres$tblquery)>0) 
      # tblres$centered<- c(11:12)
      tblres$centered<- c(5:14)# mod
      tblres$linked<-c(15)
      tblres$renamed<-c("Clade"="apgorder", "parsed 2n"="parsed2n","parsed 2C"="parsed2c","FISH (molec. cyto)"="FISH_.molecular_cyt..")
      tblres$tblquery<- tblres$tblquery[, c(2:5,21,20,14,10,12,16:19,22,23,24,25,29,30), with=FALSE] 
      }
    }
  })  
    output$rownumui = renderUI({
    if (!invalid( values[["newrownum"]] ) ){ 
      span(paste(c(substract(),"row(s) sent" ), collapse = " " ) )
    }
  }) 
    output$errorhandler = renderUI({
      if (!invalid( values[["texterrorquery"]] ) ){ 
        span(values[["texterrorquery"]])
      }
    })     
    output$errorgeneratetable = renderUI({
      if (!invalid( values[["text"]] ) ){ 
      span(values[["text"]])
        }
    }) 
    output$errorsave = renderUI({
      if (!invalid( values[["text20"]] ) ){ 
        span(values[["text20"]])
      }
    }) 
  substract<- eventReactive (input$save, { (as.integer( values[["newrownum"]] )-as.integer( values[["oldrownum"]] ) )
  }) # end substract
  DF1 <- reactive({
    as.data.frame(matrix("", ncol = input$numcols, nrow = input$numrows), stringsAsFactors = FALSE)
  })
  
  values <- reactiveValues()
  
  observeEvent(input$generatetablebutton, {
    databaseName <- "cyto"
    collectionName <- "contributionscyto"
    db <- tryCatch(mongo(collection = collectionName,
                         url = sprintf(
                           "mongodb://%s:%s@%s/%s",
                           options()$mongodb$username,
                           options()$mongodb$password,
                           options()$mongodb$host,
                           databaseName) ),
                   error = function(e) {print(paste("no internet")) ; "no internet" }
    ) # end db
    if (typeof(db)=="character")
    {
      values[["text"]] <- "Connection to mongo database was not possible"      
    }
    else {
    values[["df1"]] <- DF1()
    values[["text"]] <- ""    
        }
  })   
  observe({   
    if (!is.null(input$out)) {  
      tryCatch(df <- hot_to_r(input$out), error = function(e) {NA; NA } )
      if (is.function(df))
      { 
        values[["df1"]] <- DF1()
      }
      else { 
        values[["df1"]] <- df
      }
    }
  })
  df <- reactive({
    df <- values[["df1"]]
    df
  })
  output$out <- renderRHandsontable({
    if(invalid(df())){return()}
    else { 
      hot <- rhandsontable(df())
      hot
    }
  })
  oldrownum = eventReactive(input$generatetablebutton, {
    databaseName <- "cyto"
    collectionName <- "contributionscyto"
    db <- tryCatch(mongo(collection = collectionName,
                url = sprintf(
                  "mongodb://%s:%s@%s/%s",
                  options()$mongodb$username,
                  options()$mongodb$password,
                  options()$mongodb$host,
                  databaseName) ),
                error = function(e) {print(paste("no internet")) ; "no internet" }
                )
    data6 <- tryCatch(db$find(),  error = function(e) {print(paste("not enough data")) ; "not enough data" } )
    nrow2<-tryCatch(nrow(data6),error = function(e) {print(paste("not enough data")) ; 0 } )
    })
  observeEvent(input$generatetablebutton,{
   values[["oldrownum"]] <- oldrownum()
  })
  fieldscontrib <- c("time","fingerprint","ip") #, "r_num_years")
  formDatacontrib <- reactive({
    assign(fieldscontrib[1],Sys.time() )
    assign(fieldscontrib[2],input$fingerprint)
    assign(fieldscontrib[3],input$ipid )
    data<-as.data.frame(mget(fieldscontrib))
    data
  })
  observeEvent(input$save, {
    if(!invalid(df()) ) { 
      df2<-isolate(df())
      df2 <- df2[!apply(is.na(df2) | df2 == "", 1, all),]
      df2 <- df2 %>% na.omit()
      if(nrow(df2)!=0)  { 
      mergeddf<-cbind(df2,formDatacontrib())  
      mergeddf<-unique(mergeddf)
      save<-tryCatch(saveData(mergeddf,"contributionscyto","cyto"),error = function(e) {print(paste("no internet")) ; "no internet" } )
        databaseName <- "cyto"
        collectionName <- "contributionscyto"
        db2 <- tryCatch(mongo(collection = collectionName,
                     url = sprintf(
                       "mongodb://%s:%s@%s/%s",
                       options()$mongodb$username,
                       options()$mongodb$password,
                       options()$mongodb$host,
                       databaseName)),
        error = function(e) {print(paste("no internet")) ; "no internet" } )
        data6 <- tryCatch(db2$find(), error = function(e) {print(paste("no internet")) ; "no internet" } )
        
newrownum = reactive( {
        nrow<-tryCatch(nrow(data6),error = function(e) {print(paste("no internet")) ; 0 } )
}) # end newrownum()
      if (typeof(save)=="character")
      {
        values[["text20"]] <- "Connection to mongo database was not possible"      
      }
      else {
        values[["newrownum"]] <- newrownum()
        values[["text20"]] <- ""   
                }
       } # end if df2 rows dif 0
      else{
        values[["text20"]] <- "No data detected in table"      
        }
      } # end invalid df()
    else{}
  }) # end observeEvent
}
