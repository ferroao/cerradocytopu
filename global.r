# what is next
options(shiny.maxRequestSize=30*1024^2) 
# mandatory files
# datac.csv"
# integrate.csv
defaultEncoding <- "UTF8"
library(bib2df)
library(shiny)
library(knitr)
library(scales) #necessary for prettybreaks
library(data.table) #fread
library(ggplot2) #ggplot
library(robustHD) #winsorize
# install.packages("robustHD") #winsorize
library(shinyjs) #useshinyjs, hidden
# install.packages("shinyjs") #winsorize
library(plyr) #ddply
library(tidyr)#spread
library(dplyr) #group_by_at
library(gtools) # invalid
library(shinydashboard)
library(stringr) # for str_sub
library(DT) # dt
library(mongolite)
# install.packages("mongolite")
library(rhandsontable) 
# install.packages("rhandsontable")
library(readxl)
# install.packages("network")
library(network)
# install.packages("sna")
library(sna)
# library(ggnet)
# install.packages("GGally")
library(GGally) # 25/1/18
# install.packages("RefManageR")
library(RefManageR)
library(lazyeval)
library(grid)
library("gridExtra")
fields <- c("query", "time","fingerprint","ip") #, "r_num_years")
source("mongoptions.R")
plot_fun = function(df, par, groupx,vary,labelx,lim) {
  ggplot(df, aes_string(x=groupx, y=vary)) +
    geom_dotplot(binaxis='y', stackdir='center', binwidth = 1, cex=1.5,
                 stackratio=1, dotsize=par/df$n[1])+ coord_flip()+ #facet_grid(dummy1 ~.,scales = "free") +
    scale_y_continuous(limits = c(0,lim), breaks=pretty_breaks(10), expand=c(0,0) ) +
    coord_flip()  +  ylab(labelx) +
    theme(strip.text.y = element_blank(),
          axis.ticks.x = element_blank(),
          axis.title.y=element_blank(),
          plot.margin=unit(c(-0.05,1,-0.95,1), "cm"),
          panel.grid.major = element_line(size = 1.5, colour = "white"),
          axis.text.y=element_text(family="CourierNew",size=rel(1.2) ) ,
          axis.text.x=element_text(size=rel(1) )
    )
}
myplot <- function(x,colnumb,cmsize) {
  plot2 <- gridExtra::arrangeGrob(grobs=x, ncol=colnumb, heights= c(rep(1/(length(x)), length(x) ) ),
                                  vp= viewport(width=unit(cmsize, "cm")) )
  class(plot2) <- c("myplot", class(plot2) )
  plot2
}
print.myplot <- function(x, ...) {
  gridExtra::grid.arrange(x, bottom=grid::textGrob("", gp=grid::gpar(cex=3)) )
}
gridplot<-function (dataframe, colgroup, maxcountcol, widthdots,labelx1,colnumb,lim1,cmsize) {
  dataframe<-stringcount(dataframe,colgroup) # homogenize length in characters of grouping variable
  max1<-max(dataframe[,maxcountcol])
  maxfreq<-groupingfun(dataframe,colgroup,maxcountcol,"Count")# get the maximum number from the previous data
  dataframe<-merge(dataframe, maxfreq, all=T) # incorporates that number in the dataframe to be readed by the ggplot
  listofdf <- split(dataframe, dataframe[colgroup]) # splits the dataframe
  plot = mapply(plot_fun,
                d = listofdf, par= widthdots, groupx=colgroup, vary=maxcountcol, 
                labelx=labelx1, lim=lim1, SIMPLIFY=FALSE) # make plots
  myplot(plot,colnumb,cmsize)
}
stringcount<-function(dataframe,colgroup){
  dataframe[,colgroup]<- sprintf(paste0("%-",max(nchar(as.character(dataframe[ ,colgroup]))),"s"),
                                 dataframe[,colgroup])
  return(dataframe)
}
groupingfun <- function(df, colgroup1, varcount,colnam) {
  df %>%
    group_by_at(vars(colgroup1,varcount)) %>%
    summarise(Count = n()) %>%
    group_by_at(colgroup1) %>%
    summarise(n=max(Count) )    
}
move<-function(new.pos,nameofcolumn,dfname) {
  col_idx <<- grep(nameofcolumn, names(dfname))
  if (new.pos==1) {
    b<-dfname[ , c( col_idx, c((new.pos):ncol(dfname))[-(abs(new.pos-1-col_idx))] )]  }
  else if(col_idx==1 & new.pos==ncol(dfname)){
    b<-dfname[ , c((1:(new.pos-1)+1), col_idx )] }
  else if(col_idx==1){
    b<-dfname[ , c((1:(new.pos-1)+1), col_idx, c((new.pos+1):ncol(dfname)) )] }
  else if(new.pos==ncol(dfname)){
    b<-dfname[ , c((1:(new.pos))[-col_idx], col_idx)] }
  else if(new.pos>col_idx){
    b<-dfname[ , c((1:(new.pos))[-col_idx], col_idx, c((new.pos+1):ncol(dfname)) )] } 
  else{
    b<-dfname[ , c((1:(new.pos-1)), col_idx, c((new.pos):ncol(dfname))[-(abs(new.pos-1-col_idx))] )]}
  return(b)
  if(length(ncol(b))!=length(ncol(dfname))){print("error")}
}
saveData <- function(data,collectionName,databaseName) {
  db <- tryCatch(mongo(collection = collectionName,
              url = sprintf(
                "mongodb://%s:%s@%s/%s",
                options()$mongodb$username,
                options()$mongodb$password,
                options()$mongodb$host,
                databaseName)), error = function(e) {print(paste("no internet")) ; "no internet" } )
  data <- as.data.frame(data)
  db$insert(data)
}
intervals<-unlist(lapply(1:200, function(x) rep(x,3)))
eliminatespaces<-function(yourquery) 
{
  yourquery <- gsub("\\t|\\s+", " ", yourquery) #substituir tabs by spaces #substitute more than 1 space with 1 space
  yourquery <- gsub("\\s+$|^\\s+", "", yourquery) # substitute 1 or more spaces at end or beginning with nothing
  return(yourquery)
}
firstlettercaps<-function (x) 
{
  s <- paste(tolower(strsplit(x, " ")[[1]]), collapse = " ") # put everything small caps
  paste(toupper(substring(s, 1, 1)), substring(s, 2), sep = "") # first in caps
}
get.modes <- function(x,bw,spar) {  
  if (length(x)<2){NA} # necessario
  else {
    den <- density(x, kernel=c("gaussian"),bw=na.omit(bw),na.rm=TRUE)
    den.s <- smooth.spline(den$x, den$y, all.knots=TRUE, spar=spar)
    s.1 <- predict(den.s, den.s$x, deriv=1)
    s.0 <- predict(den.s, den.s$x, deriv=0)
    den.sign <- sign(s.1$y)
    a<-c(1,1+which(diff(den.sign)!=0))
    b<-rle(den.sign)$values
    df<-data.frame(a,b)
    df = df[which(df$b %in% -1),]
    modes<-s.1$x[df$a]
    density<-s.0$y[df$a]
    df2<-data.frame(modes,density)
    df2<-df2[with(df2, order(-density)), ]
    df2$modes
  }
} 
make.title.legend <- function(data) {
  list<-list()
  x<-1
  nchar1<-max(nchar(as.character(data[,x])) )
  nchar2<-nchar(colnames(data)[x])
  maxdif<- if (!invalid(max(c(nchar2,nchar1))-min(c(nchar2,nchar1))) ) {max(c(nchar2,nchar1))-min(c(nchar2,nchar1))} else {0}
  first <-  paste0(colnames(data)[x], sep=paste(replicate(maxdif, " "), collapse = "")) 
  list[[first]] <-first
  for (i in 1:(ncol(data)-1)) {
    x<-i+1
    nchar1<-max(nchar(as.character(data[,x])) )
    nchar2<-nchar(colnames(data)[x])
    maxdif<-     if (!invalid(nchar2) & !invalid(nchar1)) {   if(nchar2>nchar1){0} else {nchar1-nchar2} } else {0}
    first <-  paste0(colnames(data)[x], sep=paste(replicate(maxdif, " "), collapse = "")) 
    list[[first]] <-first
    title<-str_c(list, collapse = " ")
  }
  return(title)
}
make.legend.withstats <- function(data,namecol) {
  data[is.na(data)] <- ""
  data[,ncol(data)]<-paste0("(",data[,ncol(data)],")")
  nchar1<-nchar(as.character(data[,1])) 
  nchar2<-nchar(colnames(data)[1])
  maxlen<-max(c(nchar1,nchar2))
  data[,1]<-sprintf(paste0("%-",maxlen,"s"), data[,1])    
  data[,ncol(data)+1]<-paste(data[,1],data[,2],sep=" ")
  ncharmin2<-min(nchar(data[,2]))
  y<- ncharmin2-1
  nchara1<-nchar(data[,ncol(data)] ) # 7
  init1<-min(nchara1)
  y2<-init1-1
  minchar<-min(nchar(data[,2]))
  maxchar<-max(c(nchar(colnames(data)[2]),(nchar(data[,2]))))
  dif<- if (!invalid(maxchar) & !invalid(minchar)) {   maxchar-minchar} else {0}
  if (dif>0){ 
    for (i3 in minchar:(maxchar-1)) { 
      y2<-y2+1
      y<-y+1
      str_sub(data[nchar(data[,ncol(data)]) == y2, ][,ncol(data)], y2-y, y2-y)<- "  "
    } 
  }
  nd<-ncol(data)-2
  if(ncol(data)>3){ 
    for (i in 2:nd) {  
      x3<-i
      data[,ncol(data)+1]<-paste(data[,ncol(data)],data[,x3+1],sep=" ")  
      minchar<-min(nchar(data[,x3+1]))
      maxchar<-max(c(nchar(colnames(data)[x3+1]),(nchar(data[,x3+1]))))
      ncharmin2<-min(nchar(data[,x3+1]))
      y<- ncharmin2-1
      nchara1<-nchar(data[,ncol(data)] ) 
      init1<-min(nchara1)
      y2<-init1-1
      dif<- if (!invalid(maxchar) & !invalid(minchar)) {   maxchar-minchar} else {0}
      if (dif>0){ 
        for (i2 in minchar:(maxchar-1)) { 
          y2<-y2+1
          y<-y+1
          str_sub(data[nchar(data[,ncol(data)]) == y2, ][,ncol(data)], y2-y, y2-y)<- "  "
        }
      }
    }
  }
  data<-  as.data.frame(data[,c(1,ncol(data))])
  names(data)[2]<-paste(namecol)
  data[,1]<-gsub("\\s+$", "", data[,1]) 
  data
}
length2 <- function (x, na.rm=FALSE) {
  if (na.rm) sum(!is.na(x))
  else       length(x)
}
get.3modes.andcounts<- function(origtable,groupby,columnname,adjust1) {
  data <- ddply (origtable, groupby, .fun = function(xx){
    c(m1 = tryCatch(paste(format(round(get.modes(xx[,columnname],bw.nrd0(xx[,columnname])*adjust1,0.1)[1],2),nsmall=2)),
                    error = function(e) {print(paste("NA")) ; "NA" } )  ,
      m2 = tryCatch(paste(format(round(get.modes(xx[,columnname],bw.nrd0(xx[,columnname])*adjust1,0.1)[2],2),nsmall=2)),
                    error = function(e) {print(paste("NA")) ; "NA" } )  ,
      m3 = tryCatch(paste(format(round(get.modes(xx[,columnname],bw.nrd0(xx[,columnname])*adjust1,0.1)[3],2),nsmall=2)),
                    error = function(e) {print(paste("NA")) ; "NA" } )  ,
      counts=length2(xx[[columnname]], na.rm=TRUE) #http://www.cookbook-r.com/Manipulating_data/Summarizing_data/
    ) } ) 
  return(data)
}
get.3modes.andcounts2n<- function( data, col1, col2 )
{
  n = 3
  stopifnot( n > 0 )
  a1 <- lapply( split( data, data[[col1]] ), function( x ) { # split data by col1
    val  <- factor( x[[col2]] )
    z1   <- tabulate( val )
    z2   <- sort( z1[ z1 > 0 ], decreasing = TRUE ) # sorted frequency table with >0
    lenx <- length( unique( z2 ) )
    if ( lenx == 1 ) {
      return( c( paste( ( levels(val)[ which( z1 %in% z2 ) ] ), collapse = ','), rep(NA_character_, n - 1 ), sum( z1 ) ) )
    } else if ( lenx > 1 ) { # lenx > 1
      z2 <- setdiff( z2, min( z2 ) )
      z2 <- sapply( z2, function( y ) paste( levels(val)[ which( z1 %in% y ) ], collapse = ',') )      
      z2_ind <- which( cumsum( lengths(unlist( lapply(z2, strsplit, split = "," ), 
                                               recursive = F ) ) ) >= n )
      if( length( z2_ind ) > 0 ) {
        z2 <- z2[ seq_len( z2_ind[1] ) ]
      }
      if( length(z2) != n ) { z2[ (length(z2)+1):n ] <- NA_character_ }
      return( c( z2, sum( z1 ) ) )
    } else { # lenx < 1
      return( as.list( rep(NA_character_, n ), NA_character_ ) )
    }})  
  a1 <- do.call('rbind', a1)
  a1 <- data.frame( group = rownames( a1 ), a1, stringsAsFactors = FALSE )
  colnames( a1 ) <- c( col1, paste( 'm', 1:n, sep = '' ), 'count' )
  rownames( a1 ) <- NULL
  return( a1 )
}
violinplot <- function(dataset_orig, var,groupcol, adjust1, max2cx) {
    dt <- as.data.table(dataset_orig)
    dt1 <- dt[, n := .N, by = groupcol]
    g<-ggplot(dt1[n >= 3,])+
      geom_violin(aes_string(y = var, x = groupcol), scale = "width", 
                  alpha = 0.4, adjust = adjust1) + 
    scale_y_continuous(limits = c(0,ceiling(max2cx)) , breaks=pretty_breaks(15) ) + 
    coord_flip() +xlab("Clades") + ylab("Genome size 2C (pg)") +
    theme(
      axis.title.x=element_text(angle=0, color='black', size=rel(1.5)),
      axis.title.y=element_text(angle=0, color='black', size=rel(1.5)),
      axis.text.y=element_text(family="CourierNew",size=rel(1.2) ),
      axis.text.x=element_text(family="CourierNew",size=rel(1.5) ) )
} 
# simpleCap <- function(x) { # place capitals in every string
#   s <- strsplit(x, " ")[[1]]
#   paste(toupper(substring(s, 1,1)), substring(s, 2),
#         sep="", collapse=" ")
# }
# simpleCap2 <- function(x) {# place capitals in every string separated by -
#   s <- strsplit(x, "-")[[1]]
#   paste(toupper(substring(s, 1,1)), substring(s, 2),
#         sep="", collapse="-")
# }
isEmpty <- function(x) {
  return(length(x)==0)
}
themelegJtPbDvBh<-theme(legend.box.just = c("top"), legend.position="bottom", legend.direction="vertical", legend.box="horizontal") 
themelegtitCou13texCou<-  theme(legend.title=element_text(family="CourierNew",size=rel(1.3)),legend.text=element_text(family="CourierNew")) 
themeslxS05CblyS05Cb<-theme(axis.line.x=element_line(size=0.5, colour="black")) + theme(axis.line.y=element_line(size=0.5, colour="black"))
scalexB10E0<-scale_x_continuous(breaks = pretty_breaks(10), expand = c(0,0))
scalexLwrap<-scale_x_discrete(labels = function(x) str_wrap(x, width = 6))
scaley2rE0B10<-scale_y_sqrt(expand=c(0,0),breaks = pretty_breaks(10))
themePGMwPBe4tityA0S15titxS15M20ptPtitS2AxtexS10<- theme(panel.grid.major = element_line(colour = "white"))+ theme(panel.background = element_rect(fill = "#e4e4e4")) + theme(axis.title.y = element_text( angle = 0, size=rel(1.5)))+ 
  theme(axis.title.x=element_text(size=rel(1.5), margin = margin(t = 20, unit = "pt") ) ) +  theme(plot.title = element_text(size = rel(2))) +       theme(axis.text = element_text(size=rel(1.0)) )#
themePGMwPBe4tityA0S15titxS15LtexS13PtitS15AxtexS15<- theme(panel.grid.major = element_line(colour = "white") )+ theme(panel.background = element_rect(fill = "#e4e4e4") ) +theme(axis.title.y = element_text(angle = 0, size=rel(1.5)) ) + 
  theme(axis.title.x=element_text(size=rel(1.5) ) ) + theme(legend.text=element_text(size=rel(1.3)) )+  theme(plot.title = element_text(size = rel(1.5)) )+  theme(axis.text = element_text(size=rel(1.5)) )
themePGMwPBe4tityA0S15titxS15LtexS13PtitS2AxtexS10<- theme(panel.grid.major = element_line(colour = "white"))+ theme(panel.background = element_rect(fill = "#e4e4e4")) + theme(axis.title.y = element_text(angle = 0, size=rel(1.5)))+ 
  theme(axis.title.x=element_text(size=rel(1.5) ) ) + theme(legend.text=element_text(size=rel(1.3)))+  theme(plot.title = element_text(size = rel(2))) + theme(axis.text = element_text(size=rel(1.0)))# + scale_x_discrete(labels = function(x) str_wrap(x, width = 6))
themeLtexS13<-theme(legend.text=element_text(size=rel(1.3)))
themeLtitS13<-theme(legend.title=element_text(size=rel(1.3)))
themePGMyblank<-theme(panel.grid.minor.y = element_blank())
themepmu01010101cm<-theme(plot.margin=unit(c(0.1,0.1,0.1,0.1),"cm"))
themeLegBerLegSpa1<-theme(legend.background = element_rect(), legend.spacing = unit(1, "cm"))
scaleyconE0P10<-scale_y_continuous(expand=c(0,0),breaks = pretty_breaks(10))
expandl050010<-expand_limits(x = c(0,50), y = c(0,10))
expandl050<-expand_limits(x = c(0,5) )
expandl0505<-expand_limits(x = c(0,5) , y = c(0,5))
is.odd <- function(x) x %% 2 != 0 & x%%1==0
is.wholenumber<- function(x)  x%%1==0
standardnamestechniques=c("FISH (molecular cyt.)","CMA banding","Silver staining","C-banding","2C value","karyotype formula","sporophyte with image","sporophyte","gametophyte with image","gametophyte")
standardnamestechniques<-gsub(" ","_",standardnamestechniques)
# important
standardnamescountryaffi= c("Brazil","Argentina","U.S.A.","Rest of America","Europe","Rest of the World")
standardnamescountryaffi <- rev(standardnamescountryaffi)
# file= "techniquesimplenew.xls"
# file= "techniquesimplenewTurnera.xls"
file= "mainData.xls"

# bibdfallaffi<-read_excel(file, sheet = "bibdfallaffi", col_names = TRUE, col_types = NULL, na = "",
                 # skip = 0)
# setwd("~/GoogleDrive/gitlab/cerradopri")
bibdfallaffi<-readxl::read_excel(path = file,sheet = "bibdfallaffi", col_types = 'guess', 
                            guess_max = 2000000, na=c("na", "NA", ""), trim_ws = T)
bibdfallaffi<-as.data.frame(bibdfallaffi)

listaauthor<-strsplit(as.character(bibdfallaffi$`integrated affi`), ";" , fixed=TRUE)
dupauthor=function(x) {rep(x, sapply(listaauthor, length))}
authorduplicated=data.table(newauthors = unlist(listaauthor), apply(bibdfallaffi,2,dupauthor))
authorduplicated$authoralone<-gsub('(\\w+?)\\.,.*', '\\1', authorduplicated$newauthors)
authorduplicated$authoralone<-trimws(authorduplicated$authoralone)
authorduplicated$authoralone=gsub(",","", (authorduplicated$authoralone), perl=TRUE) # # put everything in small caps from
authorduplicated$authoralone=gsub("\\.\\s","", (authorduplicated$authoralone), perl=TRUE) # # put everything in small caps from
authorduplicated$authoralone=gsub("\\.","", (authorduplicated$authoralone), perl=TRUE) # # put everything in small caps from
authorduplicated$authoralone<-sub("([A-Za-z]{1})([ñéúãíçóêôöáàãA-Za-z]+)(.*)", "\\U\\1\\L\\2\\E\\3",authorduplicated$authoralone, perl=T)
authorduplicated$authoralone<-gsub("([^ñéúãíçóêôöáàãA-Za-z -]+)", "", authorduplicated$authoralone)
authorduplicated$authoralone<-gsub("[ ]", "-", authorduplicated$authoralone) # remplaza todos los espacios por hyfen
authorduplicated$authoralone<-gsub("(^.+)(-)([^ ]*$)", "\\1 \\3", authorduplicated$authoralone) # elimina los segundo hyphens
authorduplicated$authoralone<-sub("(da|DA|Da|de|De|DE|dos|Dos|DOS)-", "\\L\\1 ", authorduplicated$authoralone, perl=T) # minuscula
authorduplicated$authoralone<-sub("([ñéúãíçóêôöáàãA-Za-z]{1})([ñéúãíçóêôöáàãA-Za-z]+)(.*)","\\1\\L\\2\\E\\3", 
                                  authorduplicated$authoralone, perl=TRUE)
authorduplicated$authoralone<-sub("(.*)\\s([ñéúãíçóêôöáàãA-Za-z]+)$", "\\1 \\U\\2", authorduplicated$authoralone, perl=T) # elimina los segundo hyphens
# sort(unique(authorduplicated$authoralone))

authorduplicated$newauthors <- gsub("(\\.,)([A-Za-z])", "., \\2", authorduplicated$newauthors) # substitute 1 or more spaces at end or beginning with nothing
authorduplicated$newauthors <- gsub("\\s+$|^\\s+", "", authorduplicated$newauthors) # substitute 1 or more spaces at end or beginning with nothing
authorduplicated$newauthors<-gsub("United States", "United-States", authorduplicated$newauthors)
authorduplicated$newauthors<-gsub("United-States", "U.S.A", authorduplicated$newauthors)
authorduplicated$newauthors<-gsub("U.S.A.", "U.S.A", authorduplicated$newauthors)
authorduplicated$newauthors<-gsub("USA", "U.S.A", authorduplicated$newauthors)
authorduplicated$newauthors<-gsub("United Kingdom", "United-Kingdom", authorduplicated$newauthors)
authorduplicated$newauthors<-gsub("Scotland", "United-Kingdom", authorduplicated$newauthors)
authorduplicated$newauthors<-gsub("España", "Spain", authorduplicated$newauthors)
authorduplicated$newauthors<-gsub("Brasil", "Brazil", authorduplicated$newauthors)
authorduplicated$newauthors<-gsub("netherlands", "Netherlands", authorduplicated$newauthors)
authorduplicated$countryalone<-sub("^.*\\s([A-Za-z\\.-]+)$", "\\1", authorduplicated$newauthors) # this is country and author duplicated
# sort(unique(authorduplicated$countryalone))
authorduplicated<-as.data.frame(authorduplicated)
authorduplicated$year<-as.integer(authorduplicated$year)
authorduplicated<-authorduplicated[,c(2,1,3:ncol(authorduplicated))]
citeandcountries<-authorduplicated[,c("uniquecite","countryalone")]
citeandcountries<-unique(citeandcountries)
citeandcountries<-citeandcountries %>%
  group_by_at("uniquecite") %>%
  summarise(afficountries = paste(countryalone, collapse = ", ") )
citeandcountries<-as.data.frame(citeandcountries)
authorcountryalone<-as.data.frame(authorduplicated)
authorcountryalone<-authorcountryalone[,c("authoralone","countryalone")]
authorcountryalone<-unique(authorcountryalone)
authorcountryalone<-authorcountryalone %>%
  group_by_at("authoralone") %>%
  summarise(countryalone = paste(countryalone, collapse = "; ") )
authorcountryalone<-as.data.frame(authorcountryalone)
# bibdf<-read_excel(file, sheet = "allrefsbib", col_names = TRUE, col_types = NULL, na = "",
                  # skip = 0)
# bibdf$key=gsub("(?<=\\A)([a-z])", "\\U\\1", tolower(bibdf$key), perl=TRUE) # # put everything in small caps from
# bibdf$key=gsub("Martinez", "Martínez", bibdf$key, perl=TRUE) # # put everything in small caps from
# # bibdf$key=sapply(bibdf$key, simpleCap)
# bibdf$key<-gsub("(^|[[:space:]])([[:alpha:]])", "\\1\\U\\2", bibdf$key, perl=TRUE)#simpleCap
# # bibdf$key=sapply(bibdf$key, simpleCap2)
# bibdf$key<-gsub("(^|\\-)([[:alpha:]])", "\\1\\U\\2", bibdf$key, perl=TRUE)#simplecap2

cbPalette <- c("#a6cee3", "#1f78b4", "#b2df8a", "#33a02c", "#fb9a99", "#e31a1c", 
               "#fdbf6f", "#ff7f00", "#cab2d6", "#6a3d9a", "#ffff99", "#b15928",
               "#999999", # gris 
               "#FFFF00", # amarillo
               "black",
               "bisque3")
datac<-fread("datac.csv",encoding="UTF-8", header = TRUE)
# colnames(datac)
integrate<-fread("integrate.csv",encoding="UTF-8", header = TRUE, stringsAsFactors = F)
integrate<-as.data.frame(integrate)
colnames(integrate)<-gsub(" ","_",colnames(integrate))
genfam<-integrate[,c(1,2,4,7,9)]
genfam<-genfam[which(!is.na(genfam$check)),]
# nrow(genfam)#322
myper<-round(sum(genfam$species_number_checklist)/sum(integrate$species_number_checklist) *100,1)
projdir<-getwd()
setwd(tempdir())
on.exit(setwd(projdir))
saveRDS(myper,"myper.rds")
setwd(projdir)
genfam<-genfam[ , -which(names(genfam) %in% c("check"))]  

tech2<-read_excel(path = file,sheet = "techniquesnew", col_types = 'guess', guess_max = 2000000, na=c("na", "NA", ""), trim_ws = T)
makenumcols<-function(df){
  df<-as.data.frame(df)
  cond <- apply(df, 2, function(x) {
    x <- x[!is.na(x)]
    all(suppressWarnings(!is.na(as.numeric(x))))
  })
  numeric_cols <- names(df)[cond]
  df[,numeric_cols] <- sapply(df[,numeric_cols], as.numeric)
  return(df)
}
tech2<-makenumcols(tech2)
colnames(tech2)<-gsub(" ","_",colnames(tech2))

techall<-tech2
# unique(techall$parsed2n)
tech2= tech2[, c(1:27)] # column name
techtc <- tech2[, c("uniquecite", "link","apgorder","family_apg","species","tcl","tca","parsed2n","parsed2c","ploidy","reference")]
tech= techall[, c(1:24)]#, with=FALSE]
tech$species = as.factor(tech$species) # necessary to input choosing working, like plots 
techg = as.data.frame(tech)
# techg<-as.data.frame(techg)
techg$genus <- gsub("([A-Za-z]+).*", "\\1", techg$species) #create genus column
techg$genus = as.factor(techg$genus) # necessary to input choosing working 
setwd(tempdir())
on.exit(setwd(projdir))
saveRDS(techg,"techg.rds")
setwd(projdir)
techmatch = techall[,c(1:24,28)]
techmatch$genus <- gsub("([A-Za-z]+).*", "\\1", techmatch$species) #create genus column
techmatch$genus = as.factor(techmatch$genus) # necessary to input choosing working , like plots
techmatch$speciessimple <- gsub("(\\w+)(\\s)(\\w+).*$", "\\1\\2\\3", techmatch$species) #get fist 2 words of species name
techmatch$speciessimple = as.factor(techmatch$speciessimple) # necessary to input choosing working, like plots 
techmatch$species = as.factor(techmatch$species) # necessary to input choosing working, like plots 
techmatch$speciessimpleorig <- gsub("(\\w+)(\\s)(\\w+).*$", "\\1\\2\\3", techmatch$nameonsource) #get fist 2 words of species name
techmatch$speciessimpleorig = as.factor(techmatch$speciessimpleorig) # necessary to input choosing working, like plots 
techmatch<-setDT(techmatch)
parsed2xpergenus<-techg %>% group_by_at("genus") %>% 
  summarise(
    parsed2x=if ( isEmpty(min(as.numeric(names(sort(-table(na.omit(as.numeric(parsed2n))))))[1:(
      if (round(length( as.numeric(names(sort(-table(na.omit(as.numeric(parsed2n) ))) ) )) )<4)
      {round(length( as.numeric(names(sort(-table(na.omit(as.numeric(parsed2n) ))) ) )) )} 
      else if (round(length( as.numeric(names(sort(-table(na.omit(as.numeric(parsed2n) ))) ) )) )>=4)
      {4}   )  ]    ) ) ) {
      NA
    }  else {
      min(as.numeric(names(sort(-table(na.omit(as.numeric(parsed2n) )))))[1:(
        if (round(length( as.numeric(names(sort(-table(na.omit(as.numeric(parsed2n) ))) ) )) )<4)
        {round(length( as.numeric(names(sort(-table(na.omit(as.numeric(parsed2n) ))) ) )) )} 
        else if (round(length( as.numeric(names(sort(-table(na.omit(as.numeric(parsed2n) ))) ) )) )>=4)
        {4}   )  ]    )  } )
x2ndata = as.data.frame(tech)[, c(1,2,3,4,21)]
x2cdata = as.data.frame(tech)[, c(1,2,3,4,20)]
x2ndata=x2ndata[complete.cases(x2ndata),] #http://stackoverflow.com/questions/4862178/remove-rows-with-nas-in-data-frame
x2cdata=x2cdata[complete.cases(x2cdata),] #http://stackoverflow.com/questions/4862178/remove-rows-with-nas-in-data-frame
x2ndata=unique(x2ndata) # http://stats.stackexchange.com/questions/6759/removing-duplicated-rows-data-frame-in-r
x2ndata$apgorder = as.factor(x2ndata$apgorder)
x2ndata$`family_apg` = as.factor(x2ndata$`family_apg`) # necesario para que funcione escoger los niveles en inputs
x2cdata$apgorder = as.factor(x2cdata$apgorder)
x2cdata$`family_apg` = as.factor(x2cdata$`family_apg`) # necesario para que funcione escoger los niveles en inputs
x2ndata$`family_apg_aste` = as.factor(x2ndata$`family_apg_aste`) # necesario para que funcione escoger los niveles en inputs
x2cdata$`family_apg_aste` = as.factor(x2cdata$`family_apg_aste`) # necesario para que funcione escoger los niveles en inputs
x2ndata$genus <- gsub("([A-Za-z]+).*", "\\1", x2ndata$species) #crear columna genero http://stackoverflow.com/questions/31925811/extract-first-word-from-a-column-and-insert-into-new-column
x2cdata$genus <- gsub("([A-Za-z]+).*", "\\1", x2cdata$species) #crear columna genero http://stackoverflow.com/questions/31925811/extract-first-word-from-a-column-and-insert-into-new-column
x2ndata$genus = as.factor(x2ndata$genus) # necesario para que funcione escoger los niveles en inputs
x2cdata$genus = as.factor(x2cdata$genus) # necesario para que funcione escoger los niveles en inputs
# str(x2ndata)
# unique(tech2$parsed2n)
newmedfamgencount<-x2ndata %>% group_by_at("apgorder") %>% 
  summarise(med = median(parsed2n), fam = n_distinct(`family_apg`) , gen = n_distinct(genus) , cts=n() ) 

newmedfamgencount<-as.data.frame(newmedfamgencount)
data<-newmedfamgencount
namecol<-"ordmedcoufamcougencou"
medianfambyordergencount<-make.legend.withstats(data,namecol)
x2ndata=merge(x2ndata, medianfambyordergencount, all.x = TRUE)
fammediangencountnew<-x2ndata %>% group_by_at("family_apg") %>% 
  summarise(med = median(parsed2n), 
            gen = n_distinct(genus) , 
            cts=n() ) 
fammediangencountnew<-as.data.frame(fammediangencountnew)
data<-fammediangencountnew
namecol<-"fammedgencou"
fammediangencount<-make.legend.withstats(data,namecol)
x2ndata=merge(x2ndata, fammediangencount, all.x = TRUE)
x2ndata$fammedgencou = as.factor(x2ndata$fammedgencou)
newmedfamgencount2c<-x2cdata %>% group_by_at("apgorder") %>%
  summarise(med = format(round(median(parsed2c),2),nsmall=2),
            fam = n_distinct(`family_apg`) ,
            gen = n_distinct(genus) ,
            cts=n() )
newmedfamgencount2c<-as.data.frame(newmedfamgencount2c)
data<-newmedfamgencount2c
namecol<-"ordmedcoufamcougencou"
medianfambyordergencount2c<-make.legend.withstats(data,namecol)
x2cdata=merge(x2cdata, medianfambyordergencount2c, all.x = TRUE)
x2cdatamode2cmodescount<-get.3modes.andcounts(x2cdata,"apgorder","parsed2c",0.5)
data<-x2cdatamode2cmodescount
namecol<-"labelcount"
x2cdatamode2cmodescount<-make.legend.withstats(data,namecol)
x2cdata<-merge(x2cdata, x2cdatamode2cmodescount, all.x = TRUE)
x2cdata$labelcount = as.factor(x2cdata$labelcount)
# colnames(x2cdata)
x2cdatafammodes<-get.3modes.andcounts(x2cdata,"family_apg","parsed2c",0.5)
data<-x2cdatafammodes
namecol<-"fam2clabelcount"
x2cdatafammodes<-make.legend.withstats(data,namecol)
x2cdata<-merge(x2cdata, x2cdatafammodes, all.x = TRUE)
x2cdata$fam2clabelcount = as.factor(x2cdata$fam2clabelcount)
x2cdatagenmodes<-get.3modes.andcounts(x2cdata,"genus","parsed2c",0.5)
data<-x2cdatagenmodes
namecol<-"gen2clabelcount"
x2cdatagenmodes<-make.legend.withstats(data,namecol)
x2cdata<-merge(x2cdata, x2cdatagenmodes, all.x = TRUE)
x2cdata$gen2clabelcount = as.factor(x2cdata$gen2clabelcount)
newfammediangencount2x<-x2cdata %>% group_by_at("family_apg") %>%
  summarise(med = format(round(median(parsed2c),2),nsmall=2),
            gen = n_distinct(genus) ,
            cts=n() )
newfammediangencount2x<-as.data.frame(newfammediangencount2x)
data<-newfammediangencount2x
namecol<-"fammedgencou"
fammediangencount2c<-make.legend.withstats(data,namecol)
x2cdata=merge(x2cdata, fammediangencount2c, all.x = TRUE)
x2cdata$fammedgencou = as.factor(x2cdata$fammedgencou)
genusmedian2ccount<-x2cdata %>% group_by_at("genus") %>%
  summarise(med = format(round(median(parsed2c), 2), nsmall = 2),
            cts=n() ) #, max_mpg = max(mpg) , max_disp = max(disp))
# genusmedian2ccount
genusmedian2ccount<-as.data.frame(genusmedian2ccount)
data<-genusmedian2ccount
namecol<-"genusmediancount"
genusmedian2ccount<-make.legend.withstats(data,namecol)
x2cdata=merge(x2cdata, genusmedian2ccount, all.x = TRUE)
x2cdata$genusmediancount<-as.factor(x2cdata$genusmediancount)
x2nmodesandcount<-get.3modes.andcounts2n(x2ndata,"apgorder","parsed2n")
x2nmodesandcount<-as.data.frame(x2nmodesandcount)
data<-x2nmodesandcount
namecol<-"labelcount"
x2nmerged<-make.legend.withstats(data,namecol)
x2ndata=merge(x2ndata, x2nmerged, all.x = TRUE)
x2ndata$labelcount = as.factor(x2ndata$labelcount)
famx2nmodesandcount<-get.3modes.andcounts2n(x2ndata,"family_apg","parsed2n")
famx2nmodesandcount<-as.data.frame(famx2nmodesandcount)
data<-famx2nmodesandcount
namecol<-"famlabelcount"
famx2nmodesandcount<-make.legend.withstats(data,namecol)
# famx2nmodesandcount #b
x2ndata=merge(x2ndata, famx2nmodesandcount, by ="family_apg") # 3 modes and count per family
x2ndata$famlabelcount = as.factor(x2ndata$famlabelcount)
genx2nmodesandcount<-get.3modes.andcounts2n(x2ndata,"genus","parsed2n")
genx2nmodesandcount<-as.data.frame(genx2nmodesandcount)
data<-genx2nmodesandcount
namecol<-"genlabelcount"
genx2nmodesandcount<-make.legend.withstats(data,namecol)
x2ndata=merge(x2ndata, genx2nmodesandcount, by ="genus")
x2ndata$genlabelcount = as.factor(x2ndata$genlabelcount)
genusmedian2ncount<-x2ndata %>% group_by_at("genus") %>% 
  summarise(med = median(parsed2n), 
            cts=n() ) #, max_mpg = max(mpg) , max_disp = max(disp))
genusmedian2ncount<-as.data.frame(genusmedian2ncount)
data<-genusmedian2ncount
namecol<-"genmedgencou"
genusmedian2ncount<-make.legend.withstats(data,namecol)
x2ndata=merge(x2ndata, genusmedian2ncount, all.x = TRUE)
x2ndata$genmedgencou<-as.factor(x2ndata$genmedgencou)
tech.m = melt(tech, id=c("range","family_apg","family_apg_aste","species","provenance","uniquecite","year","reference","link","parsed2c","parsed2n","range2","affiliation_country","apgorder"))
x2ndatanolabel<-x2ndata[ , !names(x2ndata) %in% c("labelcount")] 
x2ndatatotal<-x2ndatanolabel#x2cdata[ , !names(x2cdata) %in% c("labelcount")] 
x2ndatatotal$apgorder<-"Whole sample"
x2ndupli<-rbind(x2ndatatotal,x2ndatanolabel)
x2ndupli$apgorder = as.factor(x2ndupli$apgorder)
x2ndupli<-x2ndupli[,c(3,6)]
x2ndatatotalmodes<-get.3modes.andcounts2n(x2ndupli,"apgorder","parsed2n")
x2ndatatotalmodes[is.na(x2ndatatotalmodes)] <- ""
data2<-x2ndatatotalmodes
namecol<-"labelcount"
x2ndatatotalmodes<-make.legend.withstats(data2,namecol)
x2ndatatotal<-merge(x2ndupli, x2ndatatotalmodes, all.x = TRUE)
x2ndatatotal$labelcount = as.factor(x2ndatatotal$labelcount)
# colnames(tech.m)
# ?group_by_at
summarytech<- tech.m %>% 
  group_by_at(.vars = c("range","variable")) %>%
  summarise(
    N    = sum(!is.na(value)) 
  ) %>% 
  bind_rows(
    tech.m %>% 
      group_by_at("variable")  %>%
      summarise(
        N    = sum(!is.na(value)) 
      ) %>% 
      mutate(range = "All years")
  )
tech.m2=merge(tech.m, citeandcountries, by="uniquecite", all.x = TRUE)
tech.m2<-setDT(tech.m2)
tech.m2<-subset(tech.m2, select= -`affiliation_country`)
colnames(tech.m2)[colnames(tech.m2)=='afficountries'] <- "affiliation_country" # levels(x)[[round(input$plot_click$y)]]
summarytech$range= as.factor(summarytech$range)
listacountry<-strsplit(as.character(tech.m2$provenance), ", " , fixed=TRUE) # tech.m
dupcountry=function(x) {rep(x, sapply(listacountry, length))}
countryduplicated=data.table(newprovenance= unlist(listacountry), apply(tech.m2,2,dupcountry))# tech.m
long_string1="america|Argentina|belize|Bolivia|central america or brazil|chile|colombia|Costa rica|cuba|dist colombia to brazil|cuba|
ecuador|el salvador|guatemala|guiana|dist mexico to brazil|dist venezuela|dominica|dominican republic|
honduras|jamaica|lesser antilles|mexico|neotropical|nicaragua|paraguay|panama|peru|surinam|
trinidad|uruguay|puerto rico|South America|suriname|trinidad|trinidad and tobago|USA|
venezuela|america|canada|Perú|U.S.A.|Panamá|Puerto-Rico"
long_string1 <- iconv(unique(long_string1),"","UTF-8")
long_string1=gsub("(?<=\\b)([a-z])", "\\U\\1", tolower(long_string1), perl=TRUE)
long_string1=gsub("[\r\n]", "", long_string1)
rest_worldprov="philippines|solomon islands|sri lanka|taiwan|thailand|australia|burundi|vanuatu|uganda|zaire|
canada|france|Hawaii|india|italia|japan|madagascar|myanmar|nigeria|pakistan|
Bangladesh|Malaysia|Vietnam|Sierra Leone|Nepal|Israel|Sri-Lanka|Sierra-Leone|Ghana|Senegal|Malawi"
rest_worldprov <- iconv(unique(rest_worldprov),"","UTF-8")
rest_worldprov=gsub("(?<=\\b)([a-z])", "\\U\\1", tolower(rest_worldprov), perl=TRUE)
rest_worldprov=gsub("[\r\n]", "", rest_worldprov)
countryduplicated$provspecific <- ifelse(grepl("Bra[z|s]il", countryduplicated$newprovenance, ignore.case = T), "Brazil",
                                         ifelse(grepl(long_string1, countryduplicated$newprovenance, ignore.case = T), "Rest of America",
                                                ifelse(grepl(rest_worldprov, countryduplicated$newprovenance, ignore.case = T), "Rest of the World","unknown origin")) )
countryduplicated$provspecific=as.factor(countryduplicated$provspecific)
countryduplicated=select(countryduplicated, -newprovenance, everything())
listacountry<-strsplit(as.character(tech$provenance), ", " , fixed=TRUE) #basado en tech
dupcountry=function(x) {rep(x, sapply(listacountry, length))}
countryprovduplicated=data.table(newprovenance= unlist(listacountry), apply(tech,2,dupcountry)) # based in tech
countryprovduplicated=merge(countryprovduplicated, citeandcountries, by="uniquecite", all.x = TRUE)
countryprovduplicated<-subset(countryprovduplicated, select= -`affiliation_country`) # eliminate column
colnames(countryprovduplicated)[colnames(countryprovduplicated)=='afficountries'] <- "affiliation_country" # levels(x)[[round(input$plot_click$y)]]
countryprovduplicated$provspecific <- ifelse(grepl("Brazil", countryprovduplicated$newprovenance, ignore.case = T), "Brazil",
                                         ifelse(grepl(long_string1, countryprovduplicated$newprovenance, ignore.case = T), "Rest of America",
                                                ifelse(grepl(rest_worldprov, countryprovduplicated$newprovenance, ignore.case = T), "Rest of the World","unknown origin")) )
countryprovduplicated=select(countryprovduplicated, -newprovenance, everything()) # pass column to end
listaafficountry<-strsplit(as.character(countryprovduplicated$'affiliation_country'), ", " , fixed=TRUE)
dupafficountry=function(x) {rep(x, sapply(listaafficountry, length)) }
countryaffiduplicated=data.table(newaffi= unlist(listaafficountry), apply(countryprovduplicated,2,dupafficountry))
long_string2="Austria|Denmark|Germany|Netherlands|Spain|Sweden|Switzerland|United Kingdom|United-Kingdom|Belgium|France|Italy"
long_string2 <- iconv(unique(long_string2),"","UTF-8")
long_string2=gsub("(?<=\\b)([a-z])", "\\U\\1", tolower(long_string2), perl=TRUE)
long_string3="Chile|Uruguay|Colombia|Mexico|Paraguay|Canada|Trinidad-and-Tobago|Puerto-Rico"
rest_world="Australia|India|Japan|Egypt|Nigeria|Philippines|Taiwan|Thailand|Vanuatu|Arabia|Pakistan|Algeria|China|Korea|Israel|Ghana|Bangladesh|Vietnam|Nepal|Madagascar"
rest_world=gsub("(?<=\\b)([a-z])", "\\U\\1", tolower(rest_world), perl=TRUE)
countryaffiduplicated$affispecific <-           ifelse(grepl("Bra[z|s]il", countryaffiduplicated$newaffi, ignore.case = T), "Brazil",
                                                       ifelse(grepl("Argentina", countryaffiduplicated$newaffi, ignore.case = T), "Argentina",
                                                              ifelse(grepl("U.S.A", countryaffiduplicated$newaffi, ignore.case = T), "U.S.A.",
                                                                     ifelse(grepl(long_string2, countryaffiduplicated$newaffi, ignore.case = T), "Europe",
                                                                            ifelse(grepl(long_string3, countryaffiduplicated$newaffi, ignore.case = T), "Rest of America",
                                                                                   ifelse(grepl(rest_world,   countryaffiduplicated$newaffi, ignore.case = T), "Rest of the World","NA")
                                                                            ) ) ) ) ) #  ) )
countryaffiduplicated$affispecific=as.factor(countryaffiduplicated$affispecific)
unique(countryaffiduplicated$affispecific)
countryaffiduplicated$provspecific=as.factor(countryaffiduplicated$provspecific)
countryaffiduplicated=select(countryaffiduplicated, -newaffi, everything())
countryaffiduplicated<-  as.data.frame(countryaffiduplicated)[,c(2:5,1,24,6:23,25:28)]
# standardnamescountryaffi= c("Brazil","Argentina","U.S.A.","Rest of America","Europe","Rest of the World") #up
unique(countryaffiduplicated$affispecific)
affiselectcolumn=select(countryaffiduplicated, uniquecite, affispecific, range2,provspecific)
affiselectcolumn$range2=as.factor(affiselectcolumn$range2)
affiselectcolumn$uniquecite=as.factor(affiselectcolumn$uniquecite)
colorsmanual <- read.table(text="
wheat4
darkgray
gainsboro
cadetblue1
\"#7CE3D8\" 
cornflowerblue
darkblue
darkolivegreen1
yellowgreen
chartreuse
forestgreen
seagreen
seagreen1
moccasin
lightsalmon
indianred1
red2
tomato3
magenta
mediumorchid1
mediumvioletred
yellow1
orange
\"#DDAD4B\" 
tan2
tan3
tan4
",header=FALSE,fill=TRUE,stringsAsFactors=FALSE)
c2<-colorsmanual[c(10,17,23,6,22,19,3,25,8,16,24,7,20,2,26,9,15,5,18,1,27,13,4,11,14,21,12),]
copalette<-rep(c2,2)
countryduplicated<-as.data.frame(countryduplicated)
countryduplicated<-move(6,"uniquecite",countryduplicated)
countryduplicated<-move(13,"affiliation_country",countryduplicated)
countryduplicated<-setDT(countryduplicated)
tcreference <- c(20,40,60,80,100)
newdata<-data.frame(x=tcreference)
mediantcl<-techtc %>% group_by_at("apgorder") %>% filter(!is.na(tcl)) %>% 
  summarize(med = format(round(median(tcl, na.rm=TRUE), 1), nsmall = 1), 
            ctcl= n())
mediantcl<-as.data.frame(mediantcl)  
data<-mediantcl
namecol<-"ordmedcounttcl"
mediantclcount<-make.legend.withstats(data,namecol)
mediantca<-techtc %>% group_by_at("apgorder") %>% filter(!is.na(tca)) %>% 
  summarize(med = format(round(median(tca, na.rm=TRUE), 1), nsmall = 1), 
            ctca= n())
mediantca<-as.data.frame(mediantca)
data<-mediantca
namecol<-"ordmedcounttca"
mediantcacount<-make.legend.withstats(data,namecol)
techtcsummary<-merge(techtc, mediantclcount, by="apgorder", all = TRUE) # is dataframe
techtcsummary<-merge(techtcsummary, mediantcacount, by="apgorder", all = TRUE)
techtcsummary<-as.data.frame(techtcsummary)
techtcsummary$ordmedcounttca <- as.factor(techtcsummary$ordmedcounttca)
techtcsummary$ordmedcounttcl <- as.factor(techtcsummary$ordmedcounttcl)
techtcsummary$apgorder <- as.factor(techtcsummary$apgorder)
techtconly2c <- subset(techtc, select=-c(apgorder,`family_apg`,tcl,tca,parsed2n, reference))
techtconly2c <- techtconly2c[!(is.na(techtconly2c$parsed2c)  ), ]