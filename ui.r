defaultEncoding <- "UTF8"
inputUserid <- function(inputId, value='') {
  tagList(
    singleton(tags$head(tags$script(src = "js/md5.js", type='text/javascript'))),
    singleton(tags$head(tags$script(src = "js/shinyBindings.js", type='text/javascript'))),
    tags$body(onload="setvalues()"),
    tags$input(id = inputId, class = "userid", value=as.character(value), type="text", style="display:none;")
  )
}
inputIp <- function(inputId, value=''){
  tagList(
    singleton(tags$head(tags$script(src = "js/md5.js", type='text/javascript'))),
    singleton(tags$head(tags$script(src = "js/shinyBindings.js", type='text/javascript'))),
    tags$body(onload="setvalues()"),
    tags$input(id = inputId, class = "ipaddr", value=as.character(value), type="text", style="display:none;")
  )
}
sidebar <- dashboardSidebar(
  width = 260,
  sidebarMenu(
    inputIp("ipid"), # new
    inputUserid("fingerprint"), #new
    id = "tabs",
    HTML('<h4 style="padding: 10px 10px;">Select a menu</h4>'),
    menuItem("Scientometrics", tabName="not_menu1", icon = icon("list-ul"),
             menuSubItem("Techniques chronology",icon = icon("braille"), tabName = "bubblechart1", selected=F),
             menuSubItem("Authors by region",icon = icon("braille"), tabName = "bubblechart2", selected=F)
             ,
             menuSubItem("Study effort",icon = icon("braille"), tabName = "studyhistotab", selected=F)
             ,
             menuSubItem("Coauthors network",icon = icon("braille"), tabName = "networktab", selected=F)             
    ), 
    menuItem("2n and 2C Histograms", icon = icon("list-ul"),
             menuSubItem("2n in major clades",icon = icon("bar-chart"), tabName = "2ntab", selected=T),
             menuSubItem("Genome size 2C-value", icon = icon("bar-chart"), tabName = "2ctab")
    ), #end menuitem
    menuItem("TCL and TCA vs 2C", icon = icon("list-ul"),
             selectInput("inputTCAorTCL", "Select Total Chromosome length or Area",
                         choices = c("TCL and Genome size", "TCA and Genome size"), multiple=F, selected = "TCL and Genome size", 
                         width = '98%'),
             menuSubItem("Select TCA or TCL and click here", icon = icon("bar-chart"), tabName = "tctab")
    ), #end menuitem
    menuItem("Database search",tabName = "cerradodb", icon = icon("search")
    ), #end menuitem
    menuItem("Stats",tabName = "stats", icon = icon("table")),
    menuItem("Contribute",tabName = "contribute", icon = icon("keyboard-o")),
        HTML('<br></br>'),
    menuItem("About", tabName = "about", icon = icon("info-circle") )
  ) # end sidebar menu
) # end dashboardsidebar
body <- dashboardBody(
   tags$head(tags$style(HTML('
                                /* body */
                                .content-wrapper, .right-side {
                                background-color: #FFFFFF;
                                }
                                                        ')))
  ,tags$head(tags$style(HTML('
      .main-header .sidebar-toggle:before {
        content: "\\f0c9\\00a0 Menu";}'))),
  tags$head(
    tags$style(HTML("
    
    .main-header .navbar{-webkit-transition:margin-left .3s ease-in-out;-o-transition:margin-left .3s ease-in-out;transition:margin-left .3s ease-in-out;margin-bottom:0;margin-left:350px;}
    
    @media (min-width: 768px ){
    .main-header .logo {
    width: 350px;
    }
    }
    
    .main-header. logo{
    width:350px
    }
    
      .shiny-output-error-validation {
        color: black;  	padding:15px 50px;
	margin:20px 40px;
      }
    ")),
    HTML("
                 <!DOCTYPE html>
<html lang='en'>
                 <head>
                 <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
                 "),
    tags$link(rel = "stylesheet", href = "styles.css"),
    tags$style(type='text/css', "#tablehistotclstatbutton { margin-left: 35px; }"),
    tags$style(type='text/css', "#tablehistotclbutton { margin-left: 35px; }"),
    tags$style(type='text/css', "#tablehistogenbutton { margin-left: 15px; }"),
    tags$style(type='text/css', "#showallcolumnsbutton { margin-left: 15px; }"),
    tags$style(type='text/css', "#showallcolumnshistogen { margin-left: 15px; }"),
    tags$style(type='text/css', "#tablehistogenbutton2c { margin-left: 15px; }"),
    tags$style(HTML("
    .selectize-control.single .selectize-input:after {
        content: ' ';
        display: block;
        position: absolute;
        top: 50%;
        right: 10px;
        margin-top: -1px;
        width: 0;
        height: 0;
        border-style: solid;
        border-width: 5px 5px 5px 5px;
        border-color: #333333 transparent transparent transparent;
    }
    .selectize-control.single .selectize-input.dropdown-active:after {
        top: 50%;
        margin-top: -5px;
        width: 5;
        height: 5;
        border-style: solid;
        border-width: 0px 5px 0px 0px;
        border-color: #333333 transparent transparent transparent;
    }")),
    
    tags$style(HTML("
.button{
	height:2em;
	padding:15px 50px;
	margin:20px 40px;
	cursor:pointer;
	display:inline-block;
	color:#FFF;
	font-size:1em;
	border:1px solid #eee;
	background:#eee;
	border-radius:4px;
	line-height:2em;
	border:1px solid #aaa;
	text-decoration:none;
	-webkit-transition: all 0.3s linear;
     -khtml-transition: all 0.3s linear;
       -moz-transition: all 0.3s linear;
         -o-transition: all 0.3s linear;
            transition: all 0.3s linear;
}
.button:hover {
    background-color: #4CAF50; /* Green */
    color: white;
}
.button:active {
    -webkit-box-shadow: rgba(255,255,255,0.25) 0px 1px 0px, inset rgba(255,255,255,0.03) 0px 20px 0px, inset rgba(0,0,0,0.15) 0px -20px 20px, inset rgba(255,255,255,0.05) 0px 20px 20px;
                -khtml-box-shadow: rgba(255,255,255,0.25) 0px 1px 0px, inset rgba(255,255,255,0.03) 0px 20px 0px, inset rgba(0,0,0,0.15) 0px -20px 20px, inset rgba(255,255,255,0.05) 0px 20px 20px;
                -moz-box-shadow: rgba(255,255,255,0.25) 0px 1px 0px, inset rgba(255,255,255,0.03) 0px 20px 0px, inset rgba(0,0,0,0.15) 0px -20px 20px, inset rgba(255,255,255,0.05) 0px 20px 20px;
                -o-box-shadow: rgba(255,255,255,0.25) 0px 1px 0px, inset rgba(255,255,255,0.03) 0px 20px 0px, inset rgba(0,0,0,0.15) 0px -20px 20px, inset rgba(255,255,255,0.05) 0px 20px 20px;
                box-shadow: rgba(255,255,255,0.25) 0px 1px 0px, inset rgba(255,255,255,0.03) 0px 20px 0px, inset rgba(0,0,0,0.15) 0px -20px 20px, inset rgba(255,255,255,0.05) 0px 20px 20px;
                text-shadow:1px 1px 1px #eee;
                }
        /* logo */
                            .skin-blue .main-header .logo {
                            background-color: #009852;
                            }
                            /* logo when hovered */
                            .skin-blue .main-header .logo:hover {
                            background-color: #009852;
                            }
                            /* navbar (rest of the header) */
                            .skin-blue .main-header .navbar {
                            background-color: #009852;
                            }
                            .skin-blue .main-sidebar {
                            background-color: #00a65a;
                            }
                            /* other links in the sidebarmenu when hovered */
                            .skin-blue .main-sidebar .sidebar .sidebar-menu a:hover{
                            background-color: #00cc6e;
                            }
.skin-blue .sidebar-menu>li.header{color:#47735b;background:#009852}
.skin-blue .sidebar-menu>li>a{border-left:3px solid transparent}
.skin-blue .sidebar-menu>li.active>a,
.skin-blue .sidebar-menu>li:hover>a{color:#fff;background:#00b65a;border-left-color:#184631}
.skin-blue .sidebar-menu>li>.treeview-menu{margin:0 1px;background:#00cc6e}.skin-blue .sidebar a{color:#404543}
.skin-blue .sidebar a:hover{text-decoration:none}
.skin-blue .treeview-menu>li>a{color:#6c7460}
.skin-blue .treeview-menu>li.active>a,.skin-blue .treeview-menu>li>a:hover{color:#fff}
                            ")) ),
  tags$head(
    tags$script(
      HTML("
           window.onload = function() {
           resize();
           }
           window.onresize = function() {
           resize();
           }
           Shiny.addCustomMessageHandler ('triggerResize',function (val) {
           window.dispatchEvent(new Event('resize'));
           });
           function resize(){
           var h = window.innerHeight - $('.navbar').height() - 150; // Get dashboardBody height
           $('#box').height(h);
           }"
      )),
    tags$style(HTML("
.js-irs-0 .irs-bar,.js-irs-2 .irs-bar,.js-irs-4 .irs-bar,.js-irs-6 .irs-bar {
border-top-color: #d2d2d2;
border-bottom-color: #d2d2d2;
}
.js-irs-0 .irs-bar-edge,.js-irs-2 .irs-bar-edge,.js-irs-4 .irs-bar-edge,.js-irs-6 .irs-bar-edge {
border-color: #d2d2d2;
}
.js-irs-0 .irs-bar-edge,.js-irs-2 .irs-bar-edge,.js-irs-4 .irs-bar-edge,.js-irs-6 .irs-bar-edge {
          background: #f1f1f1;
}
.js-irs-0 .irs-bar,.js-irs-2 .irs-bar,.js-irs-4 .irs-bar,.js-irs-6 .irs-bar {
          background: #f1f1f1;
}
.js-irs-0 .irs-single,.js-irs-2 .irs-single,.js-irs-4 .irs-single,.js-irs-6 .irs-single {
          background: #0099ff;
}
.js-irs-0 .irs-slider, .js-irs-1 .irs-slider, .js-irs-2 .irs-slider, .js-irs-3 .irs-slider, .js-irs-4 .irs-slider, .js-irs-5 .irs-slider,.js-irs-6 .irs-slider,.js-irs-7 .irs-slider,.js-irs-8 .irs-slider,.js-irs-9 .irs-slider,.js-irs-10 .irs-slider,.js-irs-11 .irs-slider,.js-irs-12 .irs-slider,.js-irs-13 .irs-slider {width: 15px; height: 15px; top: 20px;}
"
    )
    )
  ), #head
  tabItems(
    tabItem(tabName = "bubblechart1",
            fluidRow(
              tabsetPanel(id="tabsetpanel1",
                          tabPanel("Plot", 
                                   column(width=7,
                                          uiOutput("bubbletechniquebox")
                                   ),#column
                                   column(width=3,  
                                          actionButton('showcountry',strong('Options of region of provenance of plants'),  icon("hand-pointer-o")
                                          ),
                                          uiOutput("selectcountryui")  
                                   ) # column close
                          ), #tab panel
                          tabPanel("Table", "Here, the sample of the clicked bubble is shown!",
                                   column(width=12,
                                          uiOutput("tabletechniquebox")
                                   ) #column close
                          ) #tabpanel 
              ) #tabsetpanel
            )#fluidrow
    ),#tabitem
    tabItem(tabName = "bubblechart2",
            fluidRow(
              tabsetPanel(id="tabsetpanel2",
                          tabPanel("Plot for plants collected in Brazil", 
                                   uiOutput("bubbleaffiboxes")
                          ), #tab panel
                          tabPanel("Table for plants collected in Brazil", "Here, the sample of the clicked bubble is shown!",
                                   column(width=12,
                                          shinyjs::hidden(
                                            div(id = "hiddenbutton",
                                                uiOutput('showallcolumnsbutton')
                                            ) # end div 
                                          ),
                                          uiOutput("tableaffibox")
                                   ) #column close
                          ) #tabpanel 
              ) #tabsetpanel
            )#fluidrow
    ),#tabitem
    tabItem(tabName = "networktab",
            fluidRow(
              tabsetPanel(id="tabsetnetwork",
                          tabPanel("Co-author network", 
                                   uiOutput("networkboxes")
                          )
                          , #tab panel
                        tabPanel("Table", 
                                 HTML("<div class='col-sm-12' style='padding:0px 5px px 5px;'>"),
                                 uiOutput("resultsauthortablebox"),
                                 HTML("</div>") 
                        ), #end tabpanel
                        tabPanel("Historical Video", 
                                 HTML("<div class='col-sm-8' style='padding:0px 5px px 5px;'>"),
                                 uiOutput("videobox"),
                                 HTML("</div>") 
                        ) #end tabpanel
              ) #tabsetpanel
            )#fluidrow
    ),#tabitem
    tabItem(tabName = "studyhistotab",
            fluidRow(
              tabsetPanel(id="tabsetstudyhisto",
                          tabPanel("Study effort histogram", 
                                   uiOutput("studyhistoboxes")
                          ) #tabpanel 
              ) #tabsetpanel
            )#fluidrow
    ),#tabitem
tabItem(tabName = "2ntab",
        fluidRow(
          tags$style(HTML("
                          .tabs-above > .nav > li > a {
                          background-color: #cdebff;
                          }
                          .tabs-above > .nav > li[class=active] > a {
                          background-color: #81ccff;
                          " )),
          tabsetPanel(id="tabsetpanel3",
                      tabPanel("Histogram per major clade",
                               uiOutput("majorcladepanel") 
                      ) # end tabpanel
                      ,
                      tabPanel("Histogram per family",
                               uiOutput("familypanel")
                      ),
                      tabPanel("Histogram per genus",
                               uiOutput("genuspanel")
                      ) # end tabpanel
                      ,
                      tabPanel("Table of selected genera", "Here, the sample of the previous tab is shown when you click the \"generate table\" button!",
                               column(width=12,
                                      useShinyjs(),
                                      shinyjs::hidden(
                                        div(id = "hiddenbuttonhistogen",
                                            uiOutput('showallcolumnshistogenbutton')
                                        )
                                      ),
                                      uiOutput("tablehistogenbox")
                               ) #column close
                      ) #tabpanel
          ) #end tabsetpanel
          ) # end fluidrow
        ) # end tabitem
,
tabItem(tabName = "2ctab",
        fluidRow(
          tabsetPanel(id="tabsetpanel4",
                      tabPanel("Histogram per major clade", 
                               uiOutput("majorcladepanel2c") 
                      ) # end tabpanel
                      ,
                      tabPanel("Histogram per family", 
                               uiOutput("familypanel2c")
                      ) # end tabpane
                      ,
                      tabPanel("Histogram per genus", 
                               uiOutput("genuspanel2c")
                      ) # end tabpanel
                      ,
                      tabPanel("Table of selected genera", "Here, the sample of the previous tab is shown when you click the \"generate table\" button!",
                               HTML("<div class='col-sm-12' style='padding:5px 5px 0px 5px;'>"),
                               uiOutput("tablehistogenbox2c")
                               ,
                               HTML("</div>") 
                      ) #tabpanel 
          ) #end tabsetpanel
        ) # end fluidrow
) # end tabitem
    ,
    tabItem(tabName = "tctab",
            fluidRow(
              tabsetPanel(id="tabsetpaneltcl",
                          tabPanel("Histogram per major clade", 
                                          HTML("<div class='col-sm-12' style='padding:5px 5px 0px 5px;'>"),
                                          fluidRow(column(4,uiOutput("empty3454") ), column(8,fluidRow(
                                     column(4,uiOutput("tcltabbox2") )
                                   ) ) ),
                                   fluidRow(
                                     HTML("<div class='col-sm-4' style='padding:0px 0px 0px 0px;'>"),
                                                   uiOutput("tcltabbox"),
                                                   actionButton('tablehistotclbutton',strong('Generate table of histogram data'),  icon("hand-pointer-o"),
                                                                style="color: #000; background-color: #0099ff; border-color: #2e6da4"
                                                   ),
                                  HTML("</div>") ,
                                  HTML("<div class='col-sm-8' style='padding:0px 0px 0px 0px;'>"),
                                          uiOutput("tclhistobox"),
                                  HTML("</div>") 
                                  ), # fluid
                                  HTML("</div>") 
                          ) # end tabpanel
                          ,
                          tabPanel("Correlation details", 
                                   fluidRow(
                                     column(5, 
                                            HTML('<br></br>'),
                                            uiOutput("correlationbox"),
                                            uiOutput("tablehistotclstatbuttonoutput"),
                                            HTML('<br></br>'),
                                            uiOutput("historesbox")
                                            ,
                                            HTML('<br></br>'),
                                            uiOutput("quantilequantileresbox"),
                                            HTML('<br></br>'),
                                            uiOutput("plotresidualsbox")
                                     ), #column
                                     column(7,          
                                            HTML('<br></br>'),
                                            uiOutput("plotregressionbox")
                                     ) #column
                                   )#fluidrow
                          )#tabpanel
                          ,
                          tabPanel("Table of selected sample", "Here, the sample of the previous tab is shown when you click the \"generate table\" button!",
                                   column(width=12,
                                          uiOutput("tablehistotclbox")
                                   ) #column close
                          ) #tabpanel 
              ) #end tabsetpanel
            ) # end fluidrow
    ) # end tabitem
    ,
    tabItem(tabName = "about",
            fluidRow(
                column(width=2),
                column(width=8
                        ,tags$head(tags$style(HTML('
                                                        /* body */
                                .content-wrapper, .right-side {
                                background-color: #FFFFFF;
                                }
                                                        ')))
                        
                        ,img(src='CeCylogo.png', align = "right", width="20%")
                        
                        ,uiOutput("markdown")
                ) # column
              # htmlOutput("abouttext")
            ) #fluidrow close
    )#tabitem
    ,
    tabItem(tabName = "cerradodb",
            fluidRow(
              tabsetPanel(id="tabsetpaneldb",
                          tabPanel("Query and table", 
                                   HTML("<br><div class='col-sm-2' style='min-width: 230px !important; padding:0px 0px 0px 0px;'>"),
                                          uiOutput("searchdbbox"),
                                   HTML("</div>") ,
                                   HTML("<div class='col-sm-9' style='padding:0px 5px px 5px;'>"),
                                          uiOutput("resultsdbtablebox"),
                                          htmlOutput("abouttable"),
                                   HTML("</div>") 
                          ) #end tabpanel
                          ,
                          tabPanel("2n and 2C Histograms", 
                                   column(width=3, 
                                          uiOutput("dbhisto"),
                                          uiOutput("dbhisto2")
                                   ) #end column
                                   ,
                                   column(width=9,
                                          uiOutput("histodbbox")
                                   ) #end column
                          ) #end tabpanel
              ) # end tabsetpanel
            ) # end fluidrow
    ),#tabitem
    tabItem(tabName = "stats",
            fluidRow(
              column(width=2, 
                     uiOutput("statsbox")
              ) #end column
              , # end column
              column(width=10,
                     uiOutput("statsresultsbox"),
                     htmlOutput("aboutstats")
              ) # column
            ) #fluidrow
    ),
tabItem(tabName = "contribute",
        uiOutput("contributetab")
)
  ) #tabitems
) # body
dashboardPage(
  title="Cerrado Cytogenetics database",#skin= "green",
  dashboardHeader(    title = "Cytogenetics of plants of Cerrado", 
                      tags$li(a(href = 'https://lgbio.icb.ufg.br/',
                                img(src = 'https://lgbio.icb.ufg.br/up/275/o/LGBio.gif',
                                    title = "LGBIO UFG", height = "40px"),
                                style = "padding-top:5px; padding-bottom:5px;"),
                              class = "dropdown"),
                      titleWidth = 350
    ), #dashboardheader
  sidebar,
  body
)
